<?php

namespace App;

use App\Traits\SerializesData;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer user_id
 * @property float amount
 * @property integer id
 * @property User user
 * @property string status
 * @property WithdrawalFee fee
 */
class Withdrawal extends Model
{
    use SerializesData;

    protected $guarded = [];

    const STATUS_PENDING = 'pending', STATUS_DONE = 'done', STATUS_REJECTED = 'rejected';

    protected $casts = [
        'amount' => 'float'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function fee()
    {
        return $this->hasOne(WithdrawalFee::class);
    }
}
