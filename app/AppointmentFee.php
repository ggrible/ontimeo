<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Stripe\Charge;

/**
 * @property float stripe_fee
 * @property float fee
 * @property Appointment appointment
 */
class AppointmentFee extends Model
{
    protected $guarded = [];

    protected $casts = [
        'fee' => 'float',
        'stripe_fee' => 'float'
    ];

    public static function createFromAppointment(Appointment $appointment)
    {
        $charge = Charge::retrieve([
            'id' => $appointment->stripe_charge_id,
            'expand' => ['balance_transaction']
        ]);
        return $appointment->fees()
            ->create([
                'fee' => Setting::getByKey(Setting::KEY_APPOINTMENT_PROCESSING_FEE)->value,
                'stripe_fee' => round($charge->balance_transaction->fee / 100, 2)
            ]);
    }

    public function getTotalFee()
    {
        return round($this->fee + $this->stripe_fee, 2);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function appointment()
    {
        return $this->belongsTo(Appointment::class);
    }
}
