<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionAddOn extends Model
{
    protected $guarded = [];
}
