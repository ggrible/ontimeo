<?php

namespace App\Observers;

use App\Voucher;
use Stripe\Coupon;

class VoucherObserver
{
    public function saving(Voucher $voucher)
    {
        if (!$voucher->stripe_id) {
            $voucher->createStripeCoupon();
        }
    }
}
