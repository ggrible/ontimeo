<?php

namespace App\Observers;

use App\Invoice;
use App\Transaction;

class InvoiceObserver
{
    public function created(Invoice $invoice)
    {
        Transaction::createFromInvoice($invoice);
    }
}
