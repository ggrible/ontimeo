<?php

namespace App\Observers;

use App\AppointmentFee;
use App\Transaction;

class AppointmentFeeObserver
{
    public function created(AppointmentFee $appointmentFee)
    {
        Transaction::createFromAppointmentFee($appointmentFee);
    }
}
