<?php

namespace App\Observers;

use App\Setting;
use App\Transaction;
use App\Withdrawal;

class WithdrawalObserver
{
    public function created(Withdrawal $withdrawal)
    {
        Transaction::createFromWithdrawal($withdrawal);
    }

    public function saving(Withdrawal $withdrawal)
    {
        if ($withdrawal->isDirty('status') && $withdrawal->status === Withdrawal::STATUS_DONE) {
            $withdrawal->fee()
                ->create([
                    'fee' => Setting::getByKey(Setting::KEY_WITHDRAWAL_FEE)->value
                ]);
            Transaction::createFromWithdrawal($withdrawal);
            $withdrawal->user->subtractPersonalBalance($withdrawal->amount);
        }
    }
}
