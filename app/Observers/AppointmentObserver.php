<?php

namespace App\Observers;

use App\Appointment;
use App\AppointmentFee;
use App\Transaction;
use App\UserCalendar;
use Microsoft\Graph\Graph;

class AppointmentObserver
{
    /**
     * @param Appointment $appointment
     * @throws \Microsoft\Graph\Exception\GraphException
     */
    public function created(Appointment $appointment)
    {
        if ($appointment->stripe_charge_id) {
            Transaction::createFromAppointment($appointment);
            AppointmentFee::createFromAppointment($appointment);
        }
        $appointment->syncCalendarEvent();
    }

    /**
     * @param Appointment $appointment
     * @throws \Microsoft\Graph\Exception\GraphException
     */
    public function deleted(Appointment $appointment)
    {
        if ($appointment->google_event_id) {
            if ($calendar = $appointment->doctor->getCalendar(UserCalendar::PROVIDER_GOOGLE)) {
                google()->setCalendarId($calendar->getCalendarId());
                google()->setAccessToken($calendar->getAccessTokenArray());
                google()->deleteEvent($appointment->google_event_id);
            }
        }
        if ($appointment->outlook_event_id) {
            if ($calendar = $appointment->doctor->getCalendar(UserCalendar::PROVIDER_OUTLOOK)) {
                $graph = new Graph();
                $graph->setAccessToken($calendar->access_token);
                $getEventsUrl = '/me/calendars/' . $calendar->getCalendarId() . '/events/' . $appointment->outlook_event_id;
                $graph->createRequest('DELETE', $getEventsUrl)
                    ->execute();
            }
        }
    }
}
