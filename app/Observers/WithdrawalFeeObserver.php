<?php

namespace App\Observers;

use App\Transaction;
use App\WithdrawalFee;

class WithdrawalFeeObserver
{
    public function created(WithdrawalFee $fee)
    {
        Transaction::createFromWithdrawalFee($fee);
    }
}
