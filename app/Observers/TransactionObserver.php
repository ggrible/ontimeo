<?php

namespace App\Observers;

use App\Transaction;

class TransactionObserver
{
    public function created(Transaction $transaction)
    {
        switch ($transaction->type) {
            case Transaction::TYPE_PREPAID_APPOINTMENT:
                $transaction->user->addPersonalBalance($transaction->amount);
                break;
            case Transaction::TYPE_APPOINTMENT_FEE:
            case Transaction::TYPE_WITHDRAWAL:
//                $transaction->user->subtractPersonalBalance($transaction->amount);
                break;
        }
    }
}
