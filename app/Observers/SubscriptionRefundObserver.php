<?php

namespace App\Observers;

use App\SubscriptionRefund;
use App\Transaction;

class SubscriptionRefundObserver
{
    public function created(SubscriptionRefund $refund)
    {
        Transaction::createFromSubscriptionRefund($refund);
    }
}
