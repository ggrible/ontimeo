<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property Appointment|Invoice reference
 * @property string type
 * @property User user
 * @property float amount
 */
class Transaction extends Model
{
    protected $guarded = [];

    protected $casts = [
        'amount' => 'float'
    ];

    const TYPE_PREPAID_APPOINTMENT = 'prepaid appointment', TYPE_APPOINTMENT_FEE = 'appointment fee', TYPE_SUBSCRIPTION_INVOICE = 'subscription invoice', TYPE_WITHDRAWAL = 'withdrawal', TYPE_SUBSCRIPTION_REFUND = 'subscription refund', TYPE_WITHDRAWAL_FEE = 'withdrawal fee';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function reference()
    {
        return $this->morphTo('reference');
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [self::TYPE_WITHDRAWAL, self::TYPE_APPOINTMENT_FEE, self::TYPE_PREPAID_APPOINTMENT, self::TYPE_SUBSCRIPTION_INVOICE];
    }

    /**
     * @param Appointment $appointment
     * @return Transaction
     */
    public static function createFromAppointment(Appointment $appointment)
    {
        return static::query()
            ->create([
                'type' => self::TYPE_PREPAID_APPOINTMENT,
                'user_id' => $appointment->doctor_id,
                'amount' => $appointment->amount,
                'reference_type' => get_class($appointment),
                'reference_id' => $appointment->id,
            ]);
    }

    /**
     * @param AppointmentFee $appointmentFee
     * @return Transaction
     */
    public static function createFromAppointmentFee(AppointmentFee $appointmentFee)
    {
        return static::query()
            ->create([
                'type' => self::TYPE_APPOINTMENT_FEE,
                'user_id' => $appointmentFee->appointment->doctor_id,
                'amount' => $appointmentFee->getTotalFee(),
                'reference_type' => get_class($appointmentFee),
                'reference_id' => $appointmentFee->id,
            ]);
    }

    /**
     * @param Invoice $invoice
     * @return Transaction
     */
    public static function createFromInvoice(Invoice $invoice)
    {
        return static::query()
            ->create([
                'type' => self::TYPE_SUBSCRIPTION_INVOICE,
                'user_id' => $invoice->user_id,
                'amount' => $invoice->amount,
                'reference_type' => get_class($invoice),
                'reference_id' => $invoice->id,
            ]);
    }

    /**
     * @param Withdrawal $withdrawal
     * @return Transaction
     */
    public static function createFromWithdrawal(Withdrawal $withdrawal)
    {
        if ($withdrawal->status === Withdrawal::STATUS_DONE) {
            /**
             * @var Transaction $transaction
             */
            $transaction = static::query()
                ->where('reference_type', Withdrawal::class)
                ->where('reference_id', $withdrawal->id)
                ->first();
            if (!$transaction) {
                return static::query()
                    ->create([
                        'type' => self::TYPE_WITHDRAWAL,
                        'user_id' => $withdrawal->user_id,
                        'amount' => round($withdrawal->amount - $withdrawal->fee->fee, 2),
                        'reference_type' => get_class($withdrawal),
                        'reference_id' => $withdrawal->id,
                    ]);
            } else {
                $transaction->update([
                    'amount' => round($withdrawal->amount - $withdrawal->fee->fee, 2),
                ]);
                return $transaction;
            }
        } else {
            return static::query()
                ->create([
                    'type' => self::TYPE_WITHDRAWAL,
                    'user_id' => $withdrawal->user_id,
                    'amount' => round($withdrawal->amount, 2),
                    'reference_type' => get_class($withdrawal),
                    'reference_id' => $withdrawal->id,
                ]);
        }
    }

    /**
     * @param WithdrawalFee $withdrawal_fee
     * @return Transaction
     */
    public static function createFromWithdrawalFee(WithdrawalFee $withdrawal_fee)
    {
        return static::query()
            ->create([
                'type' => self::TYPE_WITHDRAWAL_FEE,
                'user_id' => $withdrawal_fee->withdrawal->user_id,
                'amount' => $withdrawal_fee->fee,
                'reference_type' => get_class($withdrawal_fee),
                'reference_id' => $withdrawal_fee->id,
            ]);
    }

    /**
     * @param SubscriptionRefund $refund
     * @return Transaction
     */
    public static function createFromSubscriptionRefund(SubscriptionRefund $refund)
    {
        return static::query()
            ->create([
                'type' => self::TYPE_SUBSCRIPTION_REFUND,
                'user_id' => $refund->subscription->user_id,
                'amount' => $refund->amount,
                'reference_type' => get_class($refund),
                'reference_id' => $refund->id,
            ]);
    }
}
