<?php

namespace App;

use App\Traits\SerializesData;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property string stripe_product_id
 * @property string name
 * @property boolean is_default
 */
class AddOn extends Model
{
    protected $guarded = [];

    use SerializesData, SoftDeletes;

    protected $dates = ['deleted_at'];

    const ADD_ON_PREPAID = 8;

    protected $casts = [
        'is_default' => 'boolean'
    ];
}
