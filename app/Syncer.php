<?php
session_start();

class Syncer {

    public function __construct() {
        $this->outlook_apiurl = "https://graph.microsoft.com/v1.0/";
    }

    public function auth($type) {
        $this->creds($type);

        switch ($type) {
            case 'ol365':
                $url = 'https://login.microsoftonline.com/common/oauth2/v2.0/authorize?client_id=' . $this->ol365_appid . '&response_type=code&redirect_uri=' . $this->ol365_redurl . '&response_mode=query&scope=' . $this->ol365_scopes . '&state=' . rand(00000, 99999);
                header('location: ' . $url);
                break;

            default:
                # code...
                break;
        }
    }

    public function tokenize($type, $code) {
        $this->creds($type);

        switch ($type) {
            case 'ol365':
                $this->ol365_get_token($code);
                break;

            default:
                # code...
                break;
        }
    }

    public function messages($token) {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://graph.microsoft.com/v1.0/me/messages");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

        $headers = array();
        $headers[] = "Authorization: Bearer " . $token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);

        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }

        curl_close ($ch);

        return json_decode($result, true);
    }

    public function calendar($token) {
        $start	= '2017-12-01T00:00:01.0000000';
        $end	= '2017-12-31T23:59:59.0000000';

        $url = $this->outlook_apiurl . 'me/calendarView?startDateTime=' . $start . '&endDateTime=' . $end;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

        $headers = array(
            "Authorization: Bearer " . $token, // Always need our auth token!
            "Accept: application/json", // Always accept JSON response.
            "Prefer: outlook.timezone=\"Eastern Standard Time\""
        );

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);

        if (curl_errno($ch)) {
            echo 'Error: ' . curl_error($ch);
        }

        curl_close ($ch);

        return json_decode($result, true);
    }

    public function add_event_calender($token, $subject, $location, $start, $end, $body_content = '', $attendee_str = false) {
        // posts vars
        $event = array(
            'subject'  => $subject,
            'body'     => array('contentType' => 'HTML', 'content' => $body_content),
            'start'    => array('dateTime' => $start, 'timeZone' => 'Pacific Standard Time'),
            'end'      => array('dateTime' => $end, 'timeZone' => 'Pacific Standard Time'),
            'location' => array('displayName' => $location)
        );

        if (!is_null($attendee_str) && strlen($attendee_str) > 0) {
            $addresses = array_filter(explode(';', $attendee_str));
            $attendees = array();

            foreach($addresses as $address) {
                $attendee = array(
                    'emailAddress' 	=> array('address' => $address),
                    'Type' 			=> 'Required'
                );

                $attendees[] = $attendee;
            }

            $event["attendees"] = $attendees;
        } else {
            $event['attendees'] = array();
        }

        $posts 	= json_encode($event);
        $url   	= $this->outlook_apiurl . 'me/events';
        $ch 	= curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $posts);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $headers = array(
            "Authorization: Bearer " . $token,
            "Prefer: outlook.timezone=\"Eastern Standard Time\"",
            "Content-type: application/json",
        );

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);

        if (curl_errno($ch)) {
            echo 'Error: ' . curl_error($ch);
        }

        curl_close ($ch);

        if ($result['Id']) {
            return 'success';
        } else {
            return $result;
        }

        return '';
    }

    /**
     * Mail Provider Credentials
     * This will declare the initial variables
     * @param $type str
     */
    private function creds($type) {
        switch ($type) {
            case 'ol365':
                $this->ol365_appid  = '2544cffa-031c-48c1-b694-17d8eeca99ba';
                $this->ol365_secret = 'rpCZP12_~:sknxzGTQY453)';
                $this->ol365_redurl = 'https://outlook365.comontime.com/auth.php';
                // $this->ol365_scopes = 'offline_access%20user.read%calendar.readwrite';
                $this->ol365_scopes = 'openid%20offline_access%20https%3A%2F%2Fgraph.microsoft.com%2Fmail.read%20https%3A%2F%2Fgraph.microsoft.com%2Fcalendars.read%20https%3A%2F%2Fgraph.microsoft.com%2Fcalendars.readwrite';
                break;

            default:
                # code...
                break;
        }
    }

    // https://outlook365.comontime.com/auth.php

    /**
     * Generate access token privately
     */
    private function ol365_get_token($code) {
        try {
            $ch = curl_init();

            $params = array(
                'tenant'		=> 'consumers',
                'client_id'     => $this->ol365_appid,
                'grant_type'    => 'authorization_code',
                // 'scope'         => $this->ol365_scopes,
                'code'          => $code,
                'redirect_uri'  => $this->ol365_redurl,
                'client_secret' => $this->ol365_secret,
            );

            $params = http_build_query($params);

            curl_setopt($ch, CURLOPT_URL, 'https://login.microsoftonline.com/common/oauth2/v2.0/token');
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            $result 	= curl_exec($ch);
            $error 		= curl_errno($ch);

            if ($error) {
                $error = curl_error($ch);
                throw new Exception($error);
            }

            curl_close($ch);

            $result = json_decode($result, true);

            $_SESSION['response'] = $result;

            if (isset($result['access_token'])) {
                header('location: index.php');
            }
        } catch (Exception $e) {
            echo "Exception-" . $e->getMessage();
        }
    }

}
