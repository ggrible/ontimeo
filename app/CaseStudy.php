<?php

namespace App;

use App\Traits\SerializesData;
use Illuminate\Http\UploadedFile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class CaseStudy extends Model
{
    use SerializesData;
    protected $guarded = [];
    protected $fillable = ['user_id', 'description'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getOwnerUser()
    {
    	return User::find($this->user_id);
    }
}
