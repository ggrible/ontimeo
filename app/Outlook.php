<?php

namespace App;

use League\OAuth2\Client\Provider\GenericProvider;

class Outlook
{
    const URL_AUTHORIZE = 'https://login.microsoftonline.com/common/oauth2/v2.0/authorize', URL_ACCESS_TOKEN = 'https://login.microsoftonline.com/common/oauth2/v2.0/token', URL_RESOURCE_OWNER_DETAILS = 'https://outlook.office.com/api/v1.0/me', SCOPES = 'openid profile offline_access User.Read Calendars.ReadWrite';

    /**
     * @var GenericProvider
     */
    private $client;

    public function __construct()
    {
        $this->client = new GenericProvider([
            'clientId' => env('LIVE_KEY'),
            'clientSecret' => env('LIVE_SECRET'),
            'redirectUri' => config('services.live.redirect'),
            'urlAuthorize' => self::URL_AUTHORIZE,
            'urlAccessToken' => self::URL_ACCESS_TOKEN,
            'urlResourceOwnerDetails' => self::URL_RESOURCE_OWNER_DETAILS,
            'scopes' => self::SCOPES
        ]);
    }

    /**
     * @return GenericProvider
     */
    public function getClient()
    {
        return $this->client;
    }
}
