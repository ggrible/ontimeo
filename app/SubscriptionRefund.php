<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SubscriptionRefund
 * @package App
 * @property Subscription subscription
 * @property float amount
 * @property integer id
 * @property string stripe_id
 */
class SubscriptionRefund extends Model
{
    protected $guarded = [];

    protected $casts = [
        'amount' => 'float'
    ];

    public function subscription()
    {
        return $this->belongsTo(Subscription::class);
    }
}
