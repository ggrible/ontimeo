<?php

namespace App;

use App\Traits\SerializesData;
use Illuminate\Database\Eloquent\Model;
use Stripe\Coupon;

/**
 * @property string stripe_id
 * @property float discount_price
 */
class Voucher extends Model
{
    use SerializesData;

    protected $guarded = [];

    protected $casts = [
        'discount_price' => 'float'
    ];

    public function createStripeCoupon()
    {
        $coupon = Coupon::create([
            'duration' => 'once',
            'amount_off' => round($this->discount_price * 100),
            'currency' => 'usd'
        ]);
        $this->stripe_id = $coupon->id;
        return $coupon;
    }
}
