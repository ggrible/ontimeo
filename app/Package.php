<?php

namespace App;

use App\Jobs\CreateStripePlanForPackageTerm;
use App\Jobs\CreateStripePlansForPackageAddOn;
use App\Traits\SerializesData;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property Collection package_add_ons
 * @property Collection offers
 * @property Collection package_terms
 * @property string name
 * @property integer id
 * @property string stripe_product_id
 * @property string promo
 */
class Package extends Model
{
    use SerializesData, SoftDeletes;

    const STATUS_ACTIVE = 'active', STATUS_INACTIVE = 'inactive';
    const PROMO_FIRST_MONTH = '1st month', PROMO_FIRST_TWO_MONTHS = '1st 2 months', PROMO_FIRST_THREE_MONTHS = '1st 3 months';

    protected $guarded = [];

    protected $dates = ['deleted_at'];

    /**
     * @return array
     */
    public static function statuses()
    {
        return [self::STATUS_ACTIVE, self::STATUS_INACTIVE];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function offers()
    {
        return $this->hasMany(PackageOffer::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function package_add_ons()
    {
        return $this->hasMany(PackageAddOn::class);
    }

    /**
     * @param array $offers
     * @return void
     */
    public function saveOffers(array $offers)
    {
        $this->offers()
            ->whereNotIn('offer', $offers)
            ->delete();
        foreach ($offers as $offer) {
            $this->offers()
                ->firstOrCreate([
                    'offer' => $offer
                ]);
        }
        $this->save();
    }

    /**
     * @param array $add_ons
     * @return void
     */
    public function saveAddOns(array $add_ons)
    {
        $add_on_ids = [];
        foreach ($add_ons as $add_on) {
            $add_on_ids[] = $add_on['id'];
            $is_new_or_updated = $this->package_add_ons()
                    ->where('add_on_id', $add_on['id'])
                    ->where('price', array_get($add_on, 'price'))
                    ->count() === 0;
            /**
             * @var PackageAddOn $package_add_on
             */
            $package_add_on = $this->package_add_ons()
                ->updateOrCreate([
                    'add_on_id' => $add_on['id']
                ], [
                    'price' => $add_on['price'],
                    'status' => $add_on['active'] ? Status::ACTIVE : Status::INACTIVE
                ]);
            if ($is_new_or_updated) {
                $package_add_on->plans()
                    ->update([
                        'stripe_plan_id' => null
                    ]);
                dispatch(new CreateStripePlansForPackageAddOn($package_add_on));
            }
        }
        $this->package_add_ons()
            ->whereNotIn('add_on_id', $add_on_ids)
            ->update([
                'status' => Status::INACTIVE
            ]);
        $this->save();
    }

    /**
     * @param array $terms
     * @return void
     */
    public function saveTerms(array $terms)
    {
        $term_ids = [];
        foreach ($terms as $term) {
            $term_ids[] = $term['id'];
            $is_new_or_updated = $this->package_terms()
                    ->where('term_id', $term['id'])
                    ->where('price', array_get($term, 'price'))
                    ->where('discount', array_get($term, 'discount'))
                    ->count() === 0;
            /**
             * @var PackageTerm $package_term
             */
            $package_term = $this->package_terms()
                ->updateOrCreate([
                    'term_id' => $term['id']
                ], [
                    'price' => array_get($term, 'price'),
                    'discount' => array_get($term, 'discount')
                ]);
            if ($is_new_or_updated) {
                $package_term->update([
                    'stripe_plan_id' => null
                ]);
                dispatch(new CreateStripePlanForPackageTerm($package_term));
            }
        }
        $this->package_terms()
            ->whereNotIn('term_id', $term_ids)
            ->update([
                'status' => Status::INACTIVE
            ]);
        $this->save();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function package_terms()
    {
        return $this->hasMany(PackageTerm::class);
    }

    /**
     * @return array
     */
    public static function promos()
    {
        return [self::PROMO_FIRST_MONTH, self::PROMO_FIRST_TWO_MONTHS, self::PROMO_FIRST_THREE_MONTHS];
    }

    public function getPromoMonths()
    {
        if (!$this->promo) {
            return null;
        }
        switch ($this->promo) {
            case self::PROMO_FIRST_MONTH:
                return 1;
            case self::PROMO_FIRST_TWO_MONTHS:
                return 2;
            case self::PROMO_FIRST_THREE_MONTHS:
                return 3;
        }
        return null;
    }

    /**
     * @return null|Carbon
     */
    public function getNewTrialEnd()
    {
        if (!$this->getPromoMonths()) {
            return null;
        }
        return now()->addMonths($this->getPromoMonths());
    }

    /**
     * @return int|null
     */
    public function getNewTrialEndTimestamp()
    {
        if ($end = $this->getNewTrialEnd()) {
            return $end->getTimestamp();
        }
        return null;
    }

    /**
     * @return float
     */
    public function getBasePrice()
    {
        return $this->package_terms()
            ->min('price');
    }

    /**
     * @return int
     */
    public function activeSubscriberCount()
    {
        return Subscription::query()
            ->leftJoin('package_terms as pt', 'pt.id', '=', 'subscriptions.package_term_id')
            ->where(function (Builder $builder) {
                $builder->whereNull('ends_at')
                    ->orWhere('ends_at', '>', now());
            })
            ->where('pt.package_id', $this->id)
            ->count();
    }

    /**
     * @return int
     */
    public function getSubscriberCount()
    {
        return Subscription::query()
            ->leftJoin('package_terms as pt', 'pt.id', '=', 'subscriptions.package_term_id')
            ->where('pt.package_id', $this->id)
            ->count();
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    /**
     * @return float
     */
    public function totalInvoiceAmount()
    {
        return round($this->invoices()
            ->sum('amount'), 2);
    }
}
