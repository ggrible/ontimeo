<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PasswordSetupToken
 * @package App
 * @property string token
 * @property integer user_id
 */
class PasswordSetupToken extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
