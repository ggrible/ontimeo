<?php

namespace App;


use App\Traits\SerializesData;
use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
    use SerializesData;

    protected $fillable = [
        'company',
        'url',
        'ceo_company',
        'telephone',
        'email',
        'company_type'
    ];
}
