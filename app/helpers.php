<?php

function beginTransaction()
{
    \Illuminate\Support\Facades\DB::beginTransaction();
}

function commit()
{
    \Illuminate\Support\Facades\DB::commit();
}

function rollback()
{
    \Illuminate\Support\Facades\DB::rollback();
}

function transaction(Closure $closure)
{
    \Illuminate\Support\Facades\DB::transaction($closure);
}

/**
 * @param \Illuminate\Database\Eloquent\Model $model
 * @param array $attributes
 * @return array
 */
function getModelAttributes(\Illuminate\Database\Eloquent\Model $model, array $attributes)
{
    $arr = [];
    $model_attributes = $model->attributesToArray();
    foreach ($attributes as $attribute) {
        $arr[$attribute] = isset($model_attributes[$attribute]) ? $model_attributes[$attribute] : null;
    }
    return $arr;
}
/**
 * @return \App\User
 */
function user() {
    if (auth()->check()) {
        return auth()->user();
    }
    return null;
}

/**
 * @return \App\Google
 */
function google()
{
    return app(\App\Google::class);
}

/**
 * @return \App\Outlook
 */
function outlook()
{
    return app(\App\Outlook::class);
}

function move_to_next_day(\Carbon\Carbon $date, $day_of_week)
{
    if ($day_of_week == 7) {
        $day_of_week = 0;
    }
    while (!$date->isDayOfWeek($day_of_week)) {
        $date->addDay();
    }
    return $date;
}

function correct_day_of_week($day_of_week) {
    if ($day_of_week == 7) {
        return 0;
    }
    return $day_of_week;
}

function daysOfCurrentWeek($customNumber = false)
{
    $day = date('w') - 1;
    $week_start = date('Y-m-d', strtotime('-'.$day.' days'));
    $dates = array();
    for ($i=0; $i <= 6; $i++) { 
        
        $date = strtotime($week_start . ' +'.$i.' days');
        $dayNumber = $i + 1;
        
        // checks if public view
        $user_id = $customNumber ? optional(\App\User::where('custom_number', $customNumber)->first())->id
        : auth()->user()->id;

        $schedule = \App\Schedule::where('day', $i + 1)
        ->where('user_id',  $user_id)
        ->first();

        $dates[$i] = [
            'date' => date('Y-m-d', $date),
            'date_human_format' => date('F d, Y', $date),
            'month' => date('m', $date),
            'month_full_format' => date('F', $date),
            'week_date' => date('d', $date),
            'week_day_full_format' => date('l', $date),
            'week_day_three_format' => date('D', $date),
            'day' => $dayNumber,
            'year' => date('Y', $date),
            'is_active' => optional($schedule)->status == 'active' ? true : false,
            'hasPassed' => $dayNumber < date('w') ? true : false,
            'schedule_id' => optional($schedule)->id
        ];
    }
    return $dates;
}
