<?php

namespace App;

use App\Traits\SerializesData;
use Illuminate\Database\Eloquent\Model;

class UserEmployee extends Model
{
    use SerializesData;

    protected $fillable = [
        'user_id',
        'employee_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }
}
