<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property float value
 */
class Setting extends Model
{

    protected $fillable = ['key', 'value', 'display_name'];

    const KEY_APPOINTMENT_PROCESSING_FEE = 'appointment_processing_fee', KEY_WITHDRAWAL_FEE = 'withdrawal_fee';

    public static function updateSettings(array $attributes)
    {
        foreach ($attributes as $key => $value) {
            self::where('key', $value['key'])
            ->update([
                'key' => $value['key'],
                'value' => $value['value']
            ]);
        }
    }

    /**
     * @param $key
     * @return Setting
     */
    public static function getByKey($key)
    {
        return static::query()
            ->where('key', $key)
            ->first();
    }
}
