<?php

namespace App\Transformers;

use App\UserCalendar;
use League\Fractal\TransformerAbstract;

class UserCalendarTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(UserCalendar $userCalendar)
    {
        return $userCalendar->attributesToArray();
    }
}
