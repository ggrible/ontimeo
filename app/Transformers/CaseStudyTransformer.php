<?php

namespace App\Transformers;

use App\CaseStudy;
use League\Fractal\TransformerAbstract;
use App\Transformers\CaseStudyCategoryTransformer;

class CaseStudyTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['user'];
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(CaseStudy $caseStudy)
    {
        return $caseStudy->attributesToArray();
    }

    public function includeUser(CaseStudy $caseStudy)
    {
        return $this->item($caseStudy->user, new UserTransformer());
    }
}
