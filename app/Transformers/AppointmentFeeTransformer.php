<?php

namespace App\Transformers;

use App\AppointmentFee;
use League\Fractal\TransformerAbstract;

class AppointmentFeeTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param AppointmentFee $appointmentFee
     * @return array
     */
    public function transform(AppointmentFee $appointmentFee)
    {
        return $appointmentFee->attributesToArray();
    }
}
