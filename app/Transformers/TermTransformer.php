<?php

namespace App\Transformers;

use App\Term;
use League\Fractal\TransformerAbstract;

class TermTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param Term $term
     * @return array
     */
    public function transform(Term $term)
    {
        return $term->attributesToArray();
    }
}
