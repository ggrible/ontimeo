<?php

namespace App\Transformers;

use App\PackageTerm;
use League\Fractal\TransformerAbstract;

class PackageTermTransformer extends TransformerAbstract
{
    protected $defaultIncludes = ['term'];
    /**
     * A Fractal transformer.
     *
     * @param PackageTerm $term
     * @return array
     */
    public function transform(PackageTerm $term)
    {
        return $term->attributesToArray();
    }

    /**
     * @param PackageTerm $term
     * @return \League\Fractal\Resource\Item
     */
    public function includeTerm(PackageTerm $term)
    {
        return $this->item($term->term, new TermTransformer());
    }
}
