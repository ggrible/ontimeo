<?php

namespace App\Transformers;

use App\CaseStudyCategory;
use League\Fractal\TransformerAbstract;

class CaseStudyCategoryTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(CaseStudyCategory $caseStudyCategory)
    {
        return $caseStudyCategory->attributesToArray();
    }
}
