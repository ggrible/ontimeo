<?php

namespace App\Transformers;

use App\WithdrawalFee;
use League\Fractal\TransformerAbstract;

class WithdrawalFeeTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param WithdrawalFee $fee
     * @return array
     */
    public function transform(WithdrawalFee $fee)
    {
        return $fee->attributesToArray();
    }
}
