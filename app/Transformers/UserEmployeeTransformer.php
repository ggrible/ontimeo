<?php

namespace App\Transformers;

use App\UserEmployee;
use League\Fractal\TransformerAbstract;

class UserEmployeeTransformer extends TransformerAbstract
{
    protected $availabletIncludes = ['user'];
    protected $defaultIncludes = ['employee'];
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(UserEmployee $userEmployee)
    {
        return $userEmployee->attributesToArray();
    }

    public function includeUser(UserEmployee $userEmployee)
    {
        return $this->item($userEmployee->user, new UserTransformer());
    }
    
    public function includeEmployee(UserEmployee $userEmployee)
    {
        return $this->item($userEmployee->employee, new EmployeeTransformer());
    }
}
