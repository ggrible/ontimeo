<?php

namespace App\Transformers;

use App\Subscription;
use League\Fractal\TransformerAbstract;

class SubscriptionTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['package', 'add_ons'];

    /**
     * A Fractal transformer.
     *
     * @param Subscription $subscription
     * @return array
     */
    public function transform(Subscription $subscription)
    {
        return $subscription->attributesToArray();
    }

    public function includePackage(Subscription $subscription)
    {
        return $this->item($subscription->getPackage(), new PackageTransformer());
    }

    public function includeAddOns(Subscription $subscription)
    {
        return $this->collection($subscription->getAddOns(), new AddOnTransformer());
    }
}
