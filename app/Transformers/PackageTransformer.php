<?php

namespace App\Transformers;

use App\Package;
use League\Fractal\TransformerAbstract;

class PackageTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['package_add_ons', 'offers', 'package_terms'];
    /**
     * A Fractal transformer.
     *
     * @param Package $package
     * @return array
     */
    public function transform(Package $package)
    {
        return array_merge($package->attributesToArray(), [
            'base_price' => $package->getBasePrice(),
            'subscriber_count' => $package->activeSubscriberCount(),
            'total_invoice_amount' => $package->totalInvoiceAmount(),
        ]);
    }

    /**
     * @param Package $package
     * @return \League\Fractal\Resource\Collection
     */
    public function includePackageAddOns(Package $package)
    {
        return $this->collection($package->package_add_ons, new PackageAddOnTransformer());
    }

    /**
     * @param Package $package
     * @return \League\Fractal\Resource\Collection
     */
    public function includeOffers(Package $package)
    {
        return $this->collection($package->offers, new PackageOfferTransformer());
    }

    /**
     * @param Package $package
     * @return \League\Fractal\Resource\Collection
     */
    public function includePackageTerms(Package $package)
    {
        return $this->collection($package->package_terms, new PackageTermTransformer());
    }
}
