<?php

namespace App\Transformers;

use App\SubscriptionRefund;
use League\Fractal\TransformerAbstract;

class SubscriptionRefundTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param SubscriptionRefund $refund
     * @return array
     */
    public function transform(SubscriptionRefund $refund)
    {
        return $refund->attributesToArray();
    }
}
