<?php

namespace App\Transformers;

use App\Lead;
use League\Fractal\TransformerAbstract;

class LeadTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Lead $lead)
    {
        return $lead->attributesToArray();
    }
}
