<?php

namespace App\Transformers;

use App\User;
use App\Withdrawal;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['subscription', 'appointments', 'employees', 'calendars', 'pending_withdrawals'];

    /**
     * A Fractal transformer.
     *
     * @param User $user
     * @return array
     */
    public function transform(User $user)
    {
        return array_merge($user->attributesToArray(), [
            'is_doctor' => $user->isDoctor(),
            'is_super_admin' => $user->isSuperAdmin(),
            'photo' => $user->photo ? asset('storage/' . $user->photo) : null,
            'cover_photo' => $user->cover_photo ? asset('storage/' . $user->cover_photo) : null,
            'account_status' => $user->getAccountStatus(),
        ]);
    }

    public function includeSubscription(User $user)
    {
        if (($subscription = $user->subscription()) && $user->subscribed()) {
            return $this->item($subscription, new SubscriptionTransformer());
        }
        return null;
    }

    /**
     * @param User $user
     * @return \League\Fractal\Resource\Collection
     */
    public function includeAppointments(User $user)
    {
        return $this->collection($user->appointments, new AppointmentTransformer());
    }

    public function includeEmployees(User $user)
    {
        return $this->collection($user->employees, new UserEmployeeTransformer());
    }

    /**
     * @param User $user
     * @return \League\Fractal\Resource\Collection
     */
    public function includeCalendars(User $user)
    {
        return $this->collection($user->calendars, new UserCalendarTransformer());
    }

    /**
     * @param User $user
     * @return \League\Fractal\Resource\Collection
     */
    public function includePendingWithdrawals(User $user)
    {
        return $this->collection($user->withdrawals()->where('status',  Withdrawal::STATUS_PENDING)->get(), new WithdrawalTransformer());
    }
}
