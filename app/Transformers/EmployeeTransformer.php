<?php

namespace App\Transformers;

use App\Employee;
use League\Fractal\TransformerAbstract;

class EmployeeTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['schedules'];
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Employee $employee) 
    {
        return array_merge($employee->attributesToArray(), [
            'name' => $employee->first_name . ' ' . $employee->last_name,
            'photo' => $employee->photo ? asset('storage/' . $employee->photo) : null
        ]);
    }

    public function includeSchedules(Employee $employee)
    {
        return $this->collection($employee->schedules, new ScheduleTransformer());
    }
}
