<?php

namespace App\Transformers;

use App\Setting;
use League\Fractal\TransformerAbstract;

class SettingTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Setting $setting)
    {
        return $setting->attributesToArray();
    }
}
