<?php

namespace App\Transformers;

use App\AddOn;
use League\Fractal\TransformerAbstract;

class AddOnTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param AddOn $addOn
     * @return array
     */
    public function transform(AddOn $addOn)
    {
        return $addOn->attributesToArray();
    }
}
