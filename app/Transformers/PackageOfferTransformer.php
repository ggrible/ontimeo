<?php

namespace App\Transformers;

use App\PackageOffer;
use League\Fractal\TransformerAbstract;

class PackageOfferTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param PackageOffer $offer
     * @return array
     */
    public function transform(PackageOffer $offer)
    {
        return $offer->attributesToArray();
    }
}
