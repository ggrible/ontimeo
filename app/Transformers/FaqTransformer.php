<?php

namespace App\Transformers;

use App\Faq;
use League\Fractal\TransformerAbstract;

class FaqTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Faq $faq)
    {
        return $faq->attributesToArray();
    }
}
