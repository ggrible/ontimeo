<?php

namespace App\Transformers;

use App\Withdrawal;
use League\Fractal\TransformerAbstract;

class WithdrawalTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['user'];
    
    /**
     * A Fractal transformer.
     *
     * @param Withdrawal $withdrawal
     * @return array
     */
    public function transform(Withdrawal $withdrawal)
    {
        return $withdrawal->attributesToArray();
    }

    public function includeUser(Withdrawal $withdrawal)
    {
        return $this->item($withdrawal->user, new UserTransformer());
    }
}
