<?php

namespace App\Transformers;

use App\Voucher;
use League\Fractal\TransformerAbstract;

class VoucherTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param Voucher $voucher
     * @return array
     */
    public function transform(Voucher $voucher)
    {
        return $voucher->attributesToArray();
    }
}
