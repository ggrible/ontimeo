<?php

namespace App\Transformers;

use App\PackageAddOn;
use League\Fractal\TransformerAbstract;

class PackageAddOnTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['add_on'];
    /**
     * A Fractal transformer.
     *
     * @param PackageAddOn $addOn
     * @return array
     */
    public function transform(PackageAddOn $addOn)
    {
        return $addOn->attributesToArray();
    }

    /**
     * @param PackageAddOn $packageAddOn
     * @return \League\Fractal\Resource\Item
     */
    public function includeAddOn(PackageAddOn $packageAddOn)
    {
        return $this->item($packageAddOn->add_on, new AddOnTransformer());
    }
}
