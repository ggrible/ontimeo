<?php

namespace App\Transformers;

use App\Schedule;
use League\Fractal\TransformerAbstract;

class ScheduleTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['times'];
    /**
     * A Fractal transformer.
     *
     * @param Schedule $schedule
     * @return array
     */
    public function transform(Schedule $schedule)
    {
        return $schedule->attributesToArray();
    }

    /**
     * @param Schedule $schedule
     * @return \League\Fractal\Resource\Collection
     */
    public function includeTimes(Schedule $schedule)
    {
        return $this->collection($schedule->times, new ScheduleTimeTransformer());
    }
}
