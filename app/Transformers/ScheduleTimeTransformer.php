<?php

namespace App\Transformers;

use App\ScheduleTime;
use League\Fractal\TransformerAbstract;

class ScheduleTimeTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param ScheduleTime $scheduleTime
     * @return array
     */
    public function transform(ScheduleTime $scheduleTime)
    {
        return $scheduleTime->attributesToArray();
    }
}
