<?php

namespace App\Transformers;

use App\Transaction;
use League\Fractal\TransformerAbstract;

class TransactionTransformer extends TransformerAbstract
{
    protected $defaultIncludes = ['reference'];

    /**
     * A Fractal transformer.
     *
     * @param Transaction $transaction
     * @return array
     */
    public function transform(Transaction $transaction)
    {
        return $transaction->attributesToArray();
    }

    public function includeReference(Transaction $transaction)
    {
        $reference = $transaction->reference;
        if (!$reference) {
            return null;
        }
        switch ($transaction->type) {
            case Transaction::TYPE_PREPAID_APPOINTMENT:
                return $this->item($reference, new AppointmentTransformer());
            case Transaction::TYPE_SUBSCRIPTION_INVOICE:
                return $this->item($reference, new InvoiceTransformer());
            case Transaction::TYPE_APPOINTMENT_FEE:
                return $this->item($reference, new AppointmentFeeTransformer());
            case Transaction::TYPE_WITHDRAWAL:
                return $this->item($reference, new WithdrawalTransformer());
            case Transaction::TYPE_SUBSCRIPTION_REFUND:
                return $this->item($reference, new SubscriptionRefundTransformer());
            case Transaction::TYPE_WITHDRAWAL_FEE:
                return $this->item($reference, new WithdrawalFeeTransformer());
        }
        return null;
    }
}
