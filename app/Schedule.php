<?php

namespace App;

use App\Traits\SerializesData;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @property Collection times
 * @property string status
 * @property integer id
 * @property integer day
 * @property string avail_to
 * @property string avail_type
 * @property string avail_from
 * @property string type
 * @property string google_event_id
 * @property User user
 * @property integer user_id
 * @property integer is_pre_booking
 * @property float pre_amount
 * @property float full_amount
 *
 */
class Schedule extends Model
{
    use SerializesData;

    protected $guarded = [];

    const TYPE_ONE_ON_ONE = 'one_on_one', TYPE_GROUP = 'group';
    const AVAIL_OPEN_HOURS = 'open_hours', AVAIL_APPOINTMENT_ONLY = 'appointment_only';
    const STATUS_ACTIVE = 'active', STATUS_INACTIVE = 'inactive';
    const INTERVALS = [15,30,60];
    const DAYS = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"];

    protected $casts = [
        'is_pre_booking' => 'boolean',
        'day' => 'integer'
    ];

    public function times()
    {
        return $this->hasMany(ScheduleTime::class);
    }

    public static function getAllStatus(){
        return [self::STATUS_ACTIVE, self::STATUS_INACTIVE];
    }

    public static function getTypes()
    {
        return [self::TYPE_GROUP, self::TYPE_ONE_ON_ONE];
    }

    public static function getAvailTypes()
    {
        return [self::AVAIL_OPEN_HOURS, self::AVAIL_APPOINTMENT_ONLY];
    }

    public static function getIntervals(){
        return self::INTERVALS;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return bool
     */
    public function hasGoogleEvent()
    {
        return !!$this->google_event_id;
    }

    /**
     * @return float
     */
    public function getPreAmountInCents()
    {
        return round($this->pre_amount * 100);
    }
}
