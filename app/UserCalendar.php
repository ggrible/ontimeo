<?php

namespace App;

use App\Traits\Encryptable;
use Carbon\Carbon;
use Google_Service_Calendar;
use Google_Service_Calendar_Calendar;
use Illuminate\Database\Eloquent\Model;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model\Calendar;

/**
 * @property mixed expires_at
 * @property string access_token
 * @property string refresh_token
 * @property string calendar_id
 * @property User user
 * @property string provider
 */
class UserCalendar extends Model
{
    use Encryptable;

    const PROVIDER_GOOGLE = 'google', PROVIDER_OUTLOOK = 'outlook';

    protected $guarded = [];

    protected $encrypts = ['access_token', 'refresh_token'];

    protected $hidden = [
        'access_token', 'refresh_token'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function updateAccessToken()
    {
        switch ($this->provider) {
            case self::PROVIDER_GOOGLE:
                $client = google()->getClient();
                $client->setAccessToken([
                    'access_token' => $this->access_token,
                    'refresh_token' => $this->refresh_token,
                    'expires_in' => Carbon::parse($this->expires_at)->diffInSeconds(now())
                ]);
                $carbon_expires_at = Carbon::parse($this->expires_at);
                if (now()->addMinute(2)->gt($carbon_expires_at)) {
                    $data = $client->fetchAccessTokenWithRefreshToken();
                    $this->update([
                        'access_token' => $data['access_token'],
                        'refresh_token' => $data['refresh_token'],
                        'expires_at' => now()->addSeconds($data['expires_in'])
                    ]);
                }
                break;
            case self::PROVIDER_OUTLOOK:
                $carbon_expires_at = Carbon::parse($this->expires_at);
                if (now()->addMinute(2)->gt($carbon_expires_at) || true) {
                    $client = outlook()->getClient();
                    $newToken = $client->getAccessToken('refresh_token', [
                        'refresh_token' => $this->refresh_token
                    ]);
                    $this->update([
                        'access_token' => $newToken->getToken(),
                        'refresh_token' => $newToken->getRefreshToken(),
                        'expires_at' => Carbon::createFromTimestamp($newToken->getExpires()),
                    ]);
                }
                break;
        }
    }

    /**
     * @return string
     */
    public function getAccessToken()
    {
        $this->updateAccessToken();
        return $this->access_token;
    }

    public function getAccessTokenArray()
    {
        $this->updateAccessToken();
        return [
            'access_token' => $this->access_token,
            'refresh_token' => $this->refresh_token,
            'expires_in' => Carbon::parse($this->expires_at)->diffInSeconds(now())
        ];
    }

    /**
     * @return string
     * @throws \Microsoft\Graph\Exception\GraphException
     */
    public function getCalendarId()
    {
        if (!$this->calendar_id) {
            switch ($this->provider) {
                case self::PROVIDER_GOOGLE:
                    $calendar = new Google_Service_Calendar_Calendar();
                    $calendar->setSummary(config('google-calendar.calendar_name'));
                    $calendar->setTimeZone(config('settings.timezone'));
                    google()->setAccessToken($this->getAccessTokenArray());
                    $calendar = google()->getCalendarService()->calendars->insert($calendar);
                    $this->update([
                        'calendar_id' => $calendar->getId()
                    ]);
                    break;
                case self::PROVIDER_OUTLOOK:
                    $url = "/me/calendars";
                    $graph = new Graph();
                    $graph->setAccessToken($this->getAccessToken());
                    /**
                     * @var Calendar $calendar
                     * @var Calendar $item
                     */
                    $calendars = $graph->createCollectionRequest('get', '/me/calendars')->setReturnType(Calendar::class)->execute();
                    $calendar = null;
                    foreach ($calendars as $item) {
                        if ($item->getName() === config('google-calendar.calendar_name')) {
                            $calendar = $item;
                            break;
                        }
                    }
                    if (!$calendar) {
                        $graph = new Graph();
                        $graph->setAccessToken($this->access_token);
                        $calendar = $graph->createRequest("POST", $url)
                            ->attachBody([
                                'name' => config('google-calendar.calendar_name')
                            ])
                            ->setReturnType(Calendar::class)
                            ->execute();
                    }
                    $this->update([
                        'calendar_id' => $calendar->getId()
                    ]);
                    break;
            }
        }
        return $this->calendar_id;
    }
}
