<?php

namespace App;

use App\Traits\SerializesData;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer length
 */
class Term extends Model
{
    use SerializesData;

    protected $guarded = [];

    /**
     * @return float|int
     */
    public function lengthInYears()
    {
        return $this->length / 12;
    }
}
