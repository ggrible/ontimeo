<?php

namespace App;

use App\Traits\SerializesData;
use Illuminate\Database\Eloquent\Model;

/**
 * @property AddOn add_on
 * @property string stripe_plan_id
 * @property float price
 * @property Package package
 */
class PackageAddOn extends Model
{
    use SerializesData;
    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function add_on()
    {
        return $this->belongsTo(AddOn::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function plans()
    {
        return $this->hasMany(PackageAddOnPlan::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function package()
    {
        return $this->belongsTo(Package::class);
    }

    public function findPlan(PackageTerm $package_term)
    {
        return $this->plans()
            ->where('package_term_id', $package_term->id)
            ->first();
    }
}
