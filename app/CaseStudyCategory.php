<?php

namespace App;

use App\Traits\SerializesData;
use Illuminate\Http\UploadedFile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class CaseStudyCategory extends Model
{
    use SerializesData;
    protected $guarded = [];
    protected $fillable = ['name', 'photo'];

    public function savePhoto(UploadedFile $file)
    {
        $filename = str_random() . '.' . $file->getClientOriginalExtension();
        Storage::putFileAs('public/case-study-category/' . $this->id . '/', $file, $filename);
        $this->photo = '/case-study-category/' . $this->id . '/' . $filename;
        $this->save();
    }
}
