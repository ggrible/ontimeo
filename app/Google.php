<?php

namespace App;

use Carbon\Carbon;
use Google_Client;
use Google_Service_Calendar;
use Google_Service_Calendar_Event;

class Google
{
    /**
     * @var Google_Client
     */
    private $client;
    /**
     * @var Google_Service_Calendar
     */
    private $service;

    /**
     * @var string
     */
    private $calendarId;

    /**
     * Google constructor.
     */
    public function __construct()
    {
        $client = new Google_Client();
        $client->setApplicationName(config('app.name'));
        $client->setClientId(config('services.google.client_id'));
        $client->setClientSecret(config('services.google.client_secret'));
        $client->setAccessType("offline");
        $client->setApprovalPrompt('force');
        $client->addScope(Google_Service_Calendar::CALENDAR);
        $client->setRedirectUri(url('/google/callback'));
        $this->client = $client;
    }

    public function setCalendarId($id)
    {
        $this->calendarId = $id;
    }

    /**
     * @return Google_Client
     */
    public function getClient()
    {
        return $this->client;
    }

    public function setAccessToken(array $access_token)
    {
        $this->getClient()->setAccessToken($access_token);
    }

    /**
     * @return Google_Service_Calendar
     */
    public function getCalendarService()
    {
        if ($this->service) {
            return $this->service;
        }
        return $this->service = new Google_Service_Calendar($this->getClient());
    }

    public function deleteEvent($event_id)
    {
        $this->getCalendarService()->events->delete($this->calendarId, $event_id);
    }

    /**
     * @param $event_id
     * @return Google_Service_Calendar_Event
     */
    public function getEvent($event_id)
    {
        return $this->getCalendarService()
            ->events
            ->get($this->calendarId, $event_id);
    }

    /**
     * @param $from
     * @param $to
     * @param $summary
     * @return Google_Service_Calendar_Event
     */
    public function createEvent($from, $to, $summary)
    {
        $event = new Google_Service_Calendar_Event(array(
            'summary' => $summary,
            'start' => array(
                'dateTime' => $from,
                'timeZone' => 'utc',
            ),
            'end' => array(
                'dateTime' => $to,
                'timeZone' => 'utc',
            ),
            /*'recurrence' => array(
                'RRULE:FREQ=WEEKLY'
            ),*/
        ));
        return $event;
    }

    /**
     * @param Google_Service_Calendar_Event $event
     * @return Google_Service_Calendar_Event
     */
    public function insertEvent(Google_Service_Calendar_Event $event)
    {
        return $this->getCalendarService()->events->insert($this->calendarId, $event);
    }

    /**
     * @param Google_Service_Calendar_Event $event
     * @return Google_Service_Calendar_Event
     */
    public function updateEvent(Google_Service_Calendar_Event $event)
    {
        return $this->getCalendarService()
            ->events
            ->update($this->calendarId, $event->getId(), $event);
    }

    /**
     * @param Carbon $date
     * @return \Google_Service_Calendar_EventDateTime
     */
    public function createEventDateTime(Carbon $date)
    {
        $datetime = new \Google_Service_Calendar_EventDateTime();
        $datetime->setTimeZone('utc');
        $datetime->setDateTime($date->toAtomString());
        return $datetime;
    }
}
