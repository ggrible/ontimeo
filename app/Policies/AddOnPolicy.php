<?php

namespace App\Policies;

use App\AddOn;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AddOnPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function before(User $user, $ability)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
    }

    public function create(User $user)
    {

    }

    public function update(User $user, AddOn $addOn)
    {

    }

    public function delete(User $user, AddOn $addOn)
    {

    }
}
