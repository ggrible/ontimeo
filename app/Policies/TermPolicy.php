<?php

namespace App\Policies;

use App\Term;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TermPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    public function before(User $user, $ability)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
    }

    public function create(User $user)
    {

    }

    public function update(User $user, Term $term)
    {

    }

    public function delete(User $user, Term $term)
    {

    }
}
