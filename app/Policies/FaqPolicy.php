<?php

namespace App\Policies;

use App\Faq;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class FaqPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function before(User $user, $ability)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
    }

    public function create(User $user)
    {

    }

    public function update(User $user, Faq $faq)
    {

    }

    public function delete(User $user, Faq $faq)
    {

    }
}
