<?php

namespace App;

use App\Traits\SerializesData;
use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    use SerializesData;
    
    protected $fillable = [
        'title',
        'sub_title',
        'description'
    ];
}
