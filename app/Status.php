<?php

namespace App;

class Status
{
    const ACTIVE = 'active', INACTIVE = 'inactive';

    /**
     * @return array
     */
    public static function statuses()
    {
        return [self::ACTIVE, self::INACTIVE];
    }
}
