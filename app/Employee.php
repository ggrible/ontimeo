<?php

namespace App;

use App\Traits\SerializesData;
use Illuminate\Http\UploadedFile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Employee extends Model
{
    use SerializesData;

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'job_title', 
        'telephone_ext',
        'photo'
    ];

    public function savePhoto(UploadedFile $file)
    {
        $filename = str_random() . '.' . $file->getClientOriginalExtension();
        Storage::putFileAs('public/employees/' . $this->id, $file, $filename);
        $this->photo = 'employees/' . $this->id . '/' . $filename;
        $this->save();
    }

    public function schedules()
    {
        return $this->hasMany(Schedule::class, 'employee_id', 'id');
    }
}
