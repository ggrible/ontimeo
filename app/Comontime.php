<?php

namespace App;

class Comontime
{
    public static function scriptVariables()
    {
        return [
            'state' => [
                'user' => auth()->check() ? auth()->user()->getSerializedData(null, 'calendars') : null,
            ]
        ];
    }
}
