<?php

namespace App;

use App\Traits\SerializesData;
use Google_Service_Calendar_Calendar;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Storage;
use Laravel\Cashier\Billable;
use Silber\Bouncer\Database\HasRolesAndAbilities;
use Stripe\Customer;

/**
 * @property string last_name
 * @property string first_name
 * @property string name
 * @property Collection subscriptions
 * @property string stripe_id
 * @property string photo
 * @property string cover_photo
 * @property Collection appointments
 * @property integer id
 * @property Collection invoices
 * @property float personal_balance
 * @property Collection calendars
 */
class User extends Authenticatable
{
    use Notifiable, HasRolesAndAbilities, SerializesData;
    use Billable {
        updateCard as billableUpdateCard;
    }

    const STATUS_ACTIVE = 'active', STATUS_INACTIVE = 'inactive';
    const ROLE_DOCTOR = 'doctor', ROLE_SUPER_ADMIN = 'super-admin';
    const ACCOUNT_STATUS_ACTIVE = 'active', ACCOUNT_STATUS_INACTIVE = 'inactive', ACCOUNT_STATUS_CANCELLED = 'cancelled';

    protected $guarded = [];
    protected $appends = ['is_super_admin'];

    protected $casts = [
        'personal_balance' => 'float'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return bool
     */
    public function isDoctor()
    {
        return $this->roles()
            ->where('name', self::ROLE_DOCTOR)
            ->count() > 0;
    }

    /**
     * @return bool
     */
    public function isSuperAdmin()
    {
        return $this->roles()
            ->where('name', self::ROLE_SUPER_ADMIN)
            ->count() > 0;
    }

    public function getIsSuperAdminAttribute()
    {
        return $this->isSuperAdmin();
    }

    /**
     * Get all of the subscriptions for the Stripe model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subscriptions()
    {
        return $this->hasMany(Subscription::class, $this->getForeignKey())->orderBy('created_at', 'desc');
    }

    /**
     * @param string $subscription
     * @return Subscription
     */
    public function subscription($subscription = 'default')
    {
        return $this->subscriptions->sortByDesc(function ($value) {
            return $value->created_at->getTimestamp();
        })
            ->first(function ($value) use ($subscription) {
                return $value->name === $subscription;
            });
    }

    public function updateCard($token)
    {
        if (!$this->stripe_id) {
            $this->createAsStripeCustomer($token);
        } else {
            $this->billableUpdateCard($token);
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function password_setup_tokens()
    {
        return $this->hasMany(PasswordSetupToken::class);
    }

    public function schedules()
    {
        return $this->hasMany(Schedule::class);
    }

    /**
     * @param UploadedFile $file
     */
    public function savePhoto(UploadedFile $file)
    {
        $filename = str_random() . '.' . $file->getClientOriginalExtension();
        Storage::putFileAs('public/users/' . $this->id, $file, $filename);
        $this->photo = 'users/' . $this->id . '/' . $filename;
        $this->save();
    }

    /**
     * @param UploadedFile $file
     */
    public function saveCoverPhoto(UploadedFile $file)
    {
        $filename = str_random() . '.' . $file->getClientOriginalExtension();
        Storage::putFileAs('public/users/' . $this->id, $file, $filename);
        $this->cover_photo = 'users/' . $this->id . '/' . $filename;
        $this->save();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function calendars()
    {
        return $this->hasMany(UserCalendar::class);
    }

    /**
     * @param $provider
     * @return UserCalendar
     */
    public function getCalendar($provider)
    {
        return $this->calendars()
            ->where('provider', $provider)
            ->first();
    }

    public function scopeIsNotAdmin($query)
    {
        return $query->whereHas('roles', function ($q) {
            $q->where('name', self::ROLE_DOCTOR);
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function appointments()
    {
        return $this->hasMany(Appointment::class, 'doctor_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    /**
     * @return float
     */
    public function getTotalPrepaidAppointmentAmount()
    {
        return round($this->appointments()
            ->sum('amount'), 2);
    }

    /**
     * @return float
     */
    public function getTotalUpcomingAppointmentAmount()
    {
        return round($this->appointments()->where('appointment_datetime', '>', now())->sum('amount'), 2);
    }

    public function addPersonalBalance($amount)
    {
        $this->update([
            'personal_balance' => round($this->personal_balance + $amount, 2)
        ]);
    }

    public function subtractPersonalBalance($amount)
    {
        $this->update([
            'personal_balance' => round($this->personal_balance - $amount, 2)
        ]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function withdrawals()
    {
        return $this->hasMany(Withdrawal::class);
    }

    public function getAccountStatus()
    {
        $subscription = $this->subscription();
        if (!$subscription) {
            return self::ACCOUNT_STATUS_INACTIVE;
        }
        if (!$subscription->active()) {
            return self::ACCOUNT_STATUS_CANCELLED;
        }
        return self::ACCOUNT_STATUS_ACTIVE;
    }

    /**
     * @return Invoice
     */
    public function latestInvoice()
    {
        return $this->invoices()
            ->latest()
            ->first();
    }

    public function employees()
    {
        return $this->hasMany(UserEmployee::class, 'user_id');
    }

    public function syncCalendarAppointments()
    {
        $this->appointments()->each(function (Appointment $appointment) {
            $appointment->syncCalendarEvent();
        });
    }
}
