<?php

namespace App;

use App\Traits\SerializesData;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model\Event;

/**
 * @property string google_event_id
 * @property Schedule schedule
 * @property string avail_time
 * @property string outlook_event_id
 */
class ScheduleTime extends Model
{
    use SerializesData;

    protected $guarded = [];
    protected $appends = ['is_valid'];

    public function hasGoogleEvent()
    {
        return !!$this->google_event_id;
    }

    public function hasOutlookEvent()
    {
        return !!$this->outlook_event_id;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function schedule()
    {
        return $this->belongsTo(Schedule::class);
    }

    /**
     * @throws \Microsoft\Graph\Exception\GraphException
     */
    public function syncCalendarEvent()
    {
        $this->syncGoogleCalendarEvent();
        $this->syncOutlookCalendarEvent();
    }

    /**
     * @return Carbon
     */
    public function getTimeCarbon()
    {
        return Carbon::parse($this->avail_time, config('settings.timezone'));
    }

    /**
     * @throws \Microsoft\Graph\Exception\GraphException
     */
    private function syncOutlookCalendarEvent()
    {
        if (!($calendar = $this->schedule->user->getCalendar(UserCalendar::PROVIDER_OUTLOOK))) {
            return;
        }
        if (!$this->hasOutlookEvent()) {
            $graph = new Graph();
            $graph->setAccessToken($calendar->access_token);
            $getEventsUrl = '/me/calendars/' . $calendar->getCalendarId() . '/events';
            $carbon_time = move_to_next_day($this->getTimeCarbon(), $this->schedule->day);
            $summary = "Schedule: " . $this->schedule->type . "|" . $this->schedule->avail_type;
            /**
             * @var Event $event
             */
            $event = $graph->createRequest('POST', $getEventsUrl)
                ->attachBody([
                    'subject' => $summary,
                    'body' => [
                        'content' => $summary
                    ],
                    'start' => [
                        'dateTime' => $carbon_time->toAtomString(),
                        'timeZone' => 'utc'
                    ],
                    'end' => [
                        'dateTime' => $carbon_time->toAtomString(),
                        'timeZone' => 'utc'
                    ],
                    'recurrence' => [
                        'pattern' => [
                            'type' => 'weekly',
                            'interval' => 1,
                            'daysOfWeek' => [Schedule::DAYS[$this->schedule->day - 1]]
                        ],
                        'range' => [
                            'type' => 'noEnd',
                            'startDate' => $carbon_time->toDateString()
                        ]
                    ]
                ])
                ->setReturnType(Event::class)
                ->execute();
            $this->update([
                'outlook_event_id' => $event->getId()
            ]);
        }
    }

    /**
     * @throws \Microsoft\Graph\Exception\GraphException
     */
    private function syncGoogleCalendarEvent()
    {
        if (!($calendar = $this->schedule->user->getCalendar(UserCalendar::PROVIDER_GOOGLE))) {
            return;
        }
        if (!$this->hasGoogleEvent()) {
            google()->setCalendarId($calendar->getCalendarId());
            google()->setAccessToken($calendar->getAccessTokenArray());
            $carbon_time = move_to_next_day($this->getTimeCarbon(), $this->schedule->day);
            $summary = "Schedule: " . $this->schedule->type . "|" . $this->schedule->avail_type;
            $event = google()->createEvent($carbon_time->toAtomString(), $carbon_time->toAtomString(), $summary);
            $event = google()->insertEvent($event);
            $this->update([
                'google_event_id' => $event->getId()
            ]);
        }
    }

    /**
     * @param Carbon $date
     * @return bool
     */
    public function isAvailable(Carbon $date)
    {
        $date = $date->copy()->setTimeFromTimeString($this->avail_time);
        if (now()->gte($date)) {
            return false;
        }
        if (!$date->isDayOfWeek($this->schedule->day == 7 ? 0 : $this->schedule->day)) {
            return false;
        }
        if ($this->schedule->type === Schedule::TYPE_GROUP) {
            return true;
        }
        return $this->schedule
                ->user
                ->appointments()
                ->where('appointment_datetime', $date->toDateTimeString())
                ->count() == 0;
    }

    public function getIsValidAttribute()
    {
        // $day = date('w') - 1;
        // $week_start = date('Y-m-d', strtotime('-'.$day.' days')); // start date of the week
        // $date = strtotime($week_start . ' +'. ($this->schedule->day - 1) .' days'); // date of this schedule time
        // $date =  date('Y-m-d', $date);
        return true;
        // return $this->isAvailable(Carbon::parse($date));
    }
}
