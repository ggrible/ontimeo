<?php

namespace App\Http\Controllers;

use App\CaseStudy;
use Illuminate\Http\Request;
use App\Transformers\CaseStudyTransformer;

class CaseStudyController extends Controller
{
    public function index()
    {
        return fractal()
        ->collection(CaseStudy::all(), new CaseStudyTransformer())
        ->respond();
    }

    public function store(Request $request)
    {
        $this->authorize('create', CaseStudy::class);

        $this->validate($request, [
            'user_id' => 'required|exists:users,id',
            'description' => 'nullable|string|max:65535'
        ]);

        $caseStudy = CaseStudy::query()
            ->create($request->only(['user_id', 'description']));
        
        return $caseStudy->getSerializedData();
    }

    public function update(Request $request, CaseStudy $caseStudy)
    {
        $this->authorize('update', $caseStudy);
        
        $this->validate($request, [
            'user_id' => 'required|exists:users,id',
            'description' => 'nullable|string|max:65535'
        ]);

        $caseStudy->update($request->only(['user_id', 'description']));
        return $caseStudy->getSerializedData();
    }

    public function destroy(CaseStudy $caseStudy)
    {
        $this->authorize('delete', $caseStudy);
        $caseStudy->delete();
    }
}
