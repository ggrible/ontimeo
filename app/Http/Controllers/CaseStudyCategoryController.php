<?php

namespace App\Http\Controllers;

use App\CaseStudyCategory;
use Illuminate\Http\Request;
use App\Transformers\CaseStudyCategoryTransformer;

class CaseStudyCategoryController extends Controller
{
    public function index()
    {
        return fractal()
        ->collection(CaseStudyCategory::all(), new CaseStudyCategoryTransformer())
        ->respond();
    }

    public function store(Request $request)
    {
        $this->authorize('create', CaseStudyCategory::class);

        $this->validate($request, [
            'photo' => 'required|image',
            'name' => 'required|string|max:255'
        ]);

        $caseStudyCategory = CaseStudyCategory::query()
            ->create($request->only(['name']));

        if ($request->hasFile('photo')) {
            $caseStudyCategory->savePhoto($request->file('photo'));
        }
        
        return $caseStudyCategory->getSerializedData();
    }

    public function update(Request $request, CaseStudyCategory $caseStudyCategory)
    {
        $this->authorize('update', $caseStudyCategory);
        
        $this->validate($request, [
            'name' => 'required|string|max:255'
        ]);

        $caseStudyCategory->update($request->only(['name']));

        if ($request->hasFile('photo')) {
            $caseStudyCategory->savePhoto($request->file('photo'));
        }

        return $caseStudyCategory->getSerializedData();
    }

    public function destroy(CaseStudyCategory $caseStudyCategory)
    {
        $this->authorize('delete', $caseStudyCategory);
        $caseStudyCategory->delete();
    }
}
