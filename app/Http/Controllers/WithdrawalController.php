<?php

namespace App\Http\Controllers;

use App\Http\Requests\Withdrawal\ApproveRequest;
use App\Http\Requests\Withdrawal\StoreRequest;
use App\Transformers\WithdrawalTransformer;
use App\Withdrawal;
use Illuminate\Http\Request;

class WithdrawalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $withdrawals = new Withdrawal;
        if ($request->has('status')) {
            $withdrawals = $withdrawals->where('status', $request->get('status'));
        }
        $withdrawals = $withdrawals->get();
        return fractal()
            ->collection($withdrawals, new WithdrawalTransformer())
            ->respond();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        /**
         * @var Withdrawal $withdrawal
         */
        $withdrawal = $request->user()->withdrawals()
            ->create([
                'amount' => $request->get('amount')
            ]);
        return $withdrawal->getSerializedData();
    }

    /**
     * Display the specified resource.
     *
     * @param Withdrawal $withdrawal
     * @return array
     */
    public function show(Withdrawal $withdrawal)
    {
        return $withdrawal->getSerializedData();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param ApproveRequest $request
     * @param Withdrawal $withdrawal
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function approve(ApproveRequest $request, Withdrawal $withdrawal)
    {
        if ($withdrawal->user->personal_balance < $withdrawal->amount) {
            throw new \Exception("Can't withdraw amount greater than personal balance of user.");
        }
        transaction(function () use ($request, $withdrawal) {
            $withdrawal->update([
                'status' => Withdrawal::STATUS_DONE
            ]);
        });
        return fractal($withdrawal, new WithdrawalTransformer())->respond();
    }
}
