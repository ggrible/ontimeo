<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function updatePhoto(Request $request)
    {
        $this->validate($request, [
            'photo' => 'required|image',
        ]);
        $request->user()->savePhoto($request->file('photo'));
    }
    public function updateCoverPhoto(Request $request)
    {
        $this->validate($request, [
            'photo' => 'required|image',
        ]);
        $request->user()->saveCoverPhoto($request->file('photo'));
    }

    public function updateDetails(Request $request)
    {
        $this->validate($request, [
            'company_name' => 'required|string|max:255',
            'first_name' => 'nullable|string|max:255',
            'last_name' => 'nullable|string|max:255',
            'company_industry' =>'required|exists:case_study_categories,id',
            'company_bio' => 'nullable|string|max:65535',
            'photo' => 'nullable|image',
        ]);
        transaction(function () use ($request) {
            $request->user()->update($request->only(['company_name', 'company_industry', 'company_bio']));
            if ($request->hasFile('photo')) {
                $request->user()->savePhoto($request->file('photo'));
            }
            if ($request->filled('first_name')) {
                $request->user()->update([
                    'first_name' => $request->get('first_name')
                ]);
            }
            if ($request->filled('last_name')) {
                $request->user()->update([
                    'last_name' => $request->get('last_name')
                ]);
            }
        });
    }

    public function cancelSubscription(Request $request)
    {
        /**
         * @var User $user
         */
        $user = $request->user();
        $user->subscription()->cancel();
    }
}
