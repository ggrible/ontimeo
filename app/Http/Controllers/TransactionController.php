<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\Transformers\TransactionTransformer;
use App\User;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class TransactionController extends Controller
{
    public function index(Request $request)
    {
        $this->validate($request, [
            'type' => 'nullable|in:' . implode(',', Transaction::getTypes())
        ]);
        $query = Transaction::query();
        $query->latest();
        if ($request->has('type')) {
            $query->where('type', $request->get('type'));
        }
        if ($request->user()->isSuperAdmin()) {

        } else {
            $query->where('user_id', $request->user()->id);
        }
        $pagination = $query->paginate();
        return fractal()
            ->collection($pagination->items(), new TransactionTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($pagination))
            ->respond();
    }

    public function getSummary(Request $request)
    {
        /**
         * @var User $user
         */
        $user = $request->user();
        $prepaid_appointments = $user->getTotalPrepaidAppointmentAmount();
        $free_appointments = $user->appointments()
            ->whereNull('stripe_charge_id')
            ->where('appointment_datetime', '>', now()->toDateTimeString())
            ->count();

        return [
            'prepaid_appointments' => $prepaid_appointments,
            'free_appointments' => $free_appointments,
            'personal_balance' => $user->personal_balance,
            'active_appointments' => $user->getTotalUpcomingAppointmentAmount(),
        ];
    }
}
