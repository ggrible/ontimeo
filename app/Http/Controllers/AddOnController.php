<?php

namespace App\Http\Controllers;

use App\AddOn;
use App\Jobs\CreateStripeProductForAddOn;
use App\Status;
use App\Transformers\AddOnTransformer;
use Illuminate\Http\Request;

class AddOnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return fractal()
            ->collection(AddOn::all(), new AddOnTransformer())
            ->respond();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(Request $request)
    {
        $this->authorize('create', AddOn::class);
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'description' => 'nullable|string|max:65535',
            'price' => 'required|numeric',
            'status' => 'required|string|in:' . implode(',', Status::statuses())
        ]);
        /**
         * @var AddOn $add_on
         */
        $add_on = AddOn::query()
            ->create($request->only(['name', 'description', 'price', 'status']));
        dispatch(new CreateStripeProductForAddOn($add_on));
        return $add_on->getSerializedData();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param AddOn $add_on
     * @return array
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request, AddOn $add_on)
    {
        $this->authorize('update', $add_on);
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'description' => 'nullable|string|max:65535',
            'price' => 'required|numeric',
            'status' => 'required|string|in:' . implode(',', Status::statuses())
        ]);
        /**
         * @var AddOn $add_on
         */
        $add_on->update($request->only(['name', 'description', 'price', 'status']));
        return $add_on->getSerializedData();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param AddOn $add_on
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Exception
     */
    public function destroy(AddOn $add_on)
    {
        $this->authorize('delete', $add_on);
        if ($add_on->is_default) {
            throw new \Exception("Can't delete default add ons.");
        }
        $add_on->delete();
    }
}
