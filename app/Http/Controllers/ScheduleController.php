<?php

namespace App\Http\Controllers;

use App\Http\Requests\Schedule\StoreRequest;
use App\Schedule;
use App\ScheduleTime;
use App\Transformers\ScheduleTransformer;
use App\User;
use App\UserCalendar;
use Carbon\Carbon;
use Eluceo\iCal\Component\Calendar;
use Eluceo\iCal\Component\Event;
use Eluceo\iCal\Property\Event\RecurrenceRule;
use Google_Service_Calendar;
use Google_Service_Calendar_Event;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $schedules = new Schedule;
        $user_id = $request->has('custom_number') ? 
        User::where('custom_number', $request->get('custom_number'))->first()->id : $request->user_id;

        if ($request->has('user_id') && !$request->has('employee_id')) {
            $schedules = $schedules
            ->where('user_id', $user_id)
            ->whereNull('employee_id');
        }

        if ($request->has('employee_id')) {
            $schedules = $schedules
            ->where('user_id', $user_id)
            ->where('employee_id', $request->employee_id);
        }

        $schedules = $schedules->get();

        return fractal()
            ->collection($schedules, new ScheduleTransformer())
            ->respond();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return array
     */
    public function store(StoreRequest $request)
    {
        /**
         * @var User $user
         * @var Schedule $schedule
         */
        $user = $request->user();

        $schedule = null;

        transaction(function () use ($request, $user, &$schedule) {
            /**
             * @var ScheduleTime $schedule_time
             * @var UserCalendar $google_calendar
             */

            $day = $request->get('day');
            $type = $request->get('type');
            $interval = $request->get('interval');
            $is_pre_booking = $request->get('is_pre_booking');
            $pre_amount = $request->get('pre_amount');
            $full_amount = $request->get('full_amount');
            $avail_type = $request->get('avail_type');
            $avail_from = $request->get('avail_from');
            $avail_to = $request->get('avail_to');
            $avail_times = $request->get('avail_times');
            $status = $request->get('status');
            $employee_id = $request->get('employee_id');

            if ($request->has('employee_id')) {
                $schedule = $user->schedules()
                ->where('day', $day)
                ->where('employee_id', $request->get('employee_id'))
                ->first();
            } else {
                $schedule = $user->schedules()
                ->where('day', $day)
                ->first();
            }

            if (!$schedule) {
                $schedule = new Schedule();
            }

            $schedule->user_id = $user->id;
            $schedule->day = $day;
            $schedule->type = $type;
            $schedule->interval = $interval;
            $schedule->is_pre_booking = $is_pre_booking;
            $schedule->status = $status;
            $schedule->employee_id = $employee_id;

            if ($is_pre_booking) {
                $schedule->pre_amount = $pre_amount;
                $schedule->full_amount = $full_amount;
            }

            $schedule->avail_type = $avail_type;
            if ($avail_type == Schedule::AVAIL_OPEN_HOURS) {
                $schedule->avail_from = $avail_from;
                $schedule->avail_to = $avail_to;
                $schedule->save();
                /*$schedule->times->each(function (ScheduleTime $time) {
                    $time->delete();
                });*/
                $carbon_from = Carbon::parse($avail_from);
                $carbon_to = Carbon::parse($avail_to);
                $times = [];
                do {
                    $times[] = $time = $carbon_from->toTimeString();
                    if ($schedule->times()->where('avail_time', $time)->count() === 0) {
                        $schedule->times()
                            ->create([
                                'avail_time' => $carbon_from->toTimeString()
                            ]);
                    }
                } while ($carbon_from->addMinutes($interval)->lte($carbon_to));
                $schedule->times()
                    ->whereNotIn('avail_time', $times)
                    ->get()
                    ->each(function (ScheduleTime $time) {
                        $time->delete();
                    });
            } else {
                $schedule->save();
                if ($request->has('avail_times')) {
                    $time_strings = [];
                    foreach ($avail_times as $key => $val) {
                        $carbon_time = Carbon::parse($val);
                        $schedule->times()
                            ->firstOrCreate([
                                'avail_time' => $carbon_time->toTimeString()
                            ]);
                        $time_strings[] = $carbon_time->toTimeString();
                    }
                    $schedule->times()
                        ->whereNotIn('avail_time', $time_strings)
                        ->get()
                        ->each(function (ScheduleTime $item) {
                            $item->delete();
                        });
                } else {
                    $schedule->times->each(function (ScheduleTime $time) {
                        $time->delete();
                    });
                }
            }
        });
        return $schedule->getSerializedData();
    }

    public function downloadICalendar(Request $request)
    {
        /**
         * @var Collection $schedules
         * @var Schedule $schedule
         * @var User $user
         */
        $user = $request->user();
        $schedules = $user->schedules()
            ->where('status', Schedule::STATUS_ACTIVE)
            ->get();

        $vCalendar = new Calendar('www.example.com');

        foreach ($schedules as $schedule) {

            if ($schedule->avail_type === Schedule::AVAIL_OPEN_HOURS) {

                $vEvent = new Event();

                $avail_from_time_split = explode(":", $schedule->avail_from);
                $avail_from = new \DateTime();
                $avail_from->modify(Schedule::DAYS[$schedule->day - 1]);
                $avail_from->setTime($avail_from_time_split[0], $avail_from_time_split[1]);

                $avail_to_time_split = explode(":", $schedule->avail_to);
                $avail_to = new \DateTime();
                $avail_to->modify(Schedule::DAYS[$schedule->day - 1]);
                $avail_to->setTime($avail_to_time_split[0], $avail_to_time_split[1]);

                $vEvent->setDtStart($avail_from);
                $vEvent->setDtEnd($avail_to);

                $vEvent->setSummary("Schedule: " . $schedule->type . "|" . $schedule->avail_type);
                $recurrenceRule = new RecurrenceRule();
                $recurrenceRule->setFreq(RecurrenceRule::FREQ_WEEKLY);
                $recurrenceRule->setInterval(1);
                $vEvent->setRecurrenceRule($recurrenceRule);
                $vEvent->setUseUtc(false);

                $vCalendar->addComponent($vEvent);

            } elseif ($schedule->avail_type === Schedule::AVAIL_APPOINTMENT_ONLY) {

                foreach ($schedule->times as $time) {

                    $vEvent = new Event();

                    $avail_time_time_split = explode(":", $time->avail_time);
                    $avail_time = new \DateTime();
                    $avail_time->modify(Schedule::DAYS[$schedule->day - 1]);
                    $avail_time->setTime($avail_time_time_split[0], $avail_time_time_split[1]);

                    $vEvent->setDtStart($avail_time);
                    $vEvent->setDtEnd($avail_time);

                    $vEvent->setSummary("Schedule: " . $schedule->type . "|" . $schedule->avail_type);
                    $recurrenceRule = new RecurrenceRule();
                    $recurrenceRule->setFreq(RecurrenceRule::FREQ_WEEKLY);
                    $recurrenceRule->setInterval(1);
                    $vEvent->setRecurrenceRule($recurrenceRule);
                    $vEvent->setUseUtc(false);

                    $vCalendar->addComponent($vEvent);
                }
            }


        }
        $filename = "clinic_schedules";
        header('Content-Type: text/calendar; charset=utf-8');
        header('Content-Disposition: attachment; filename="' . $filename . '.ics"');

        echo $vCalendar->render();
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param Schedule $schedule
     * @return array
     */
    public function show(Request $request, Schedule $schedule)
    {
        return $schedule->getSerializedData();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function setStatus(Request $request, Schedule $schedule)
    {
        $this->validate($request, [
            'status' => 'required|in:' . implode(",", Schedule::getAllStatus())
        ]);
        $schedule->status = $request->get('status');
        $schedule->save();
    }

    public function saveEventToGoogleCalendar(Schedule $schedule)
    {

        /**
         * @var UserCalendar $user_calendar
         */
        $user_calendar = UserCalendar::query()->find(1);

        google()->syncCalendar($user_calendar, $schedule);

    }

    public function getScheduleByDate(Request $request)
    {
        $dateStart = $request->has('date_start') ? date('Y-m-d', strtotime($request->get('date_start') . " +1 days")) : now()->toDateString();
        $time = strtotime($dateStart);
        $daysLeftOfCurrentMonth = date('t') - date('d');

        $user = $request->has('custom_number') ? User::where('custom_number', $request->get('custom_number'))
        ->first() : $request->user();

        $dates = [];
        for ($i = 0; $i <= 30 ; $i++) {
            
            $date = $dateStart;
            $date = strtotime($date . ' +'.$i.' days');

            $weekDayNumber = date('w', $date) == 0 ? 7 : date('w', $date);

            $dayNumber = $i + 1;
    
            if ($request->has('employee_id')) {
                $schedule = \App\Schedule::where('day', $weekDayNumber)
                ->where('user_id',  $user->id)
                ->where('employee_id', $request->get('employee_id'));
            } else {
                $schedule = \App\Schedule::where('day', $weekDayNumber)
                ->where('user_id',  $user->id)
                ->whereNull('employee_id');
            }

            $schedule = $schedule->first();
    
            $dates[$i] = [
                'date' => date('Y-m-d', $date),
                'date_human_format' => date('F d, Y', $date),
                'month' => date('m', $date),
                'month_full_format' => date('F', $date),
                'week_date' => date('d', $date),
                'week_day_full_format' => date('l', $date),
                'week_day_three_format' => date('D', $date),
                'day' => $weekDayNumber,
                'year' => date('Y', $date),
                'is_active' => optional($schedule)->status == 'active' ? true : false,
                'hasPassed' => false,
                'schedule_id' => optional($schedule)->id
            ];
        }
        return $dates;
    }
}
