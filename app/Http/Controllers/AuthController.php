<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function user(Request $request)
    {
        return $request->user()->getSerializedData();
    }

    public function loggedInFirstTime(Request $request)
    {
        $request->user()
            ->update([
                'logged_in_first_time' => true
            ]);
    }
}
