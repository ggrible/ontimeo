<?php

namespace App\Http\Controllers;

use App\Lead;
use Illuminate\Http\Request;
use App\Transformers\LeadTransformer;

class LeadController extends Controller
{
    public function index()
    {
        return fractal()
            ->collection(Lead::all(), new LeadTransformer())
            ->respond();
    }

    public function store(Request $request)
    {
        $this->authorize('create', Lead::class);
        $this->validate($request, [
            'company' => 'required|string|max:255',
            'url' => 'required|string|max:255',
            'ceo_company' => 'required|string|max:255',
            'telephone' => 'required|string|max:255',
            'email' => 'required|string|max:255|email',
            'company_type' => 'required|string|max:255'
        ]);
        $lead = Lead::query()
            ->create($request->only(['company', 'url', 'ceo_company', 'telephone', 'email', 'company_type']));
        return $lead->getSerializedData();
    }

    public function update(Request $request, Lead $lead)
    {
        $this->authorize('update', $lead);
        $this->validate($request, [
            'company' => 'required|string|max:255',
            'url' => 'required|string|max:255',
            'ceo_company' => 'required|string|max:255',
            'telephone' => 'required|string|max:255',
            'email' => 'required|string|max:255|email',
            'company_type' => 'required|string|max:255'
        ]);
        $lead->update($request->only(['company', 'url', 'ceo_company', 'telephone', 'email', 'company_type']));
        return $lead->getSerializedData();
    }

    public function destroy(Lead $lead)
    {
        $this->authorize('delete', $lead);
        $lead->delete();
    }

    public function profile($company)
    {
        $lead = Lead::where('company', $company)
        ->first();
        if ($lead) {
            return view('lead', [
                'lead' => $lead
            ]);
        }
        abort(404);
    }
}
