<?php

namespace App\Http\Controllers;

use App\Http\Requests\Package\StoreRequest;
use App\Http\Requests\Package\SubscribeRequest;
use App\Http\Requests\Package\UpdateRequest;
use App\Jobs\CreateStripeProductForPackage;
use App\Notifications\SetupPasswordNotification;
use App\Package;
use App\PackageAddOn;
use App\PackageTerm;
use App\Subscription;
use App\Transformers\PackageTransformer;
use App\User;
use Illuminate\Http\Request;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return fractal()
            ->collection(Package::all(), new PackageTransformer())
            ->respond();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return array
     * @throws \Exception
     */
    public function store(StoreRequest $request)
    {
        beginTransaction();
        try {
            /**
             * @var Package $package
             */
            $package = Package::query()
                ->create($request->only(['name', 'description', 'is_featured', 'status', 'domain_additional_charge', 'promo']));
            dispatch(new CreateStripeProductForPackage($package));
            $package->saveOffers($request->get('offers'));
            $package->saveTerms($request->get('terms'));
            $package->saveAddOns($request->get('add_ons'));
            commit();
            return $package->getSerializedData();
        } catch (\Exception $exception) {
            rollback();
            throw $exception;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Package $package
     * @return array
     */
    public function show(Package $package)
    {
        return $package->getSerializedData();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param Package $package
     * @return array
     * @throws \Exception
     */
    public function update(UpdateRequest $request, Package $package)
    {
        beginTransaction();
        try {
            /**
             * @var Package $package
             */
            $package->update($request->only(['name', 'description', 'is_featured', 'status', 'domain_additional_charge', 'promo']));
            if ($request->has('offers')) {
                $package->saveOffers($request->get('offers'));
            }
            $package->saveTerms($request->get('terms'));
            $package->saveAddOns($request->get('add_ons'));
            commit();
            return $package->refresh()->getSerializedData();
        } catch (\Exception $exception) {
            rollback();
            throw $exception;
        }
    }

    /**
     * Remove the specified resource from storage.s
     *
     * @param Package $package
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Exception
     */
    public function destroy(Package $package)
    {
        $this->authorize('delete', $package);
        $package->delete();
    }

    /**
     * @param SubscribeRequest $request
     * @param Package $package
     * @throws \Exception
     */
    public function subscribe(SubscribeRequest $request, Package $package)
    {
        beginTransaction();
        try {
            /**
             * @var User $user
             * @var PackageTerm $package_term
             * @var PackageAddOn $package_add_on
             */
            $user = User::query()
                ->create(array_merge($request->only(['first_name', 'last_name', 'email', 'mobile_number', 'landline_number', 'custom_number']), [
                    'password' => bcrypt(str_random()),
                    'is_password_setup' => false,
                ]));
            $user->assign(User::ROLE_DOCTOR);
            $user->password_setup_tokens()
                ->create([
                    'token' => bcrypt($code = str_random(60))
                ]);
            $user->notify(new SetupPasswordNotification($code, $user->email));
            $user->updateCard($request->get('card_token'));
            $customer = $user->asStripeCustomer();
            $package_term = PackageTerm::query()->find($request->get('package_term_id'));
            $plans = [];
            $plans[] = [
                'plan' => $package_term->stripe_plan_id,
            ];
            foreach ($request->get("package_add_ons") as $package_add_on_id) {
                $package_add_on = PackageAddOn::query()->find($package_add_on_id);
                $plans[] = [
                    'plan' => $package_add_on->findPlan($package_term)->stripe_plan_id
                ];
            }
            $trial_end = $package->getNewTrialEnd();
            $stripe_subscription = \Stripe\Subscription::create([
                'customer' => $customer->id,
                'items' => $plans,
                'trial_end' => $trial_end ? $trial_end->getTimestamp() : null,
                'coupon' => $request->has('coupon_code') ? $request->voucher()->stripe_id  : null
            ]);
            /**
             * @var Subscription $subscription
             */
            $subscription = $user->subscriptions()
                ->create([
                    'name' => 'default',
                    'stripe_id' => $stripe_subscription->id,
                    'stripe_plan' => $package_term->stripe_plan_id,
                    'quantity' => 1,
                    'package_term_id' => $package_term->id,
                    'trial_ends_at' => $trial_end
                ]);
            foreach ($request->get("package_add_ons") as $package_add_on_id) {
                $package_add_on = PackageAddOn::query()->find($package_add_on_id);
                $subscription->subscription_add_ons()
                    ->create([
                        'package_add_on_id' => $package_add_on_id,
                        'stripe_plan_id' => $package_add_on->findPlan($package_term)->stripe_plan_id
                    ]);
            }
            commit();
        } catch (\Exception $exception) {
            rollback();
            throw $exception;
        }
    }

    public function getCountSummary()
    {
        $active = 0;
        User::query()->isNotAdmin()->each(function (User $user) use (&$active) {
            if ($user->getAccountStatus() === User::ACCOUNT_STATUS_ACTIVE) {
                $active++;
            }
        });
        $summary = [
            [
                'id' => 'active',
                'count' => $active
            ]
        ];
        Package::query()->each(function (Package $package) use (&$summary) {
            $summary[] = [
                'id' => $package->id,
                'count' => $package->getSubscriberCount()
            ];
        });
        return $summary;
    }
}
