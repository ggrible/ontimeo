<?php

namespace App\Http\Controllers;

use App\UserCalendar;
use Illuminate\Http\Request;

class GoogleController extends Controller
{
    public function handle(Request $request)
    {
        return redirect(google()->getClient()->createAuthUrl());
    }

    public function handleCallback(Request $request)
    {
        $data = google()->getClient()->fetchAccessTokenWithAuthCode($request->get('code'));
        /**
         * @var UserCalendar $user_calendar
         */
        $user_calendar = $request->user()->calendars()
            ->updateOrCreate([
                'provider' => UserCalendar::PROVIDER_GOOGLE
            ], [
                'access_token' => $data['access_token'],
                'refresh_token' => $data['refresh_token'],
                'expires_at' => now()->addSeconds($data['expires_in'])
            ]);
        $user_calendar->user->syncCalendarAppointments();
        return redirect('/email-sync');
    }
}
