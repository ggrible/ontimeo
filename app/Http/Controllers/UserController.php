<?php

namespace App\Http\Controllers;

use App\User;
use Stripe\Refund;
use Stripe\Invoice;
use Illuminate\Http\Request;
use App\Transformers\UserTransformer;
use App\Transformers\UserEmployeeTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class UserController extends Controller
{

    public function index(Request $request)
    {
        $this->validate($request, [
            'package_id' => 'nullable|exists:packages,id'
        ]);
        $query  = User::query();
        $query->isNotAdmin();
        if ($request->user()->isSuperAdmin()) {
            if ($request->filled('package_id')) {
                $query->select('users.*')
                    ->leftJoin('subscriptions as s', 's.user_id', '=', 'users.id')
                    ->leftJoin('package_terms as pt', 'pt.id', '=', 's.package_term_id')
                    ->where('pt.package_id', $request->get('package_id'));
            }
        }
        if ($request->has('status')) {
            // 
        }
        if ($request->has('paginate')) {
            $pagination = $query->paginate($request->get('paginate'));
            return fractal()
            ->collection($pagination->items(), new UserTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($pagination))
            ->respond();
        } else {
            $query = $query->get();
            return fractal()
            ->collection($query, new UserTransformer())
            ->respond();
        }
    }

    public function findByCustomNumber(Request $request)
    {
        $this->validate($request, [
            'custom_number' => 'required|string'
        ]);
        $user = User::query()
            ->where('custom_number', $request->get('custom_number'))
            ->firstOrFail();
        if (!$user->isDoctor()) {
            throw new NotFoundHttpException();
        }
        return fractal()
            ->item($user, new UserTransformer())
            ->respond();
    }

    /**
     * @param Request $request
     * @param User $user
     * @throws \Exception
     */
    public function refundSubscription(Request $request, User $user)
    {
        if (!$user->subscribed()) {
            throw new BadRequestHttpException('User is not subscribed');
        }
        $subscription = $user->subscription();
        if (!$subscription->active()) {
            throw new BadRequestHttpException('User subscription is not active.');
        }
        if ($subscription->refund()->count() > 0) {
            throw new BadRequestHttpException("User subscription is already refunded.");
        }
        beginTransaction();
        try {
            $invoice = $user->latestInvoice();
            $stripe_invoice = Invoice::retrieve($invoice->stripe_id);
            $cost_in_cents = 0;
            foreach ($stripe_invoice->lines->data as $invoice_item) {
                $cost_in_cents += $invoice_item->amount;
            }
            if ($cost_in_cents > 0) {
                $refund = Refund::create([
                    'charge' => $stripe_invoice->charge,
                    'amount' => abs($cost_in_cents),
                ]);
                $subscription->refund()->create([
                    'amount' => round($cost_in_cents / 100, 2),
                    'stripe_id' => $refund->id,
                ]);
                $subscription->cancelNow();
            }
            commit();
        } catch (\Exception $exception) {
            rollback();
            throw $exception;
        }
    }

    public function userEmployees(Request $request)
    {
        $employees = $request->has('custom_number') ? User::where('custom_number', $request->get('custom_number'))
        ->first()->employees
        : $request->user()->employees;
        return fractal()
        ->collection($employees, new UserEmployeeTransformer())
        ->respond();
    }
}
