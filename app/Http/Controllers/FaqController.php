<?php

namespace App\Http\Controllers;

use App\Faq;
use Illuminate\Http\Request;
use App\Transformers\FaqTransformer;

class FaqController extends Controller
{
    public function index()
    {
        return fractal()
            ->collection(Faq::all(), new FaqTransformer())
            ->respond();
    }

    public function store(Request $request)
    {
        $this->authorize('create', Faq::class);
        $this->validate($request, [
            'title' => 'required|string|max:255',
            'sub_title' => 'nullable|string|max:255',
            'description' => 'required|string',
        ]);
        $faq = Faq::query()
            ->create($request->only(['title', 'sub_title', 'description']));
        return $faq->getSerializedData();
    }

    public function update(Request $request, Faq $faq)
    {
        $this->authorize('update', $faq);
        $this->validate($request, [
            'title' => 'required|string|max:255',
            'sub_title' => 'nullable|string|max:255',
            'description' => 'required|string',
        ]);
        $faq->update($request->only(['title', 'sub_title', 'description']));
        return $faq->getSerializedData();
    }

    public function destroy(Faq $faq)
    {
        $this->authorize('delete', $faq);
        $faq->delete();
    }
}
