<?php

namespace App\Http\Controllers;

use App\Status;
use App\Term;
use App\Transformers\TermTransformer;
use Illuminate\Http\Request;

class TermController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return fractal()
            ->collection(Term::all(), new TermTransformer())
            ->respond();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(Request $request)
    {
        $this->authorize('create', Term::class);
        $this->validate($request, [
            'length' => 'required|integer',
            'status' => 'required|string|in:' . implode(',', Status::statuses()),
            'name' => 'required|string|max:255'
        ]);
        $term = Term::query()
            ->create($request->only(['name', 'length', 'status']));
        return $term->getSerializedData();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Term $term
     * @return array
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request, Term $term)
    {
        $this->authorize('update', $term);
        $this->validate($request, [
            'length' => 'required|integer',
            'status' => 'required|string|in:' . implode(',', Status::statuses()),
            'name' => 'required|string|max:255'
        ]);
        $term->update($request->only(['name', 'length', 'status']));
        return $term->getSerializedData();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Term $term
     * @return void
     * @throws \Exception
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Term $term)
    {
        $this->authorize('delete', $term);
        $term->delete();
    }
}
