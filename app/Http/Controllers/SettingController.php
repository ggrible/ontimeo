<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;
use App\Transformers\SettingTransformer;
use Brotzka\DotenvEditor\DotenvEditor;
use Brotzka\DotenvEditor\Exceptions\DotEnvException;

class SettingController extends Controller
{

    public function index()
    {
        return fractal()
            ->collection(Setting::all(), new SettingTransformer())
            ->respond();
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'settings' => 'nullable|array',
        ]);

        Setting::updateSettings($request->get('settings'));
    }

    public function stripe()
    {
        return response()->json([
            'public_key' => env('STRIPE_KEY'),
            'secret_key' => env('STRIPE_SECRET')
        ]);
    }

    public function updatestripe(Request $request)
    {
        $env = new DotenvEditor();
        if(!$env->keyExists("STRIPE_KEY")) {
            $env->addData([
                'STRIPE_KEY' => $request->public_key,
                'STRIPE_SECRET' => $request->secret_key
            ]);
        }
    }
}
