<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Http\Requests\Appointment\StoreRequest;
use App\Transformers\AppointmentTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Stripe\Charge;

class AppointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $appointments = new Appointment;
        
        if ($request->has('doctor_id')) {
            $appointments = $appointments->where('doctor_id', $request->get('doctor_id'));
        }

        $appointments = $appointments->get();

        return fractal()
            ->collection($appointments, new AppointmentTransformer())
            ->respond();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(StoreRequest $request)
    {
        beginTransaction();
        try {

            $schedule = $request->schedule_time()->schedule;
            $appointment_datetime = Carbon::parse($request->get('date'))->setTimeFromTimeString($request->schedule_time()->avail_time);
            if ($schedule->is_pre_booking) {
                $charge = Charge::create([
                    'amount' => $schedule->getPreAmountInCents(),
                    'currency' => 'usd',
                    'description' => 'Appointment Pre Booking Charge for ' . $request->get('name'),
                    'source' => $request->get('card_token'),
                    'metadata' => [
                        'doctor_id' => $request->get('doctor_id'),
                        'name' => $request->get('name'),
                        'appointment_datetime' => $appointment_datetime->toDateTimeString(),
                    ]
                ]);
                $charge = Charge::retrieve([
                    'id' => $charge->id,
                    'expand' => ['balance_transaction']
                ]);
                $appointment = Appointment::query()
                    ->create([
                        'doctor_id' => $request->get('doctor_id'),
                        'appointment_datetime' => $appointment_datetime,
                        'name' => $request->get('name'),
                        'amount' => $schedule->pre_amount,
                        'stripe_charge_id' => $charge->id,
                        'contact_number' => $request->get('contact_number'),
                        'email' => $request->get('email'),
                        'employee_id' => $request->get('employee_id') ? $request->get('employee_id') : null
                    ]);
            } else {
                $appointment = Appointment::query()
                    ->create([
                        'doctor_id' => $request->get('doctor_id'),
                        'appointment_datetime' => $appointment_datetime,
                        'name' => $request->get('name'),
                        'contact_number' => $request->get('contact_number'),
                        'email' => $request->get('email'),
                        'employee_id' => $request->get('employee_id') ? $request->get('employee_id') : null
                    ]);
            }
            commit();
            return fractal($appointment, new AppointmentTransformer())
                ->respond();
        } catch (\Exception $exception) {
            rollback();
            throw $exception;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
