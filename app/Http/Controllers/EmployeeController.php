<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;
use App\Transformers\EmployeeTransformer;

class EmployeeController extends Controller
{
    public function index(Request $request)
    {
        return fractal()
            ->collection(Employee::all(), new EmployeeTransformer())
            ->respond();
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|unique:employees,email',
            'job_title' => 'required|string|max:255',
            'telephone_ext' => 'required',
            'photo' => 'nullable|image'
        ]);
        $employee = Employee::query()
            ->create($request->only(['first_name', 'last_name', 'email', 'job_title', 'telephone_ext']));
        if ($employee) {
            $request->user()
            ->employees()
            ->create([
                'employee_id' => $employee->id
            ]);
            if ($request->hasFile('photo')) {
                $employee->savePhoto($request->file('photo'));
            }
        }
        return $employee->getSerializedData();
    }

    public function update(Request $request, Employee $employee)
    {
        $this->validate($request, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|unique:employees,email,'. $employee->id,
            'job_title' => 'required|string|max:255',
            'telephone_ext' => 'required'
        ]);
        $employee->update($request->only(['first_name', 'last_name', 'email', 'job_title', 'telephone_ext']));
        return $employee->getSerializedData();
    }

    public function destroy(Employee $employee)
    {
        $employee->delete();
    }
}
