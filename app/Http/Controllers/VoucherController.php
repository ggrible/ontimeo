<?php

namespace App\Http\Controllers;

use App\Voucher;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use App\Transformers\VoucherTransformer;
use Illuminate\Validation\Rule;

class VoucherController extends Controller
{
    public function index()
    {
        return fractal()
            ->collection(Voucher::all(), new VoucherTransformer())
            ->respond();
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(Request $request)
    {
        $this->authorize('create', Voucher::class);
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'coupon_code' => 'required|string|max:255|unique:vouchers,coupon_code',
            'discount_price' => 'required|numeric|min:1',
            'date_from' => 'required|date_format:Y-m-d',
            'date_to' => 'required|date_format:Y-m-d|after_or_equal:date_from',
        ]);
        beginTransaction();
        try {
            $voucher = Voucher::query()
                ->create($request->only(['name', 'coupon_code', 'discount_price', 'date_from', 'date_to']));
            commit();
            return $voucher->getSerializedData();
        } catch (\Exception $exception) {
            rollback();
            throw $exception;
        }
    }

    public function update(Request $request, Voucher $voucher)
    {
        $this->authorize('update', $voucher);
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'coupon_code' => 'required|string|max:255|unique:vouchers,coupon_code,' . $voucher->id,
            'discount_price' => 'required|numeric|min:1',
            'date_from' => 'required|date_format:Y-m-d',
            'date_to' => 'required|date_format:Y-m-d|after_or_equal:date_from',
        ]);
        $voucher->update($request->only(['name', 'coupon_code', 'date_from', 'date_to']));
        return $voucher->getSerializedData();
    }

    public function destroy(Voucher $voucher)
    {
        $this->authorize('delete', $voucher);
        $voucher->delete();
    }

    public function check(Request $request)
    {
        $this->validate($request, [
            'coupon_code' => [
                'nullable',
                'string',
                Rule::exists('vouchers', 'coupon_code')->where(function (Builder $builder) {
                    $builder->where('date_from', '<=', today())
                        ->where('date_to', '>=', today());
                })
            ]
        ]);
        return fractal(Voucher::query()->where('coupon_code', $request->get('coupon_code'))->first(), new VoucherTransformer())
            ->respond();
    }
}
