<?php

namespace App\Http\Controllers;

use App\Http\Requests\SetupPassword\SubmitRequest;
use App\PasswordSetupToken;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SetupPasswordController extends Controller
{
    public function showForm(Request $request, $email, $code)
    {
        $this->validateData($email, $code);
        return view('setup-password');
    }

    public function submit(SubmitRequest $request)
    {
        transaction(function () use ($request) {
            /**
             * @var User $user
             */
            $user = User::query()
                ->where('email', $request->get('email'))
                ->first();
            $user->update([
                'password' => bcrypt($request->get('password')),
                'is_password_setup' => true
            ]);
            $user->password_setup_tokens()
                ->delete();
        });
    }

    public function validateData($email, $code)
    {
        /**
         * @var User $user
         * @var PasswordSetupToken $token
         */
        $user = User::query()
            ->where('email', $email)
            ->firstOrFail();
        $token = $user->password_setup_tokens()
            ->firstOrFail();
        if (!Hash::check($code, $token->token)) {
            throw new NotFoundHttpException('Invalid link.');
        }
    }
}
