<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Http\Request;

class TelnyxController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function search(Request $request)
    {
        $this->validate($request, [
            'city' => 'required|string|max:255',
            'state' => 'required|string|max:255',
        ]);
        $client = new Client([
            'headers' => [
                'x-api-user' => 'nfo@ontimeo.com',
                'x-api-token' => 'q1NNSkpssP3sHxxWgVyEZDy',
//                'Content-Type' => 'application/json'
            ]
        ]);
        $response = $client->post('https://api.telnyx.com/origination/number_searches', [
            RequestOptions::JSON => [
                'search_type' => 2,
                'search_descriptor' => [
                    'city' => $request->get('city'),
                    'state' => $request->get('state'),
                    'has_all_features' => ['sms']
                ]
            ]
        ]);
        if ($response->getStatusCode() !== 200) {
            throw new \Exception($response->getBody()->getContents());
        }
        return response()->json(json_decode($response->getBody()->getContents()));
    }
}
