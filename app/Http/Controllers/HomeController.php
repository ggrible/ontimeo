<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function getSummary()
    {
        return [
            'appointments' => Appointment::query()->count(),
            'clients' => User::query()->isNotAdmin()->count()
        ];
    }
}
