<?php

namespace App\Http\Controllers;

use App\Outlook;
use App\UserCalendar;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use League\OAuth2\Client\Provider\GenericProvider;

class OutlookController extends Controller
{

        public function handle(Request $request)
        {
            $response = redirect(\outlook()->getClient()->getAuthorizationUrl());
            session()->put('oauth_state', \outlook()->getClient()->getState());
            return $response;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function handleCallback(Request $request)
    {
        if ($request->filled('code')) {
            // Check that state matches
            if (!$request->filled('state') || ($request->get('state') !== session('oauth_state'))) {
                throw new \Exception('State provided in redirect does not match expected value.');
            }

            // Clear saved state
            session()->remove('oauth_state');

            // Initialize the OAuth client
            $oauthClient = \outlook()->getClient();

            try {
                // Make the token request
                $accessToken = $oauthClient->getAccessToken('authorization_code', [
                    'code' => $request->get('code')
                ]);
                /**
                 * @var UserCalendar $user_calendar
                 */
                $user_calendar = $request->user()->calendars()
                    ->updateOrCreate([
                        'provider' => UserCalendar::PROVIDER_OUTLOOK
                    ], [
                        'access_token' => $accessToken->getToken(),
                        'refresh_token' => $accessToken->getRefreshToken(),
                        'expires_at' => Carbon::createFromTimestamp($accessToken->getExpires()),
                    ]);
                $user_calendar->user->syncCalendarAppointments();
                return redirect('/email-sync');
            }
            catch (\Exception $e) {
                throw $e;
            }
        }
        elseif ($request->filled('error')) {
            throw new \Exception('ERROR: '.$_GET['error'].' - '.$_GET['error_description']);
        }
    }
}
