<?php

namespace App\Http\Requests\Appointment;

use App\ScheduleTime;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Validator;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'doctor_id' => 'required|exists:users,id',
            'date' => 'required|date',
            'name' => 'required|string|max:255',
            'schedule_time_id' => 'required|exists:schedule_times,id',
            'email' => 'nullable|email|max:255',
            'contact_number' => 'nullable|string|max:20',
            'employee_id' => 'nullable|exists:employees,id'
        ];
        if ($this->filled('schedule_time_id') && ($schedule_time = $this->schedule_time())) {
            if ($schedule_time->schedule->is_pre_booking) {
                $rules['card_token'] = 'required|string|max:255';
            } else {
                $rules = array_merge($rules, [
                    'contact_number' => 'required|string|max:20',
                ]);
            }
        }
        return $rules;
    }

    /**
     * @return User
     */
    public function doctor()
    {
        return User::query()->find($this->get('doctor_id'));
    }

    /**
     * @return ScheduleTime
     */
    public function schedule_time()
    {
        return ScheduleTime::query()->find($this->get('schedule_time_id'));
    }

    public function withValidator(Validator $validator)
    {
        $validator->after(function (Validator $validator) {
            if ($validator->errors()->any()) {
                return;
            }
            if (!$this->doctor()->isDoctor()) {
                $validator->errors()->add('doctor_id', 'Invalid doctor.');
            }
            if ($this->schedule_time()->schedule->user_id != $this->doctor()->id) {
                $validator->errors()->add('schedule_time_id', 'Invalid schedule time.');
            }
            if ($validator->errors()->any()) {
                return;
            }
            if (!$this->schedule_time()->isAvailable(Carbon::parse($this->get('date')))) {
                $validator->errors()->add('date', 'Date is not available for the selected time.');
            }
        });
    }
}
