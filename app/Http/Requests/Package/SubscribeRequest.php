<?php

namespace App\Http\Requests\Package;

use App\Package;
use App\PackageAddOn;
use App\PackageTerm;
use App\Status;
use App\Voucher;
use Illuminate\Database\Query\Builder;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Validator;

class SubscribeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|email|max:255|unique:users,email',
            'mobile_number' => 'nullable|string|max:255',
            'landline_number' => 'nullable|string|max:255',
            'custom_number' => 'nullable|string|max:255|unique:users',
            'package_term_id' => [
                'required',
                Rule::exists('package_terms', 'id')->where(function (Builder $builder) {
                    $builder->where('status', Status::ACTIVE);
                })
            ],
            'package_add_ons' => 'required|array',
            'package_add_ons.*' => [
                'required',
                Rule::exists('package_add_ons', 'id')->where(function (Builder $builder) {
                    $builder->where('status', Status::ACTIVE);
                })
            ],
            'card_token' => 'required|string|max:255',
            'coupon_code' => [
                'nullable',
                'string',
                Rule::exists('vouchers', 'coupon_code')->where(function (Builder $builder) {
                    $builder->where('date_from', '<=', today())
                        ->where('date_to', '>=', today());
                })
            ]
        ];
    }

    /**
     * @return Voucher
     */
    public function voucher()
    {
        return Voucher::query()
            ->where('coupon_code', $this->get('coupon_code'))
            ->first();
    }

    /**
     * @return PackageTerm
     */
    public function package_term()
    {
        return PackageTerm::query()->find($this->get('package_term_id'));
    }

    public function withValidator(Validator $validator)
    {
        $validator->after(function (Validator $validator) {
            if ($validator->errors()->any()) {
                return;
            }
            foreach ($this->get('package_add_ons') as $package_add_on_id) {
                /**
                 * @var PackageAddOn $package_add_on
                 */
                $package_add_on = PackageAddOn::query()->find($package_add_on_id);
                if ($package_add_on->status !== Status::ACTIVE || !$package_add_on->add_on || $package_add_on->add_on->status !== Status::ACTIVE) {
                    $validator->errors()->add('package_add_ons', 'One of the add ons are inactive.');
                    break;
                }
            }
        });
    }
}
