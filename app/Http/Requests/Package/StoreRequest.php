<?php

namespace App\Http\Requests\Package;

use App\Package;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('create', Package::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'description' => 'nullable|string|max:65535',
            'is_featured' => 'required|boolean',
            'status' => 'required|string|in:' . implode(',', Package::statuses()),
            'promo' => 'nullable|string|in:' . implode(',', Package::promos()),
            'offers' => 'nullable|array',
            'offers.*' => 'required|string|max:255',
            'add_ons' => 'required|array',
            'add_ons.*.id' => 'required|exists:add_ons,id',
            'add_ons.*.price' => 'required|numeric',
            'terms' => 'required|array',
            'terms.*.id' => 'required|exists:terms,id',
            'terms.*.price' => 'required|numeric',
            'terms.*.discount' => 'nullable|numeric|min:1|max:100',
        ];
    }
}
