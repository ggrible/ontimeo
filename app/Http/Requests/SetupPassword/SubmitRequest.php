<?php

namespace App\Http\Requests\SetupPassword;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Validator;

class SubmitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|exists:users,email',
            'token' => 'required|string',
            'password' => 'required|string|min:6',
        ];
    }

    public function withValidator(Validator $validator)
    {
        $validator->after(function (Validator $validator) {
            if ($validator->errors()->any()) {
                return;
            }
            /**
             * @var User $user
             */
            $user = User::query()
                ->where('email', $this->get('email'))
                ->first();
            $token = $user->password_setup_tokens()->first();
            if (!Hash::check($this->get('token'), $token->token)) {
                $validator->errors()->add('token', 'Invalid token.');
            }
        });
    }
}
