<?php

namespace App\Http\Requests\Schedule;

use App\Schedule;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Validator;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'day' => 'required|integer|between:1,7',
            'type' => 'required|string|in:' . implode(',', Schedule::getTypes()),
            'interval' => 'required|in: ' . implode(',', Schedule::getIntervals()),
            'is_pre_booking' => 'required|boolean',
            'pre_amount' => 'nullable|numeric',
            'full_amount' => 'nullable|numeric',
            'avail_type' => 'required|string|in:' . implode(',', Schedule::getAvailTypes()),
            'avail_from' => 'required_if:avail_type,' . Schedule::AVAIL_OPEN_HOURS . '|date_format:H:i',
            'avail_to' => 'required_if:avail_type,' . Schedule::AVAIL_OPEN_HOURS . '|date_format:H:i|after:avail_from',
            'avail_times' => 'nullable|array',
            "avail_times.*" => 'date_format:H:i',
            'employee_id' => 'nullable|exists:employees,id'
        ];
    }

    public function withValidator(Validator $validator)
    {
        $validator->after(function (Validator $validator) {
            if ($validator->errors()->any()) {
                return;
            }
            if ($this->get('avail_type') === Schedule::AVAIL_OPEN_HOURS) {
                $carbon_from = Carbon::parse($this->get('avail_from'));
                $carbon_to = Carbon::parse($this->get('avail_to'));
                if ($carbon_from->gte($carbon_to)) {
                    $validator->errors()->add('avail_to', 'To time must be greater than from time.');
                }
            }
        });
    }
}
