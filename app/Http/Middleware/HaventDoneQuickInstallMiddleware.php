<?php

namespace App\Http\Middleware;

use Closure;

class HaventDoneQuickInstallMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->isDoctor() && $request->user()->logged_in_first_time) {
            return redirect('/profile');
        }
        return $next($request);
    }
}
