<?php

namespace App\Providers;

use App\Faq;
use App\Lead;
use App\Term;
use App\AddOn;
use App\Package;
use App\Voucher;
use App\CaseStudy;
use App\CaseStudyCategory;
use App\Policies\FaqPolicy;
use App\Policies\LeadPolicy;
use App\Policies\TermPolicy;
use App\Policies\AddOnPolicy;
use App\Policies\PackagePolicy;
use App\Policies\VoucherPolicy;
use App\Policies\CaseStudyPolicy;
use App\Policies\CaseStudyCategoryPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        Package::class => PackagePolicy::class,
        Term::class => TermPolicy::class,
        AddOn::class => AddOnPolicy::class,
        CaseStudy::class => CaseStudyPolicy::class,
        CaseStudyCategory::class => CaseStudyCategoryPolicy::class,
        Voucher::class => VoucherPolicy::class,
        Lead::class => LeadPolicy::class,
        Faq::class => FaqPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
