<?php

namespace App\Providers;

use App\Appointment;
use App\AppointmentFee;
use App\Google;
use App\Invoice;
use App\Observers\AppointmentFeeObserver;
use App\Observers\AppointmentObserver;
use App\Observers\InvoiceObserver;
use App\Observers\ScheduleObserver;
use App\Observers\ScheduleTimeObserver;
use App\Observers\SubscriptionRefundObserver;
use App\Observers\TransactionObserver;
use App\Observers\UserObserver;
use App\Observers\VoucherObserver;
use App\Observers\WithdrawalFeeObserver;
use App\Observers\WithdrawalObserver;
use App\Outlook;
use App\Schedule;
use App\ScheduleTime;
use App\SubscriptionRefund;
use App\Transaction;
use App\User;
use App\Voucher;
use App\Withdrawal;
use App\WithdrawalFee;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Stripe\Stripe;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);
        Schema::defaultStringLength(191);
        ScheduleTime::observe(ScheduleTimeObserver::class);
        Schedule::observe(ScheduleObserver::class);
        Appointment::observe(AppointmentObserver::class);
        AppointmentFee::observe(AppointmentFeeObserver::class);
        Invoice::observe(InvoiceObserver::class);
        Transaction::observe(TransactionObserver::class);
        Withdrawal::observe(WithdrawalObserver::class);
        SubscriptionRefund::observe(SubscriptionRefundObserver::class);
        Voucher::observe(VoucherObserver::class);
        WithdrawalFee::observe(WithdrawalFeeObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Stripe::setApiKey(config('services.stripe.secret'));
        config([
            'services.google.redirect' => url('/google/callback'),
            'services.live.redirect' => url('/outlook/callback'),
        ]);
        $this->app->singleton(Google::class, function ($app) {
            return new Google();
        });
        $this->app->singleton(Outlook::class, function ($app) {
            return new Outlook();
        });
    }
}
