<?php

namespace App;

use App\Traits\SerializesData;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PackageTerm
 * @package App
 * @property Term term
 * @property string stripe_plan_id
 * @property Package package
 * @property float price
 * @property integer id
 * @property float discount
 */
class PackageTerm extends Model
{
    use SerializesData;

    protected $guarded = [];

    protected $casts = [
        'discount' => 'float'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function term()
    {
        return $this->belongsTo(Term::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function package()
    {
        return $this->belongsTo(Package::class);
    }

    /**
     * @return float|int
     */
    public function discountAmount()
    {
        if (!$this->discount) {
            return 0;
        }
        return round($this->price * ($this->discount / 100), 2);
    }
}
