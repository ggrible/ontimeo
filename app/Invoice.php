<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string stripe_id
 * @property User user
 */
class Invoice extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
