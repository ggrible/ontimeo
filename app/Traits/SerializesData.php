<?php

namespace App\Traits;

use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\ArraySerializer;
use League\Fractal\Serializer\DataArraySerializer;

trait SerializesData
{
    /**
     * @param ArraySerializer|DataArraySerializer $serializer
     * @param string $include
     * @return array
     */
    public function getSerializedData($serializer = null, $include = null)
    {
        if ($serializer === null) {
            $serializer = new ArraySerializer();
        }
        $class = array_last(explode('\\', static::class));
        $transformer = ('App\\Transformers\\' . $class . 'Transformer');
        $resource = new Item($this, new $transformer);
        $manager = new Manager();
        $manager->setSerializer($serializer);
        $includeString = '';
        if (\request() && \request()->has(config('fractal.auto_includes.request_key'))) {
            $includeString .= $this->cleanIncludeString(\request(config('fractal.auto_includes.request_key')));
        }
        $includeString .= ',' . $this->cleanIncludeString($include);
        if ($includeString !== '') {
            $manager->parseIncludes($includeString);
        }
        return $manager->createData($resource)->toArray();
    }

    /**
     * @param $include
     * @return string
     */
    public function cleanIncludeString($include)
    {
        if (!$include) return '';
        return trim(trim($include), ',');
    }
}