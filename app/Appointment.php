<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model\Event;

/**
 * Class Appointment
 * @package App
 * @property User doctor
 * @property int doctor_id
 * @property string appointment_datetime
 * @property string name
 * @property string contact_number
 * @property float amount
 * @property int id
 * @property string stripe_charge_id
 * @property string google_event_id
 * @property string outlook_event_id
 */
class Appointment extends Model
{
    protected $guarded = [];

    /**
     * @var Schedule
     */
    private $schedule;

    protected $casts = [
        'amount' => 'float',
        'net_amount' => 'float'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function doctor()
    {
        return $this->belongsTo(User::class, 'doctor_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function fees()
    {
        return $this->hasMany(AppointmentFee::class);
    }

    /**
     * @return Schedule
     */
    public function getSchedule()
    {
        if ($this->schedule) {
            return $this->schedule;
        }
        $day_of_week = $this->getAppointmentDatetimeCarbon()->dayOfWeek;
        return $this->schedule = $this->doctor->schedules()
            ->where('day', $day_of_week == 0 ? 7 : $day_of_week)
            ->first();
    }

    public function getAppointmentDatetimeCarbon()
    {
        return Carbon::parse($this->appointment_datetime, config('settings.timezone'));
    }

    /**
     * @throws \Microsoft\Graph\Exception\GraphException
     */
    public function syncCalendarEvent()
    {
        $carbon_time = $this->getAppointmentDatetimeCarbon();
        $summary = ($this->getSchedule()->type === Schedule::TYPE_ONE_ON_ONE ? '1-on-1 ' : 'group ') . $this->name . '(' . $this->contact_number . ')';
        if (!$this->google_event_id && ($calendar = $this->doctor->getCalendar(UserCalendar::PROVIDER_GOOGLE))) {
            google()->setCalendarId($calendar->getCalendarId());
            google()->setAccessToken($calendar->getAccessTokenArray());
            $event = google()->createEvent($carbon_time->toAtomString(), $carbon_time->toAtomString(), $summary);
            $event = google()->insertEvent($event);
            $this->update([
                'google_event_id' => $event->getId()
            ]);
        }
        if (!$this->outlook_event_id && ($calendar = $this->doctor->getCalendar(UserCalendar::PROVIDER_OUTLOOK))) {
            $graph = new Graph();
            $graph->setAccessToken($calendar->getAccessToken());
            $getEventsUrl = '/me/calendars/' . $calendar->getCalendarId() . '/events';
            /**
             * @var Event $event
             */
            $event = $graph->createRequest('POST', $getEventsUrl)
                ->attachBody([
                    'subject' => $summary,
                    'body' => [
                        'content' => $summary
                    ],
                    'start' => [
                        'dateTime' => $carbon_time->toAtomString(),
                        'timeZone' => 'utc'
                    ],
                    'end' => [
                        'dateTime' => $carbon_time->toAtomString(),
                        'timeZone' => 'utc'
                    ],
                ])
                ->setReturnType(Event::class)
                ->execute();
            $this->update([
                'outlook_event_id' => $event->getId()
            ]);
        }
    }
}
