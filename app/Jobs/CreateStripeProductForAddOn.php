<?php

namespace App\Jobs;

use App\AddOn;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Stripe\Product;

class CreateStripeProductForAddOn implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var AddOn
     */
    private $addOn;

    /**
     * Create a new job instance.
     *
     * @param AddOn $addOn
     */
    public function __construct(AddOn $addOn)
    {
        $this->addOn = $addOn;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->addOn->stripe_product_id) {
            echo "Already has stripe product id.";
            return;
        }
        $product = Product::create([
            'name' => $this->addOn->name,
            'type' => 'service',
            'metadata' => [
                'add_on_id' => $this->addOn->id
            ]
        ]);
        $this->addOn->update([
            'stripe_product_id' => $product->id
        ]);
    }
}
