<?php

namespace App\Jobs;

use App\Package;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Stripe\Product;

class CreateStripeProductForPackage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var Package
     */
    private $package;

    /**
     * Create a new job instance.
     *
     * @param Package $package
     */
    public function __construct(Package $package)
    {
        $this->package = $package;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->package->stripe_product_id) {
            echo "Already has stripe product id.";
            return;
        }
        $product = Product::create([
            'name' => $this->package->name,
            'type' => 'service',
            'metadata' => [
                'package_id' => $this->package->id
            ]
        ]);
        $this->package->update([
            'stripe_product_id' => $product->id
        ]);
    }
}
