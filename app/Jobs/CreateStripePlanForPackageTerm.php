<?php

namespace App\Jobs;

use App\PackageTerm;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Stripe\Plan;

class CreateStripePlanForPackageTerm implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var PackageTerm
     */
    private $package_term;

    /**
     * Create a new job instance.
     *
     * @param PackageTerm $package_term
     */
    public function __construct(PackageTerm $package_term)
    {
        $this->package_term = $package_term;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $package = $this->package_term->package;
        if ($this->package_term->stripe_plan_id) {
            echo "Stripe plan already created for package term.";
            return;
        }
        $plan = Plan::create([
            'currency' => 'usd',
            'interval' => 'month',
            'interval_count' => $this->package_term->term->length,
            'product' => $package->stripe_product_id,
            'nickname' => $package->name . ' ' . $this->package_term->term->length . ' Months',
            'amount' => round(($this->package_term->price - $this->package_term->discountAmount()) * 100 * $this->package_term->term->length),
        ]);
        $this->package_term->update([
            'stripe_plan_id' => $plan->id,
        ]);
    }
}
