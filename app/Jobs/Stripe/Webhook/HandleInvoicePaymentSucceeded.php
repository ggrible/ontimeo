<?php

namespace App\Jobs\Stripe\Webhook;

use App\Invoice;
use App\StripeWebhookCall;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class HandleInvoicePaymentSucceeded implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var StripeWebhookCall
     */
    private $call;

    /**
     * Create a new job instance.
     *
     * @param StripeWebhookCall $call
     */
    public function __construct(StripeWebhookCall $call)
    {
        $this->call = $call;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $stripe_invoice = $this->call->payload['data']['object'];
        /**
         * @var User $user
         */
        $user = User::query()
            ->where('stripe_id', $stripe_invoice['customer'])
            ->first();
        if (!$user) {
            echo 'User with stripe id ' . $stripe_invoice['customer'] . ' not found.';
            return;
        }
        Invoice::query()
            ->create([
                'user_id' => $user->id,
                'stripe_id' => $stripe_invoice['id'],
                'amount' => round($stripe_invoice['amount_paid'], 2),
                'package_id' => $user->subscription()->package_term->package_id
            ]);
    }
}
