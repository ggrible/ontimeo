<?php

namespace App\Jobs;

use App\PackageAddOn;
use App\PackageTerm;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Stripe\Plan;

class CreateStripePlansForPackageAddOn implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var PackageAddOn
     */
    private $package_add_on;

    /**
     * Create a new job instance.
     *
     * @param PackageAddOn $package_add_on
     */
    public function __construct(PackageAddOn $package_add_on)
    {
        $this->package_add_on = $package_add_on;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $add_on = $this->package_add_on->add_on;
        $package = $this->package_add_on->package;
        /**
         * @var PackageTerm $package_term
         */
        foreach ($package->package_terms as $package_term) {
            $plan = Plan::create([
                'currency' => 'usd',
                'interval' => 'month',
                'interval_count' => $package_term->term->length,
                'product' => $add_on->stripe_product_id,
                'nickname' => $package->name . ' - ' . $add_on->name . ' ' . $package_term->term->length . ' Months',
                'amount' => round($this->package_add_on->price * 100 * $package_term->term->length),
            ]);
            $this->package_add_on->plans()
                ->updateOrCreate([
                    'package_term_id' => $package_term->id
                ], [
                    'stripe_plan_id' => $plan->id
                ]);
        }
    }
}
