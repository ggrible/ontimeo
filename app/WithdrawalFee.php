<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property float fee
 * @property Withdrawal withdrawal
 */
class WithdrawalFee extends Model
{
    protected $guarded = [];

    protected $casts = [
        'fee' => 'float'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function withdrawal()
    {
        return $this->belongsTo(Withdrawal::class);
    }
}
