<?php

use App\Setting;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $defaultSettings = [
            ['display_name' => 'Appointment Processing Fee', 'key' => 'appointment_processing_fee', 'value' => 1],
            ['display_name' => 'Withdrawal Fee', 'key' => 'withdrawal_fee', 'value' => 1]
        ];

        foreach ($defaultSettings as $setting) {
            Setting::firstOrCreate(
            [ 'key' => $setting['key'] ] // indentifier if exist
            ,$setting);
        }
    }
}
