<?php

use Illuminate\Database\Seeder;

class DefaultPackagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * @var \App\Package $package
         */

        //unique
        $package = \App\Package::query()
            ->create([
                'name' => 'Unique',
                'description' => 'Appointment Scheduling Like A Genius',
                'is_featured' => false,
                'status' => \App\Package::STATUS_ACTIVE,
                'promo' => '1st month',
                'domain_additional_charge' => null,
            ]);
        $package->offers()
            ->createMany([
                ['offer' => '2nd Line To Your Existing Phone'],
                ['offer' => 'SMS Enabled'],
                ['offer' => 'Calender syn (more)'],
                ['offer' => 'Sms Booking'],
                ['offer' => 'Reputation Management'],
            ]);
        $package_terms = $package->package_terms()
            ->createMany([
                ['term_id' => 1, 'price' => 9.99, 'discount' => 10],
                ['term_id' => 2, 'price' => 19.99, 'discount' => 10],
                ['term_id' => 3, 'price' => 29.99, 'discount' => 10],
                ['term_id' => 4, 'price' => 39.99, 'discount' => 10],
            ]);
        \App\Jobs\CreateStripeProductForPackage::withChain($package_terms->map(function ($package_term) {
            return new \App\Jobs\CreateStripePlanForPackageTerm($package_term);
        })->toArray())
            ->dispatch($package);
        $add_ons = $package->package_add_ons()
            ->createMany([
                ['add_on_id' => 6, 'price' => 5.99],
                ['add_on_id' => 3, 'price' => 12.98],
            ]);
        foreach ($add_ons as $add_on) {
            dispatch_now(new \App\Jobs\CreateStripePlansForPackageAddOn($add_on));
        }

        //complete
        $package = \App\Package::query()
            ->create([
                'name' => 'Complete',
                'description' => 'Win Back Your Lost Customers.',
                'is_featured' => false,
                'status' => \App\Package::STATUS_ACTIVE,
                'promo' => '1st month',
                'domain_additional_charge' => null,
            ]);
        $package->offers()
            ->createMany([
                ['offer' => '2nd Line To Your Existing Phone'],
                ['offer' => 'SMS & Voice Enabled'],
                ['offer' => 'Calender syn (more)'],
                ['offer' => 'Sms Booking'],
                ['offer' => 'Reputation Management'],
            ]);
        $package_terms = $package->package_terms()
            ->createMany([
                ['term_id' => 1, 'price' => 9.99, 'discount' => 10],
                ['term_id' => 2, 'price' => 19.99, 'discount' => 10],
                ['term_id' => 3, 'price' => 29.99, 'discount' => 10],
                ['term_id' => 4, 'price' => 39.99, 'discount' => 10],
            ]);
        \App\Jobs\CreateStripeProductForPackage::withChain($package_terms->map(function ($package_term) {
            return new \App\Jobs\CreateStripePlanForPackageTerm($package_term);
        })->toArray())
            ->dispatch($package);
        $add_ons = $package->package_add_ons()
            ->createMany([
                ['add_on_id' => 6, 'price' => 5.99],
                ['add_on_id' => 3, 'price' => 12.98],
            ]);
        foreach ($add_ons as $add_on) {
            dispatch_now(new \App\Jobs\CreateStripePlansForPackageAddOn($add_on));
        }

        //shared
        $package = \App\Package::query()
            ->create([
                'name' => 'Shared',
                'description' => 'Freemium Business Models',
                'is_featured' => false,
                'status' => \App\Package::STATUS_ACTIVE,
                'promo' => '1st month',
                'domain_additional_charge' => null,
            ]);
        $package->offers()
            ->createMany([
                ['offer' => 'Shared Number'],
                ['offer' => 'SMS Enabled'],
                ['offer' => 'Calender syn (more)'],
                ['offer' => 'Sms Booking'],
                ['offer' => 'Reputation Management'],
            ]);
        $package_terms = $package->package_terms()
            ->createMany([
                ['term_id' => 1, 'price' => 9.99, 'discount' => 10],
                ['term_id' => 2, 'price' => 19.99, 'discount' => 10],
                ['term_id' => 3, 'price' => 29.99, 'discount' => 10],
                ['term_id' => 4, 'price' => 39.99, 'discount' => 10],
            ]);
        \App\Jobs\CreateStripeProductForPackage::withChain($package_terms->map(function ($package_term) {
            return new \App\Jobs\CreateStripePlanForPackageTerm($package_term);
        })->toArray())
            ->dispatch($package);
        $add_ons = $package->package_add_ons()
            ->createMany([
                ['add_on_id' => 6, 'price' => 5.99],
                ['add_on_id' => 3, 'price' => 12.98],
            ]);
        foreach ($add_ons as $add_on) {
            dispatch_now(new \App\Jobs\CreateStripePlansForPackageAddOn($add_on));
        }
    }
}
