<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $user = \App\User::query()
            ->create([
                'first_name' => 'System',
                'last_name' => 'Administrator',
                'email' => 'admin@user.com',
                'password' => bcrypt('123456')
            ]);
        $user->assign('super-admin');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\User::query()
            ->where('email', 'admin@user.com')
            ->delete();
    }
}
