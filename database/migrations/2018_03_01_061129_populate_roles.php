<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Silber\Bouncer\BouncerFacade;

class PopulateRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        BouncerFacade::role()->create([
            'name' => 'super-admin',
            'title' => 'Superadmin',
        ]);
        BouncerFacade::role()->create([
            'name' => 'admin',
            'title' => 'Admin',
        ]);
        BouncerFacade::role()->create([
            'name' => 'manager',
            'title' => 'Manager',
        ]);
        BouncerFacade::role()->create([
            'name' => 'regular',
            'title' => 'Regular',
        ]);
        BouncerFacade::role()->create([
            'name' => 'doctor',
            'title' => 'Doctor',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Silber\Bouncer\Database\Role::query()
            ->delete();
    }
}
