<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersTableChangeCompanyIndustryToForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\User::query()
            ->update([
                'company_industry' => null,
            ]);
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedInteger('company_industry')->nullable()->change();
            $table->foreign('company_industry')->references('id')->on('case_study_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['company_industry']);
            $table->string('company_industry')->nullable()->change();
        });
    }
}
