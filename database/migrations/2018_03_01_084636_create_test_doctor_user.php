<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestDoctorUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $user = \App\User::query()
            ->create([
                'first_name' => 'Test',
                'last_name' => 'Doctor',
                'email' => 'doctor@user.com',
                'password' => bcrypt('123456')
        ]);
        $user->assign('doctor');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\User::query()
            ->delete();
    }
}
