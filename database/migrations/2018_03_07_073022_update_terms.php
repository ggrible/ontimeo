<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTerms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\Term::query()
            ->create([
                'length' => 12,
                'id' => 4
            ]);
        \App\Term::query()
            ->where('id', 1)
            ->update([
                'length' => 1
            ]);
        \App\Term::query()
            ->where('id', 2)
            ->update([
                'length' => 3
            ]);
        \App\Term::query()
            ->where('id', 3)
            ->update([
                'length' => 6
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\Term::query()
            ->where('id', 4)
            ->delete();
        \App\Term::query()
            ->where('id', 1)
            ->update([
                'length' => 12
            ]);
        \App\Term::query()
            ->where('id', 2)
            ->update([
                'length' => 24
            ]);
        \App\Term::query()
            ->where('id', 3)
            ->update([
                'length' => 36
            ]);
    }
}
