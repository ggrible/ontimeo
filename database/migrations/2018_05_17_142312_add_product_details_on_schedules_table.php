<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductDetailsOnSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('schedules', function (Blueprint $table) {
            $table->string('product')->nullable()->default(null);
            $table->string('category')->nullable()->default(null);
            $table->integer('limit')->unsigned()->nullable()->default(5);
            $table->decimal('price', 8, 2)->nullable()->default(null);
            $table->decimal('discount', 8, 2)->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('schedules', function (Blueprint $table) {
            $table->dropColumn(['product', 'category', 'limit', 'price', 'discount']);
        });
    }
}
