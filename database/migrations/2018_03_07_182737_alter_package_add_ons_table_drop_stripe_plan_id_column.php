<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPackageAddOnsTableDropStripePlanIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('package_add_ons', function (Blueprint $table) {
            $table->dropColumn('stripe_plan_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('package_add_ons', function (Blueprint $table) {
            $table->string('stripe_plan_id')->index()->nullable();
        });
    }
}
