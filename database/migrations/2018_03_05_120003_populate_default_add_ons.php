<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PopulateDefaultAddOns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $add_ons = [
            [1, 'Site Backup & Restore', 5.99],
            [2, 'All In One', 49.99],
            [3, 'Custom Email', 2],
            [4, 'IVR Booking', 79.99],
            [5, 'Remote Secretary', 149],
            [6, 'Social Media Booking', 3.99],
            [7, 'Virtual Business Address', 99],
        ];
        foreach ($add_ons as $add_on) {
            \App\AddOn::query()
                ->create([
                    'id' => $add_on[0],
                    'name' => $add_on[1],
                    'price' => $add_on[2],
                    'is_default' => true,
                ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\AddOn::query()
            ->delete();
    }
}
