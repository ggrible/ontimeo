<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AssignRoleForAdminUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\User::query()
            ->where('email', 'admin@user.com')
            ->first()
            ->assign(\App\User::ROLE_SUPER_ADMIN);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\User::query()
            ->where('email', 'admin@user.com')
            ->first()
            ->retract(\App\User::ROLE_SUPER_ADMIN);
    }
}
