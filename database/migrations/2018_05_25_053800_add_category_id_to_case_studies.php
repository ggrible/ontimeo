<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoryIdToCaseStudies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('case_studies', function($table)
        {
            //$table->unsignedInteger('category_id')->nullable(false);
            $table->unsignedInteger('category_id');
            $table->foreign('category_id')->references('id')->on('case_study_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('case_studies', function($table)
        {
            $table->dropColumn('category_id');
        });
    }
}
