<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageAddOnPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_add_on_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('package_add_on_id');
            $table->foreign('package_add_on_id')->references('id')->on('package_add_ons')->onDelete('cascade');
            $table->unsignedInteger('package_term_id');
            $table->foreign('package_term_id')->references('id')->on('package_terms')->onDelete('cascade');
            $table->string('stripe_plan_id')->index()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package_add_on_plans');
    }
}
