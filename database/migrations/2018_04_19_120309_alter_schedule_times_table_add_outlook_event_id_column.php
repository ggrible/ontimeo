<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterScheduleTimesTableAddOutlookEventIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('schedule_times', function (Blueprint $table) {
            $table->string('outlook_event_id')->nullable()->after('google_event_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('schedule_times', function (Blueprint $table) {
            $table->dropColumn('outlook_event_id');
        });
    }
}
