<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PopulateTerms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\Term::query()
            ->create([
                'length' => 12,
                'id' => 1
            ]);
        \App\Term::query()
            ->create([
                'length' => 24,
                'id' => 2,
            ]);
        \App\Term::query()
            ->create([
                'length' => 36,
                'id' => 3,
            ]);
    }

    /**
     * Reverse the migrations.s
     *
     * @return void
     */
    public function down()
    {
        \App\Term::query()
            ->delete();
    }
}
