<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedSmallInteger("day");
            $table->string("type")->index();
            $table->unsignedSmallInteger("interval")->index();
            $table->boolean("is_pre_booking");
            $table->decimal("pre_amount")->nullable();
            $table->decimal("full_amount")->nullable();
            $table->string("avail_type")->index();
            $table->time("avail_from")->nullable();
            $table->time("avail_to")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
    }
}
