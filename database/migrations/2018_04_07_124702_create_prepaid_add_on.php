<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrepaidAddOn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $add_on = \App\AddOn::query()
            ->create([
                'id' => \App\AddOn::ADD_ON_PREPAID,
                'name' => 'Prepaid',
                'price' => 9.99,
                'is_default' => true,
            ]);
        dispatch_now(new \App\Jobs\CreateStripeProductForAddOn($add_on));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\AddOn::query()
            ->where('id', \App\AddOn::ADD_ON_PREPAID)
            ->forceDelete();
    }
}
