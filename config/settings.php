<?php

return [
    'timezone' => 'Asia/Manila',
    'processing_fees' => [
        'appointment' => 1,
    ]
];