@extends('layouts.client2')
@section('vue-content')
    <?php 
        $customNumber = Request::segment(2);
    ?>
    <profile-index v-if="!showPublicEmployees" :custom_numer="{{ $customNumber }}"></profile-index>
    <employees-index v-else :custom_number="{{ $customNumber }}"></employees-index>
@endsection
