@extends('layouts.app')
@section('content')
<div>
    <comontime-subheader></comontime-subheader>
    <comontime-header></comontime-header>
    <section class="pt-5 pb-5">
        <comontime-product-tour></comontime-product-tour>
    </section>
    
    <!-- <comontime-footer></comontime-footer> -->
    @include('includes.footer')
</div>

@endsection