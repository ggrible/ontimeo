@extends('layouts.app')
@section('content')
<?php
    $email = Request::segment(2);
    $token = Request::segment(3);
?>
<setup-password email="{{ $email }}" token="{{ $token }}"></setup-password>
@endsection
