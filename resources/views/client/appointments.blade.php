@extends('layouts.client')
@section('vue-content')
    <appointments-index :user="{{ auth()->user() }}"></appointments-index>
@endsection
