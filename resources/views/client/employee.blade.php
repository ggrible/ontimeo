@extends('layouts.client')
@section('vue-content')
    <?php 
        $customNumber = Request::segment(2);
    ?>
    <employees-index :custom_number="{{ $customNumber }}"></employees-index>
@endsection
