@extends('layouts.client')
@section('vue-content')
    <account-settings-index :user="{{ auth()->user() }}"></account-settings-index>
@endsection
