@extends('layouts.app')
@section('content')
<div>
    <main role="main">
        <div class="container">
            <div class="rpw">
                <div class="jumbotron">
                <div class="container" style="height:200px;" >
                    <!-- <h3>Upload Profile Photo Here</h3> -->
                </div>
            </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="profile-photo">
                        <img src="/img/pro-pic.jpg" alt="" class="img-thumbnail">
                    </div>
                    <ul class="nav nav-pills nav-fill flex-column mt-5">
                        <li class="nav-item">
                        <a class="nav-link active" href="#"> <i class="fa fa-user"></i> Profile</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#"><i class="fa fa-calendar"></i> Appointments</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#"> <i class="fa fa-tasks"></i> Schedules</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#"><i class="fa fa-address-card"></i> My Employees</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#"> <i class="fa fa-group"></i> My Fans</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-9">
                    <div class="profile-name">
                        <h2>Berthold Medical</h2>
                    </div>
                    <section class="profile-section">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="box mt-2 mb-2">
                                    <h3>Travel Agent</h3>
                                    <hr />
                                </div>
                                <div class="box mt-2 mb-2">
                                    <h3>My Fans</h3>
                                    <hr />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="box mt-2 mb-2">
                                    <h3>Set an Appointment</h3>
                                    <hr />
                                </div>
                                <div class="box mt-2 mb-2">
                                    <h3>Watch us Grow</h3>
                                    <hr />
                                    <div class="donation-meter">
                                        <span class="glass">
                                            <span class="amount" style="height: 60%"></span>
                                            <strong class="total" style="bottom: 60%">300</strong>
                                        </span>
                                        <div class="bulb">
                                            <span class="red-circle"></span>
                                            <span class="filler">
                                                <span></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="box mt-2 mb-2">
                                    <h3>Status</h3>
                                    <hr />
                                </div>
                            </div>
                        </div>
                        <!-- <div class="box">
                            <h2>Manage Profile</h2>
                            <p>You can update your Personal and Company information here.</p>
                            <form action="" method="post" id="frmsubmit">
                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <div>
                                            <label for="">User Name:</label>
                                            <div class="form-control">ram123</div>
                                        </div>
                                        <div>
                                            <label for="">Password:</label>
                                            <input type="password" class="form-control" id="ppword" name="pword" placeholder="Enter a new password if you wish to update the current one">
                                        </div>
                                        <div>
                                            <label for="">Email:</label>
                                            <input type="text" class="form-control" id="email" name="email" value="ram@gmail.com" required="required">
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6">
                                                <label for="">First Name:</label>
                                                <input type="text" class="form-control" id="fname" name="fname" value="Ram" required="required">
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <label for="">Last Name:</label>
                                                <input type="text" class="form-control" id="lname" name="lname" value="Zafra" required="required">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6">
                                                <label for="">Mobile:</label>
                                                <input type="text" class="number form-control" id="mobile" name="mobile" value="123413" required="required" maxlength="11">
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <label for="">Landline:</label>
                                                <input type="text" class="number form-control" id="landline" name="landline" value="234234" required="required" maxlength="10">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6">
                                                <label for="">Role:</label>
                                                <div class="form-control">Client</div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <label for="">Status:</label>
                                                <div class="form-control">Active</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <div>
                                            <label for="">Company Name:</label>
                                            <input type="text" class="form-control" id="company_name" name="company_name" value="Berthold Medical">
                                        </div>
                                        <div>
                                            <label for="">Position/Designation:</label>
                                            <input type="text" class="form-control" id="position" name="position" value="ceo">
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6">
                                                <label for="">City:</label>
                                                <input type="text" class="form-control" id="city" name="city" value="calbayog">
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <label for="">State:</label>
                                                <input type="text" class="form-control" id="state" name="state" value="na">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6">
                                                <label for="">Zip:</label>
                                                <input type="text" class="number form-control" id="zip" name="zip" value="6710">
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <label for="">Country:</label>
                                                <input type="text" class="number form-control" id="country" name="country" value="ph">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6">
                                                <label for="">Time Zone:</label>
                                                <select id="timezone_id" name="timezone_id" class="form-control">
                                                                                <option value="1">(GMT-11:00) Midway Island</option>
                                                                                <option value="2">(GMT-11:00) Samoa</option>
                                                                                <option value="3">(GMT-10:00) Hawaii</option>
                                                                                <option value="4">(GMT-09:00) Alaska</option>
                                                                                <option value="5">(GMT-08:00) Pacific Time (US &amp; Canada)</option>
                                                                                <option value="6">(GMT-08:00) Tijuana</option>
                                                                                <option value="7">(GMT-07:00) Arizona</option>
                                                                                <option value="8">(GMT-07:00) Mountain Time (US &amp; Canada)</option>
                                                                                <option value="9">(GMT-07:00) Chihuahua</option>
                                                                                <option value="10">(GMT-07:00) Mazatlan</option>
                                                                                <option value="11">(GMT-06:00) Mexico City</option>
                                                                                <option value="12">(GMT-06:00) Monterrey</option>
                                                                                <option value="13">(GMT-06:00) Saskatchewan</option>
                                                                                <option value="14" selected="selected">(GMT-06:00) Central Time (US &amp; Canada)</option>
                                                                                <option value="15">(GMT-05:00) Eastern Time (US &amp; Canada)</option>
                                                                                <option value="16">(GMT-05:00) Indiana (East)</option>
                                                                                <option value="17">(GMT-05:00) Bogota</option>
                                                                                <option value="18">(GMT-05:00) Lima</option>
                                                                                <option value="19">(GMT-04:30) Caracas</option>
                                                                                <option value="20">(GMT-04:00) Atlantic Time (Canada)</option>
                                                                                <option value="21">(GMT-04:00) La Paz</option>
                                                                                <option value="22">(GMT-04:00) Santiago</option>
                                                                                <option value="23">(GMT-03:30) Newfoundland</option>
                                                                                <option value="24">(GMT-03:00) Buenos Aires</option>
                                                                                <option value="25">(GMT-03:00) Greenland</option>
                                                                                <option value="26">(GMT-02:00) Stanley</option>
                                                                                <option value="27">(GMT-01:00) Azores</option>
                                                                                <option value="28">(GMT-01:00) Cape Verde Is.</option>
                                                                                <option value="29">(GMT) Casablanca</option>
                                                                                <option value="30">(GMT) Dublin</option>
                                                                                <option value="31">(GMT) Lisbon</option>
                                                                                <option value="32">(GMT) London</option>
                                                                                <option value="33">(GMT) Monrovia</option>
                                                                                <option value="34">(GMT+01:00) Amsterdam</option>
                                                                                <option value="35">(GMT+01:00) Belgrade</option>
                                                                                <option value="36">(GMT+01:00) Berlin</option>
                                                                                <option value="37">(GMT+01:00) Bratislava</option>
                                                                                <option value="38">(GMT+01:00) Brussels</option>
                                                                                <option value="39">(GMT+01:00) Budapest</option>
                                                                                <option value="40">(GMT+01:00) Copenhagen</option>
                                                                                <option value="41">(GMT+01:00) Ljubljana</option>
                                                                                <option value="42">(GMT+01:00) Madrid</option>
                                                                                <option value="43">(GMT+01:00) Paris</option>
                                                                                <option value="44">(GMT+01:00) Prague</option>
                                                                                <option value="45">(GMT+01:00) Rome</option>
                                                                                <option value="46">(GMT+01:00) Sarajevo</option>
                                                                                <option value="47">(GMT+01:00) Skopje</option>
                                                                                <option value="48">(GMT+01:00) Stockholm</option>
                                                                                <option value="49">(GMT+01:00) Vienna</option>
                                                                                <option value="50">(GMT+01:00) Warsaw</option>
                                                                                <option value="51">(GMT+01:00) Zagreb</option>
                                                                                <option value="52">(GMT+02:00) Athens</option>
                                                                                <option value="53">(GMT+02:00) Bucharest</option>
                                                                                <option value="54">(GMT+02:00) Cairo</option>
                                                                                <option value="55">(GMT+02:00) Harare</option>
                                                                                <option value="56">(GMT+02:00) Helsinki</option>
                                                                                <option value="57">(GMT+02:00) Istanbul</option>
                                                                                <option value="58">(GMT+02:00) Jerusalem</option>
                                                                                <option value="59">(GMT+02:00) Kyiv</option>
                                                                                <option value="60">(GMT+02:00) Minsk</option>
                                                                                <option value="61">(GMT+02:00) Riga</option>
                                                                                <option value="62">(GMT+02:00) Sofia</option>
                                                                                <option value="63">(GMT+02:00) Tallinn</option>
                                                                                <option value="64">(GMT+02:00) Vilnius</option>
                                                                                <option value="65">(GMT+03:00) Baghdad</option>
                                                                                <option value="66">(GMT+03:00) Kuwait</option>
                                                                                <option value="67">(GMT+03:00) Nairobi</option>
                                                                                <option value="68">(GMT+03:00) Riyadh</option>
                                                                                <option value="69">(GMT+03:00) Moscow</option>
                                                                                <option value="70">(GMT+03:30) Tehran</option>
                                                                                <option value="71">(GMT+04:00) Baku</option>
                                                                                <option value="72">(GMT+04:00) Volgograd</option>
                                                                                <option value="73">(GMT+04:00) Muscat</option>
                                                                                <option value="74">(GMT+04:00) Tbilisi</option>
                                                                                <option value="75">(GMT+04:00) Yerevan</option>
                                                                                <option value="76">(GMT+04:30) Kabul</option>
                                                                                <option value="77">(GMT+05:00) Karachi</option>
                                                                                <option value="78">(GMT+05:00) Tashkent</option>
                                                                                <option value="79">(GMT+05:30) Kolkata</option>
                                                                                <option value="80">(GMT+05:45) Kathmandu</option>
                                                                                <option value="81">(GMT+06:00) Ekaterinburg</option>
                                                                                <option value="82">(GMT+06:00) Almaty</option>
                                                                                <option value="83">(GMT+06:00) Dhaka</option>
                                                                                <option value="84">(GMT+07:00) Novosibirsk</option>
                                                                                <option value="85">(GMT+07:00) Bangkok</option>
                                                                                <option value="86">(GMT+07:00) Jakarta</option>
                                                                                <option value="87">(GMT+08:00) Krasnoyarsk</option>
                                                                                <option value="88">(GMT+08:00) Chongqing</option>
                                                                                <option value="89">(GMT+08:00) Hong Kong</option>
                                                                                <option value="90">(GMT+08:00) Kuala Lumpur</option>
                                                                                <option value="91">(GMT+08:00) Perth</option>
                                                                                <option value="92">(GMT+08:00) Singapore</option>
                                                                                <option value="93">(GMT+08:00) Taipei</option>
                                                                                <option value="94">(GMT+08:00) Ulaan Bataar</option>
                                                                                <option value="95">(GMT+08:00) Urumqi</option>
                                                                                <option value="96">(GMT+09:00) Irkutsk</option>
                                                                                <option value="97">(GMT+09:00) Seoul</option>
                                                                                <option value="98">(GMT+09:00) Tokyo</option>
                                                                                <option value="99">(GMT+09:30) Adelaide</option>
                                                                                <option value="100">(GMT+09:30) Darwin</option>
                                                                                <option value="101">(GMT+10:00) Yakutsk</option>
                                                                                <option value="102">(GMT+10:00) Brisbane</option>
                                                                                <option value="103">(GMT+10:00) Canberra</option>
                                                                                <option value="104">(GMT+10:00) Guam</option>
                                                                                <option value="105">(GMT+10:00) Hobart</option>
                                                                                <option value="106">(GMT+10:00) Melbourne</option>
                                                                                <option value="107">(GMT+10:00) Port Moresby</option>
                                                                                <option value="108">(GMT+10:00) Sydney</option>
                                                                                <option value="109">(GMT+11:00) Vladivostok</option>
                                                                                <option value="110">(GMT+12:00) Magadan</option>
                                                                                <option value="111">(GMT+12:00) Auckland</option>
                                                                                <option value="112">(GMT+12:00) Fiji</option>
                                                                            </select>
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <label for="">Time Format:</label>
                                                <select class="form-control" id="time_format" name="time_format">
                                                    <option value="12" selected="selected">12</option>
                                                    <option value="24">24</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div><br>
                                <div id="infomsg">
                                            </div><br>
                                <div class="btns">
                                    <input type="hidden" name="pro" value="update-profile">
                                    <button type="submit" class="btn btn-primary btn-lg btn-flat" value="Submit"><i class="fa fa-save"></i> Update Profile</button>
                                </div>
                            </form>
                        </div> -->
                    </section>
                </div>
            </div>
        </div>
    </main>
</div>
@endsection
