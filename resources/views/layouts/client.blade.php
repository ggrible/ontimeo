@extends('layouts.app')
@section('content')
<div>
    <main role="main">
        <div class="container">
            <div class="cover-photo">
                <div class="mobile-upload-btn {{ auth()->user()->photo ? 'has-cover-photo' : '' }}">
                    <a @click="$refs.coverPhoto.click()" href="javascript:void(0)"><i class="fa fa-camera"></i></a>
                </div>
                <ul class="top-links {{ auth()->user()->photo ? 'has-cover-photo' : '' }}">
                    <li class="custom-dropdown"><a href="/account-settings"><i class="fa fa-cog"></i> <span>Settings</span>
                    </a>
                    <ul class="custom-dropdown-list">
                            <li>
                                <a href="#">Accounting</a>
                            </li>
                            <li>
                                <a href="/transactions" style="border-bottom:none;">Transactions</a>
                            </li>
                        </ul>
                    </li>
                    <li><a href="/email-sync"><i class="fa fa-refresh"></i> <span>Email Sync</span></a></li>
                    <li>
                        <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i> <span>Logout</span>
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
                <div class="jumbotron-wrapper">
                    <div class="jumbotron cover-photo {{ auth()->user()->photo ? 'has-cover-photo' : '' }}" style="background-image:url({{ auth()->user()->photo ? '/storage/' . auth()->user()->cover_photo : '' }});">
                        <div class="container" style="height:200px;">
                            <div class="upload-box" @click="$refs.coverPhoto.click()" v-show="!uploadingCoverPhoto">
                                <input  @change="onChangeCoverPhoto" type="file" class="hidden" style="display:none;" ref="coverPhoto">
                                <i class="fa fa-camera"></i> &nbsp; Upload Cover Photo
                            </div>
                            <div class="upload-box-uploading text-center" style="display:none;" v-show="uploadingCoverPhoto">
                                Uploading...
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row profile-row">
                <div class="col-lg-3 col-md-12">
                    <div class="text-center">
                        <div class="profile-frame mb-2">
                            <img id="profile-photo" class="profile-image" src="{{ auth()->user()->photo ? '/storage/' . auth()->user()->photo : '/img/pro-pic.jpg' }}" alt="User Profile">
                            <input @change="onChangeProfilePhoto" id="upload-profile-photo" type="file" style="display:none;">
                            <div @click="chooseProfilePhoto" class="profile-upload-btn" v-if="!uploadingProfilePhoto" :style="uploadingProfilePhoto ? 'display:block !important;' : ''">
                                <i class="fa fa-camera"></i> &nbsp; Upload Profile
                            </div>
                            <div class="profile-photo-uploading" v-show="uploadingProfilePhoto" style="display:none;">
                                Uploading....
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 text-center">
                    <h2 class="profile-name {{ auth()->user()->photo ? 'has-cover-photo' : '' }}">{{ auth()->user()->name }}</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <!-- <div class="member-activity mt-2">
                        <h4>Member Activity</h4>
                        <p>
                            Last Login <br>
                            <i class="fa fa-clock-o"></i> Tuesday, 6:46 PM
                        </p>
                        <hr />
                        <p>
                            Member Since <br>
                            <i class="fa fa-calendar"></i> March 1, 2018
                        </p>
                    </div> -->
                    <ul class="nav nav-pills nav-fill flex-column mt-3">
                        <li class="nav-item">
                            <a class="nav-link {{ Request::is('profile') ? 'active' : '' }}" href="{{ url('/profile') }}"> <i class="fa fa-user"></i> Profile</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ Request::is('appointments') ? 'active' : '' }}" href="{{ url('/appointments') }}"><i class="fa fa-calendar"></i> Appointments</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link  {{ Request::is('schedules') ? 'active' : '' }}" href="{{ url('/schedules') }}"> <i class="fa fa-tasks"></i> Schedules</a>
                        </li>
                        <!-- <li class="nav-item" :id="$root.userAddOns" v-show="$root.userAddOns.indexOf('Extensions') > -1" v-cloak>
                            <a class="nav-link {{ Request::is('employees') ? 'active' : '' }}" href="{{ url('/employees') }}"><i class="fa fa-address-card"></i> My Employees</a>
                        </li> -->
                    </ul>
                </div>
                <div class="col-md-9">
                    <!-- vue components goes here  -->
                    @yield('vue-content')
                </div>
                <div class="bottom-blocker"></div>
            </div>
        </div>
    </main>
</div>
@endsection
