@extends('layouts.app')
@section('content')
<div>
    <main role="main">
        <div class="container">
            <div class="cover-photo">
                <div class="jumbotron-wrapper">
                    <div class="jumbotron cover-photo {{ $user->photo ? 'has-cover-photo' : '' }}" style="background-image:url({{ $user->photo ? '/storage/' . $user->cover_photo : '' }});">
                        <div class="container" style="height:200px;">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row profile-row">
                <div class="col-lg-3 col-md-12">
                    <div class="text-center">
                        <div class="profile-frame mb-2">
                            <img id="profile-photo" class="profile-image" src="{{ $user->photo ? '/storage/' . $user->photo : '/img/pro-pic.jpg' }}" alt="User Profile">
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 text-center">
                    <h2 class="profile-name {{ $user->photo ? 'has-cover-photo' : '' }}">{{ $user->name }}</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="member-activity mt-2">
                        <h4>Member Activity</h4>
                        <p>
                            Last Login <br>
                            <i class="fa fa-clock-o"></i> Tuesday, 6:46 PM
                        </p>
                        <hr />
                        <p>
                            Member Since <br>
                            <i class="fa fa-calendar"></i> March 1, 2018
                        </p>
                    </div>
                    <ul class="nav nav-pills nav-fill flex-column mt-3">
                    <!-- v-show="$root.userAddOns.indexOf('Extensions') > -1" -->
                    <li class="nav-item" v-cloak>
                        <a :class="{ 'active': !showPublicEmployees }" @click="showPublicEmployees = false" class="nav-link" href="javascript:void(0)"><i class="fa fa-user"></i> Profile</a>
                    </li>    
                    <li class="nav-item" v-cloak>
                            <a :class="{ 'active': showPublicEmployees }" @click="showPublicEmployees = true" class="nav-link" href="javascript:void(0)"><i class="fa fa-address-card"></i> My Employees</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-9">
                    <!-- vue components goes here  -->
                    @yield('vue-content')
                </div>
                <div class="bottom-blocker"></div>
            </div>
        </div>
    </main>
</div>
@endsection
