<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Comontime - Admin</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet"
          type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link href="/css/app-admin.css" rel="stylesheet"/>
    <script>
        window.Laravel = <?= json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>;
        window.Comontime = <?= json_encode(\App\Comontime::scriptVariables()); ?>;
    </script>
</head>

<body class="theme-orange">
<div id="app" v-cloak>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar" style="background: #ffff;">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse"
                   data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="/admin">
                    <img src="/img/ontime_logo.png" alt="" class="admin-logo" width="120">
                </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    {{--<li class="pull-right"><a href="javascript:void(0);">Logout</a></li>--}}
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="/img/user.png" width="48" height="48" alt="User"/>
                </div>
                <form ref="formLogout" method="post" action="/logout" class="d-none">
                    {{ csrf_field() }}
                </form>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-text="user.name"></div>
                    <div class="email" v-text="user.email"></div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="/logout" @click.prevent="$refs.formLogout.submit()"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    <li :class="{active: $route.path === '/admin'}">
                        <a href="/admin">
                            <i class="material-icons">home</i>
                            <span>Home</span>
                        </a>
                    </li>
                    <li :class="{active: $route.meta.root === 'packages'}">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">widgets</i>
                            <span>Packages</span>
                        </a>
                        <ul class="ml-menu">
                            <li :class="{active: $route.name === 'packages'}">
                                <router-link active-class="toggled" :to="{name: 'packages'}">View Packages</router-link>
                            </li>
                            <li :class="{active: $route.name === 'terms'}">
                                <router-link active-class="toggled" :to="{name: 'terms'}">Terms</router-link>
                            </li>
                            <li :class="{active: $route.name === 'addons'}">
                                <router-link active-class="toggled" :to="{name: 'addons'}">Add Ons</router-link>
                            </li>
                        </ul>
                    </li>
                    <li :class="{active: $route.meta.root === 'case-study'}">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">widgets</i>
                            <span>Companies</span>
                        </a>
                        <ul class="ml-menu">
                            <li :class="{active: $route.name === 'case-study-category'}">
                                <router-link active-class="toggled" :to="{name: 'case-study-category'}">Category</router-link>
                            </li>
                            <li :class="{active: $route.name === 'case-study'}">
                                <router-link active-class="toggled" :to="{name: 'case-study'}">Case Study</router-link>
                            </li>
                        </ul>
                    </li>
                    <li :class="{active: $route.meta.root === 'admin-transactions'}">
                        <router-link active-class="toggled" :to="{name: 'admin-transactions'}">
                            <i class="material-icons">attach_money</i>
                            <span>Transactions</span>
                        </router-link>
                    </li>
                    <li :class="{active: $route.meta.root === 'admin-withdrawal'}">
                        <router-link active-class="toggled" :to="{name: 'admin-withdrawal'}">
                            <i class="material-icons">attach_money</i>
                            <span>Withdrawal</span>
                        </router-link>
                    </li>
                    <li :class="{active: $route.meta.root === 'admin-coupons'}">
                        <router-link active-class="toggled" :to="{name: 'admin-coupons'}">
                            <i class="material-icons">local_offer</i>
                            <span>Coupons</span>
                        </router-link>
                    </li>
                    <li :class="{active: $route.meta.root === 'admin-leads'}">
                        <router-link active-class="toggled" :to="{name: 'admin-leads'}">
                            <i class="material-icons">people</i>
                            <span>Leads</span>
                        </router-link>
                    </li>
                    <li :class="{active: $route.meta.root === 'admin-faqs'}">
                        <router-link active-class="toggled" :to="{name: 'admin-faqs'}">
                            <i class="material-icons">live_help</i>
                            <span>FAQs</span>
                        </router-link>
                    </li>
                    <li :class="{active: $route.meta.root === 'admin-settings'}">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">settings</i>
                            <span>Settings</span>
                        </a>
                        <ul class="ml-menu">
                            <li :class="{active: $route.meta.root === 'admin-settings'}">
                                <router-link active-class="toggled" :to="{name: 'admin-settings'}">
                                    <span>Settings</span>
                                </router-link>
                            </li>
                            <li :class="{active: $route.name === 'stripe-config'}">
                                <router-link active-class="toggled" :to="{name: 'stripe-config'}">Stripe Configuration</router-link>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <router-view></router-view>
    </section>
</div>
<script src="/js/app-admin.js"></script>
</body>
</html>
