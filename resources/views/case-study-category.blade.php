@extends('layouts.app')
@section('content')
<div>
    <comontime-subheader></comontime-subheader>
    <comontime-header></comontime-header>
    <div class="container">
        <div class="row">
            <div class="col-md-12 p-5">
                <h1>{{ $category }}</h1>

                <?php if ($case_studies != null && count($case_studies)) { ?>
                    <div id="user_casestudy">
                        <div class="image">
                            <?php foreach ($case_studies as $CS) { ?>
                                <?php $user_data = $CS->getOwnerUser(); ?>
                                
                                <div class="image-box" style="text-align: center; padding: 10px; width: 135px; display: inline-block;">
                                    <a href="#" class="user_image" data-user_name="<?php echo $user_data->name; ?>" data-company_name="<?php echo $user_data->company_name; ?>" data-user_case_study="<?php echo $CS->description; ?>">
                                        <img style="border-radius: 50%; vertical-align: bottom !important;" src="/img/user.png" width="90" height="90" alt="<?php echo $user_data->name; ?>">
                                    </a>
                                </div>
                            <?php } ?>
                        </div>

                        <hr>

                        <section>
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-6">
                                        <img src="/img/Targetprocess_Customers-page_X2.png" alt="User" class="img-fluid">
                                    </div>
                                    <div class="col-md-6">
                                        <blockquote class="blockquote">
                                            <p class="mb-0 user_case_study"><?php echo $case_studies[0]->description; ?></p> 
                                            <footer class="blockquote-footer user_name"><?php echo $case_studies[0]->getOwnerUser()->name; ?> in <cite class="company_name"><?php echo $case_studies[0]->getOwnerUser()->company_name; ?></cite></footer>
                                        </blockquote>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                <?php } else { ?>
                    <blockquote class="blockquote-footer">No testimonials</blockquote>
                <?php } ?>

            </div>
        </div>
    </div>
    
    <!-- <comontime-footer></comontime-footer> -->
    @include('includes.footer')
</div>
@endsection