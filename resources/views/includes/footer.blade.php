<!-- START FOOTER -->
<div>
    <div class="footer footer1 two">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-lg-4 col-sm-6 text-padding">
                        <h4 class="white">About Ontime Org</h4>
                        <div class="footer_title_line"></div>
                        <ul class="quick_links">
                        <li><a href="/contact-us" title="ContactUs">Contact Us</a></li>
                            <!-- <li><a href="/case-study" title="CaseStudy">Case Study</a></li> -->
                            <!-- <li><a href="/endoresement" title="Endorsement">Endorsement</a></li> -->
                            <li><a href="/privacy-policy" title="Privacy Policy">Privacy Policy</a></li>
                        </ul>
                        <div class="newsletter mt-3">
		    				<!-- <p class="padd_bot1 margin_top1"> Sign up for special offers</p> -->
                            <form method="get" action="#" class="form-inline">
                                <label for="special-offer" id="special-offer" class="mb-3">Sign up for special offers</label>
                                <input class="form-control" name="samplees" id="samplees" placeholder="E-mail Address" type="text">
                                <input value="Submit" class="btn btn-primary" type="submit">
                            </form>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 text-padding">
                        <h4 class="white">Client Case Study</h4>
                        <div class="footer_title_line"></div>
                        <ul class="quick_links">
                            <?php
                                $categories = App\CaseStudyCategory::all();

                                foreach ($categories as $C)
                                {
                                    echo "<li><a href='/case-study/". $C->name ."' title='". $C->name ."'>". $C->name ."</a></li>";
                                }
                            ?>
                        </ul>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 text-padding">
                        <h4 class="white">Products & Services</h4>
                        <div class="footer_title_line"></div>
                        <ul class="quick_links">
                            <li><a href="/product-tour" title="Produc Tour">Product Tour</a></li>
                            <li><a href="/how-it-works" title="HowItWorks">How It Works</a></li>
                            <li><a href="/faqpage" title="Ask me anything">Ask Me Anything</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-lg-2 col-sm-6 col-xs-12 text-padding">
                        <h4 class="white">Contact Us</h4>
                        <div class="footer_title_line"></div>
                        <ul class="quick_links">
                            <li><a href="skype:ogadriva?chat" title="Skype"><i class="fa fa-skype"></i> Ontimeorg</a></li>
                            <li><a href="mailto:info@ogadriva.com" title="Email"><i class="fa fa-envelope"></i> info@ontimeo.com</a></li>
                            <li><a href="tel:16822468605" title="Phone"><i class="fa fa-phone"></i> +1 682 246 8605 (Dallas)</a></li>
                        </ul>
                    </div>
            </div>
        </div>
    </div>
    <div class="footer_payments_types">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col md-8 md-offset2 col-lg-6 imgs-padding">
                    <ul class="payment_logos">
                        <li class="white">Payments <br>We Accept</li>
                        <li><img src="/img/payment_logo1.jpg" alt=""></li>
                        <li><img src="/img/payment_logo2.jpg" alt=""></li>
                        <li><img src="/img/payment_logo3.jpg" alt=""></li>
                        <li><img src="/img/payment_logo4.jpg" alt=""></li>
                        <li><img src="/img/payment_logo5.png" alt=""></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-12 col md-8 md-offset2 col-lg-6 imgs-padding">
                    <ul class="payment_logos two">
						<li class="white">Our <br>Awards</li>
						<li><img src="/img/award-img1.png" alt=""></li>
						<li><img src="/img/award-img2.png" alt=""></li>
						<li><img src="/img/award-img3.png" alt=""></li>
						<li><img src="/img/award-img4.png" alt=""></li>
						<li><img src="/img/award-img2.png" alt=""></li>
						<!-- <li><a href="#" class="chat_but"><i class="fa fa-comments"></i> Live Chat</a></li> -->
					</ul>

                </div>
            </div>
        </div>
    </div>
    <div class="copyrights">
		<div class="container">
			<div class="one_half"><span>© 2015 All rights reserved.</span></div>
			<div class="one_half last">
				<ul class="social_icons">
					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
					<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
					<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
					<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
					<li><a href="#"><i class="fa fa-wordpress"></i></a></li>
					<li><a href="#"><i class="fa fa-android"></i></a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<!-- END FOOTER -->
<!-- /resources/views/includes/footer.blade.php -->