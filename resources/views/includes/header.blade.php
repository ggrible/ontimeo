<?php 
    $user_logged        = Auth::user();
    $current_page_url   = Request::segment(2);
?>

<style scoped>
    .subHeader { width: 100%; text-align: left; padding: 5px 0px 5px 0; }
    .btn.btn.btn-link{ color: #8d8c8c; }
    .btn.btn-link:hover { color: #fff; text-decoration: none; }
    .btn.btn-success { background-color: #00bc8c; border-color: #00bc8c; }
    .btn.btn-success:hover { background-color: #008966; border-color: #007f5e; }
</style>

<!-- START HEADER -->
<nav class="navbar navbar-expand-md navbar-dark bg-dark nav1 pt-0 pb-0">
    <div class="subHeader" style="padding: 5px 0px; float: right !important; text-align: right !important;">
        <div class="container">
            <?php if ($user_logged == null) { ?>
                <div class="float-right">
                    <button class="btn btn-link btn-sm pt-0 pb-0">Login</button>
                    <button class="btn btn-success btn-xs pt-0 pb-0">Join</button>
                </div>
            <?php } else { ?>
                <div class="float-right" style="float: right !important; text-align: right !important;">
                    <a href='<?php echo $user_logged->isSuperAdmin() ? "/admin" : "/profile"?>' class="btn btn-link btn-sm pt-0 pb-0"><i class="fa fa-user"></i>&nbsp; <?php echo $user_logged->name; ?></a>
                    <a href="/logout" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn btn-link btn-sm pt-0 pb-0" ><i class="fa fa-sign-out"></i>&nbsp; Logout</a>
                    <form id="logout-form" action="/logout" method="POST" style="display: none;"><input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"></form>
                </div>
            <?php } ?>
        </div>
    </div>
</nav>

<nav class="navbar navbar-expand-md navbar-light bg-light sticky-top">
    <div class="container">
        <a class="navbar-brand" href="/">
            <img src="/img/ontime_logo.png" alt="" class="img-responsive" width="120">
        </a>

        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="navbar-collapse collapse" id="navbarsExampleDefault" style="">
            <ul class="navbar-nav ml-auto">
                <li class='nav-item <?php echo $current_page_url == "meet-the-team" ? "active" : ""; ?>'><a class="nav-link" href="/">Home</a></li>
                <li class='nav-item <?php echo $current_page_url == "price-plans" ? "active" : ""; ?>'><a class="nav-link" href="/price-plans">Price Plans</a></li>
                <li class='nav-item <?php echo $current_page_url == "product-tour" ? "active" : ""; ?>'><a class="nav-link" href="/product-tour">Product Tour</a></li>
                <li class='nav-item <?php echo $current_page_url == "privacy-policy" ? "active" : ""; ?>'><a class="nav-link" href="/privacy-policy">Privacy Policy</a></li>
                <li class='nav-item <?php echo $current_page_url == "meet-the-team" ? "active" : ""; ?>'><a class="nav-link" href="/meet-the-team">Meet the Team</a></li>
            </ul>
        </div>
    </div>
</nav>
<!-- END HEADER -->
<!-- /resources/views/includes/header.blade.php -->