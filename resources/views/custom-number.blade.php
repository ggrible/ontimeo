@extends('layouts.app')
@section('content')
<div>
    <comontime-subheader></comontime-subheader>
    <comontime-header></comontime-header>
    <section class="pt-5    ">
        <div class="container">
            <!-- <nav class="breadcrumb mt-0" style="background:transparent;">
                <a class="breadcrumb-item" href="/">Home</a>
                <a class="breadcrumb-item" href="/price-plan">Package</a>
                <span class="breadcrumb-item active">Custom Number</span>
            </nav> -->
            <div class="row">
                <h1>Custom Number</h1>
            </div>
            <div class="row">
                <h4>Good news! You get a free Second Phone Number with this order.</h4>
                <custom-number></custom-number>
            </div>
        </div>
        <div class="container-fluid">
        <div class="row bg-primary p-5 mb-0">
            <div class="col-md-4">
                <h3>No Equipment</h3>
            </div>
            <div class="col-md-4">
                <h3>No contacts.</h3>
            </div>
            <div class="col-md-4">
                <h3>No hassle.</h3>
            </div>
        </div>
        </div>
    </section>
    
    <!-- <comontime-footer></comontime-footer> -->
    @include('includes.footer')
</div>
@endsection