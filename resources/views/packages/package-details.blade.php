@extends('layouts.app')
@section('content')
<div>
    <comontime-subheader></comontime-subheader>
    <comontime-header></comontime-header>
    <package-term :id="<?php echo $id; ?>"></package-term>
    <comontime-footer></comontime-footer>
</div>
@endsection