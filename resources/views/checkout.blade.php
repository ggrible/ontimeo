@extends('layouts.app')
@section('content')
<div>
    <comontime-subheader></comontime-subheader>
    <comontime-header></comontime-header>
    <checkout></checkout>
    
    <!-- <comontime-footer></comontime-footer> -->
    @include('includes.footer')
</div>
@endsection