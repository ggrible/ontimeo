Hello Admin,<br><br>

Request for demo:<br><br>
<b>Name:</b> {{ $user->last_name }}, {{ $user->first_name }}<br>
<b>E-mail:</b> {{ $user->email }}<br>
<b>Contact Number:</b> {{ $user->contact }}<br>
<b>Company:</b> {{ $user->company }}<br>
<b>Website:</b> {{ $user->website }}<br>
<b>Comments:</b> {{ $user->comments }}<br><br>
 
Thank You,
<br/>
<i>Ontimeo.com</i>