Hello Admin,<br><br>

Call back:<br><br>

<b>Date and Time Sent:</b> {{ $user->support_datesent }}<br>
<b>Day:</b> {{ $user->support_day }}<br>
<b>Time:</b> {{ $user->support_time }}<br>
<b>Contact Number:</b> {{ $user->support_contact }}<br><br>
<b>Comments:</b> {{ $user->support_comment }}<br><br>
 
Thank You,
<br/>
<i>Ontimeo.com</i>