@extends('layouts.app')
@section('content')
<div>
    <comontime-subheader></comontime-subheader>
    <comontime-header></comontime-header>

    <section class="pt-5 pb-5 hero-banner-contact-us">
        <div class="container" style="text-align: center;">
            <h1>Contact Us</h1>
            <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos, beatae earum, optio dolore libero temporibus aliquam animi, qui saepe odit voluptas culpa voluptatibus consequatur eius ipsum architecto sapiente deleniti quis.
            </p>
            <button type="button" class="btn btn-default btn-circle-lg waves-effect waves-circle waves-float" v-scroll-to="'.contact-us'">
                <i class="material-icons">down</i>
            </button>
        </div>
    </section>
    <section class="pt-5 pb-5 contact-us">
        <div class="container" style="text-align: center;">
            <contact-us-modal></contact-us-modal>
            <h1>Reason for Inquiry</h1>
            <p>Please select the purpose for your inquiry</p>

            <div class="row">
                <div class="col-lg-4 col-md-12 col-sm-12 last">
                    <div style="height: 189px; text-align: center;">
                        <div class="text_holder">
                            <div class="text">
                                <img src="/img/customer support.png" width="30%"/>
                                <h4 class="lessmar">Request A Demo</h4>
                                <p>Request a demonstration of Ontimeo's award-winning, differentiated instruction solutions.</p>
                                <button id="button-contact-demo-showpopup" class="btn btn-lg" style="border: 1px solid #e84c3c;">Request A Demo</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end item-->
                <div class="col-lg-4 col-md-12 col-sm-12 last">
                    <div style="height: 189px; text-align: center;">
                        <div class="text_holder">
                            <div class="text">
                                <img src="/img/customer support.png" width="30%"/>
                                <h4 class="lessmar">Sales Inquiry</h4>
                                <p>Reach out to our Sales team directly for immediate assistance with all sales-related inquiries.</p>
                                <button id="button-contact-sales-showpopup" class="btn btn-lg" style="border: 1px solid #e84c3c;">Sales Inquiry</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end item-->
                <div class="col-lg-4 col-md-12 col-sm-12 last">
                    <div style="height: 189px; text-align: center;">
                        <div class="text_holder">
                            <div class="text">
                                <img src="/img/customer support.png" width="30%"/>
                                <h4 class="lessmar">Customer Support</h4>
                                <p>Get in touch with customer support for assistance with your Ontimeo implementation and others.</p>
                                <button id="button-contact-support-showpopup" class="btn btn-lg" style="border: 1px solid #e84c3c;">Customer Support</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="pt-5 pb-5 map-address">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Corporate Headquarters</h2>
                            <p>Corporate Headquarters</p>
                            <p>Ontime Organiser Inc</p>
                            <p>9722 Forest Lane., Suite 490</p>
                            <p>Dallas, Tx 75243</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h2>General Contact</h2>
                            <p>Toll-Free: 888-900-000</p>
                            <p>Phone: 682-246-8605</p>
                            <p>Fax: 682-246-8604</p>
                            <p>Email: info@ontimeo.com</p>
                        </div>
                    </div>

                </div>
                <div class="col-md-6">
                    <div>
                        <img style="width:100%;" src="/img/map-location2.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- <comontime-footer></comontime-footer> -->
    @include('includes.footer')

    <!-- START MODAL DEMO -->
    <div id="modal-contact-demo" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5>Request a demo</h5>
                    <button id="button-contact-demo-closepopup" type="button" class="close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <form id="form_contact_requestdemo">
                        <div class="row">
                            <div class="col-md-12" style="text-align: center;">
                                <p style="line-height: 18px;">
                                    Interested in taking a closer look at OnTimeo?
                                    <br>Please complete the form below to request a demo. 
                                    <br>We'll contact you shortly to schedule an online demonstration.
                                    <br>
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="first_name">First Name</label>
                                <input id="first_name" type="text" name="first_name" class="form-control">
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="last_name">Last Name</label>
                                <input id="last_name" type="text" name="last_name" class="form-control">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="form_email">E-mail</label>
                                <input id="form_email" type="email" name="form_email" class="form-control">
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="form_contact">Phone Number</label>
                                <input id="form_contact" type="text" name="form_contact" class="form-control">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="form_company">Company</label>
                                <input id="form_company" type="text" name="form_company" class="form-control">
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="form_website">Website</label>
                                <input id="form_website" type="email" name="form_website" class="form-control">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="form_comment">Comments</label>
                                <textarea id="form_comment" name="form_comment" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p class="pull-left" style="font-size: 11px;">* Your phone number will not be used for marketing purposes.</p>
                                <div class="pull-right">
                                    <button id="button-contact-demo-sendform" type="button" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END MODAL DEMO -->

    <!-- START MODAL SALES -->
    <div id="modal-contact-sales" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5>Sales Inquiry</h5>
                    <button id="button-contact-sales-closepopup" type="button" class="close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <form id="form_contact_sales">
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END MODAL SALES -->

    <!-- START MODAL SUPPORT -->
    <div id="modal-contact-support" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5>Customer Support</h5>
                    <button id="button-contact-support-closepopup" type="button" class="close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <form id="form_contact_callback">
                        <div class="row">
                            <div class="col-sm-12"><p class="lead" style="text-align: center;">When should we call you back?</p></div>
                            <div class="form-group col-sm-6">
                                <label for="form_support_day">Day</label>
                                <select id="form_support_day" name="form_support_day" class="custom-select">
                                    <option value="TODAY" selected>Today</option>
                                    <option value="TOMORROW">Tomorrow</option>
                                    <option value="AFTER2DAYS">After 2 business days</option>
                                    <option value="NEXTWEEK">Next Week</option>
                                </select>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="form_support_time">Time</label>
                                <select id="form_support_time" name="form_support_time" class="custom-select">
                                    <option value="07AM" selected>7 AM</option>
                                    <option value="08AM">8 AM</option>
                                    <option value="09AM">9 AM</option>
                                    <option value="10AM">10 AM</option>
                                    <option value="11AM">11 AM</option>
                                    <option value="12NN">12 NN</option>
                                    <option value="01PM">1 PM</option>
                                </select>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="form_support_contact">Contact Number</label>
                                <input id="form_support_contact" type="text" name="form_support_contact" class="form-control">
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="form_support_comment">Comments</label>
                                <textarea id="form_support_comment" name="form_support_comment" class="form-control" rows="3"></textarea>
                            </div>
                            <div class="col-md-12">
                                <p class="pull-left" style="font-size: 11px;">* Your phone number will not be used for marketing purposes.</p>
                                <div class="pull-right">
                                    <button id="button-contact-callback-sendform" type="button" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END MODAL SUPPORT -->

</div>
@endsection