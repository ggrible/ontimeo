@extends('layouts.app')
@section('content')
<div>
    <comontime-subheader></comontime-subheader>
    <comontime-header></comontime-header>
    <section class="pt-5 pb-5">
       <div class="container">
        <h1>Endorsement</h1>
        <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos, beatae earum, optio dolore libero temporibus aliquam animi, qui saepe odit voluptas culpa voluptatibus consequatur eius ipsum architecto sapiente deleniti quis.
        </p>
       </div>
    </section>
    
    <!-- <comontime-footer></comontime-footer> -->
    @include('includes.footer')
</div>
@endsection
