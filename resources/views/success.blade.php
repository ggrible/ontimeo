@extends('layouts.app')
@section('content')
<div>
    <comontime-subheader></comontime-subheader>
    <comontime-header></comontime-header>
    <div class="container">
        <div class="row">
            <div class="col-md-12 p-5">
                {{-- <h1>
                    Success Page.
                </h1>
                <p>Check your email now. and Login</p> --}}
                <comontime-success></comontime-success>
            </div>
        </div>
    </div>
    
    <!-- <comontime-footer></comontime-footer> -->
    @include('includes.footer')
</div>
@endsection