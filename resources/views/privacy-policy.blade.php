@extends('layouts.app')
@section('content')
<div>
    <comontime-subheader></comontime-subheader>
    <comontime-header></comontime-header>
    
    <!-- <section class="pt-5 pb-5">
       <div class="container">
        <h1>Privacy Policy</h1>
        <p>
        Ontime is strongly committed to the protection of your privacy online because we value the trust you place in us. We take highest protective standards to protect the personal information provided to us by you.
        </p>
        <p>
        We are notified under the Data Protection Act 1998 and intend to comply with all our obligations under that Act. You consent to our processing of your information.
        </p>
        <p>
        Our privacy policy is subject to change at any time without notice. To make sure you are aware of any changes, please review this policy periodically.
        </p>
        <p>
        In order to assist us, you may provide us with information from time to time which we may require or use for the purposes of providing you with any required services, information or requested activity including contacting you and providing notification to you in relation to such services or related activity.
        </p>
        <p>
        By visiting this Website you agree to be bound by the terms and conditions of this Privacy Policy. If you do not agree please do not use or access our Website.
        </p>
        <p>
        By mere use of the Website, you expressly consent to our use and disclosure of your personal information in accordance with this Privacy Policy. This Privacy Policy is incorporated into and subject to the Terms of Use
        </p>
        Ontime recognises the importance of protecting the privacy of all information provided by users of our platform. This statement is meant to affirm our utmost respect for your rights to privacy.
        </p>
        <h1>Information We Collect</h1>
        <p>
        When you use our Website, we store your browsing information. Our primary goal in doing so is to provide you a safe, efficient, smooth and customized experience. This allows us to provide transport services that most likely meet your needs, and to customize our Website to make your experience safer and easier. More importantly, while doing so we collect personal information from you that we consider necessary for achieving this purpose..
        </p>
        <p>
        The registration information you provide may be required to use the service or Site. We will store this information and data and use it to contact you, provide you with further details of the services and otherwise for the normal use, provision of services and improvement of the Site, unless you ask us not to do so.
        </p>
        <p>
        In general, you can browse the Website without telling us who you are or revealing any personal information about yourself. Once you give us your personal information, you are not anonymous to us. Where possible, we indicate which fields are required and which fields are optional.
        </p>
        <p>
        Ontime may use your personal information to send to you services and products that you purchase, supply to you services that you purchase, send you statements and invoices, collect payments from you and send you marketing communications.
        </p>
        <p>
        Where Ontime discloses your personal information to its agents or sub-contractors for these purposes, the agent or sub-contractor in question will be obligated to use that personal information in accordance with the terms of this privacy statement.
        </p>
        <p>
        In addition to the disclosures reasonably necessary for the purposes identified elsewhere above, Ontime may disclose your personal information to the extent that it is required to do so by law, in connection with any legal proceedings or prospective legal proceedings, and in order to establish, exercise or defend its legal rights.
        </p>
        <p>
        We may provide the data collected from you to members of our group companies or other third party including our agents and contractors in connection with the service. We will inform you prior to disclosing your information to any third party. If you can be identified from the information that is disclosed, then we will not disclose such information without prior notification to you and we will obtain your permission to do so. You may inform us at any time not to pass on or share your personal information with any third parties.
        </p>
        <p>
        The information may be disclosed to third parties to enable you to gain access to any restricted part of the Site, enable you to receive information, which you have requested to be sent to you by post or for you to receive specified services.
        </p>
        <p>
        Information that Ontime collects may be stored and processed in and transferred between any of the countries in which Ontime operates to enable the use of the information in accordance with this privacy policy.
        </p>
        <p>
        We may use cookies to store and track information about you. This will assist us in measuring the effectiveness of banner advertising and provide more accurate analysis of site activity. A cookie is a small amount of data that is sent to your browser from a web server and stored on your computer.
        </p>
        <p>
        We are taking reasonable, technical and organizational precautions to prevent the loss, misuse or alteration of your personal information and will endeavour to continue taking all reasonable steps in order to protect your personal information and data. Ontime will store all the personal information you provide on its secure servers. However, we cannot guarantee the security of any personal information or data you disclose online, having regard to the nature of the inter net. We may use encryption technology from time to time to assist in protecting any information or data, which you submit
        </p>
        <p>
        In any event, you accept the inherent security implications of disclosing information over the internet and agree not to hold us responsible for any breach of security unless we have been negligent or wilfully in default of our obligations under the relevant legislation.
        </p>
        <p>
        If you choose to post reviews and tips about buses or post messages or leave feedback, we will collect that information you provide to us. We retain this information as necessary to resolve disputes, provide customer support and troubleshoot problems as permitted by law. If you send us personal correspondence, such as emails or letters, or if other users or third parties send us correspondence about your activities or postings on the Website, we may collect such information into a file specific to you.
        </p>
        We reserve the right to change this Privacy Policy and Data Protection Statement at any time by posting revisions on our Site.
        </p>
        <h1>Acknowledgment</h1>
        <p>
        You acknowledge that by using the Ontime services, some of your personal information will be passed on to any person whom you receive Ontime money from, or send Ontime money to, and will be available to any third party involved in the operation of the service including without limitation, Ontime Agent and partner banks.
        </p>
        <p>
        By using the Website and/ or by providing your information, you consent to the collection and use of the information you disclose on the Website in accordance with this Privacy Policy, including but not limited to your consent for sharing your information as per this privacy policy. If we decide to change our privacy policy, we will post those changes on this page so that you are always aware of what information we collect, how we use it, and under what circumstances we disclose it.
        </p>
        <h1>Data Security Policy</h1>
        <p>
            All proprietary or confidential information, including personal data, is contained or stored on computer and any that is contained and stored on manual files is locked up and secure.
        </p> 
        <p>
        Ontime employees who handle personal information are under an obligation to treat it confidentially and may not disclose it to third parties. Ontime employees are also responsible for the internal security of the information. Employees who violate Ontime's privacy policies are subject to a range of disciplinary actions.
        </p>
        <p>
        We control access to information and personal data, including existing procedures for authorising and authenticating users as well as software controls for restricting access and techniques for protecting data such as encryption.
        </p>
        <p>
        We maintain a business continuity plan as a contingency plan, which identifies our business functions, and assets (includ ing personal data) that would need to be maintained in the event of disaster and set out the procedures for protecting and restoring them if necessary.
        </p>
        <p>
        In respect of detection and investigation of breaches where they occur, we have in place relevant controls, which should alert us to a breach in security. We endeavour to investigate every breach of security.
        </p>
        <p>
        The Company cannot guarantee the security of any personal information or data disclosed to it or collected by it.
        </p>
       </div>
    </section> -->

    <section class="pt-5 pb-5">
        <div class="container">
            <h1>Terms of Service (“Terms”)</h1>
            <p>
                This is a legal agreement between the person or organization (“Customer” or “you”) agreeing to these Terms of Service (“Terms”) and After Hour Line Inc. (“Comontime,” “us,” or “we”). By accepting these Terms, signing an Order, or using the Services, you represent that you are of legal age and have the authority to bind the Customer to the Order, these Terms, and the applicable Service Descriptions (collectively the “Agreement”).
                <br><br>Your access to and use of the Service is conditioned upon your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who wish to access or use the Service.
                <br><br>By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you do not have permission to access the Service.
            </p>
            <p>&nbsp;</p>

            <h1>Account Setup</h1>
            <p>
                To use the Platform, you need to create After Hour Line account. When you create an account with us, you guarantee that you are above the age of 18, and that the information you provide us is accurate, complete, and current at all times. Inaccurate, incomplete, or obsolete information may result in the immediate termination of your account on the Service.
                <br><br>We reserves the right to terminate any of your Accounts set up by you and that have been inactive over 3 months.
                <br><br>When you create an account, we collect registration-related information such as name, address, e-mail and phone number. For so long as you use the account, you agree to provide true, accurate, current, and complete information which can be accomplished by logging into your account and making relevant changes directly. You are responsible for complying with these Terms when you access this Website, whether directly or through any account that you may setup through or on this Website. 
                <br><br>Because it is your account, it is your job to obtain and maintain all equipment and services needed for access to and use of this Website as well as paying related charges. It is also your responsibility to maintain the confidentiality of your password(s), including any password of a third-party site that we may allow you to use to access this Website. Should you believe your password or security for this website has been breached in any way, you must immediately notify us.
                <br><br>You may not use as a username the name of another person or entity or that is not lawfully available for use, a name or trademark that is subject to any rights of another person or entity other than you, without appropriate authorization. You may not use as a username any name that is offensive, vulgar or obscene.
                <br><br>After Hour Line or our billing processors collect your billing information after 30 days free trials. All of this registration information is protected in accordance with our Privacy Policy.
            </p>
            <p>&nbsp;</p>

            <h1>Subscriptions</h1>
            <p>
                Our Service are billed on a subscription basis (“Subscription(s)”). You will be billed in advance on a recurring and periodic basis (“Billing Cycle”). Billing cycles are set either on a monthly, quarterly or annual basis, depending on the type of subscription plan you select when purchasing a Subscription.
                <br><br>At the end of each Billing Cycle, your Subscription will automatically renew under the exact same conditions unless you cancel it or After hour line cancels it. You may cancel your Subscription renewal by contacting After Hour Line INC customer support team.
                <br><br>A valid payment method, including credit card, is required to process the payment for your Subscription. You shall provide After Hour Line with accurate and complete billing information including full name, address, state, zip code, telephone number, and a valid payment method information. By submitting such payment information, you automatically authorize After Hour Line INC to charge all Subscription fees incurred through your account to any such payment instruments.
                <br><br>Should automatic billing fail to occur for any reason, After Hour Line Inc  will issue an electronic invoice indicating that you must proceed manually, within a certain deadline date, with the full payment corresponding to the billing period as indicated on the invoice.
            </p>
            <p>&nbsp;</p>

            <h1>Free Trial</h1>
            <p>
                Your right to access and use any free Services is not guaranteed for any period of time and we reserve the right, in our sole and absolute discretion, to limit or terminate your use of any free or basic versions of any Services by any individual or entity. If you are using the Services on a trial or promotional basis (“Trial Period”), your Trial Period and access to the Services will terminate (i) at the end of the Trial Period stated in your Order, or (ii) if no date is specified, 30 days after your initial access to the Services, (iii) or upon your conversion to a subscription. 
                <br><br>Following expiration of the Trial Period, the Services may automatically continue unless you provide notice of cancellation to us, and you are responsible for payment of the applicable Fees set forth in the Order. During the Trial Period, to the extent permitted by law, we provide the Services “AS IS” and without warranty or indemnity, and all other terms otherwise apply. We may modify or discontinue any trials or promotions at any time without notice.
            </p>
            <p>&nbsp;</p>

            <h1>Beta Services</h1>
            <p>
                We may offer you access to beta services that are being provided prior to general release, but we do not make any guarantees that these services will be made generally available (“Beta Services”). You understand and agree that the Beta Services may contain bugs, errors and other defects, and use of the Beta Services is at your sole risk. We have no obligation to provide technical support and we may discontinue provision of Beta Services at any time in our sole discretion and without prior notice to you. 
                <br><br>These Beta Services are offered “AS-IS”, and to the extent permitted by applicable law, we disclaim any liability, warranties, indemnities, and conditions, whether express, implied, statutory or otherwise. If you are using Beta Services, you agree to receive related correspondence and updates from us, and acknowledge that opting out may result in cancellation of your access to the Beta Services. 
                <br><br>If you provide feedback(“Feedback”) about the Beta Service, you agree that we own any Feedback that you share with us. For the Beta Services only, these Terms supersede any conflicting terms and conditions in the Agreement, but only to the extent necessary to resolve conflict.
            </p>
            <p>&nbsp;</p>

            <h1>Fee Changes</h1>
            <p>
                After Hour Line Inc  may at any time modify the Subscription fees for the Subscriptions. Any Subscription fee change will become effective at the end of the then-current Billing Cycle.
                <br><br>After Hour Line Inc will provide you with a reasonable prior notice of any change in Subscription fees to give you an opportunity to terminate your Subscription before such change becomes effective.
                <br><br>Your continued use of the Service after the Subscription fee change comes into effect constitutes your agreement to pay the modified Subscription fee amount.
            </p>
            <p>&nbsp;</p>

            <h1>Downgrade</h1>
            <p>After Hour Line Inc  will automatically downgrade your package if you sign up as a partner when you failed to meet your expectation. This will result in lost of client base and  shared commission on those lost client.</p>
            <p>&nbsp;</p>

            <h1>Refunds</h1>
            <p>Certain refund requests for Subscriptions may be considered by After Hour Line Inc on a case-by-case basis and granted in sole discretion of After Hour Line Inc .</p>
            <p>&nbsp;</p>

            <h1>Content</h1>
            <p>
                Our Service allows you to post, link, store, share and otherwise make available certain information, text, graphics, videos, or other material (“Content”). You are responsible for the Content that you post on or through the Service, including its legality, reliability, and appropriateness.
                <br><br>By posting Content on or through the Service, You represent and warrant that: (i) the Content is yours (you own it) and/or you have the right to use it and the right to grant us the rights and license as provided in these Terms, and (ii) that the posting of your Content on or through the Service does not violate the privacy rights, publicity rights, copyrights, contract rights or any other rights of any person or entity. We reserve the right to terminate the account of anyone found to be infringing on a copyright.
                <br><br>You retain any and all of your rights to any Content you submit, post or display on or through the Service and you are responsible for protecting those rights. We take no responsibility and assume no liability for Content you or any third party posts on or through the Service. However, by posting Content using the Service you grant us the right and license to use, modify, publicly perform, publicly display, reproduce, and distribute such Content on and through the Service.
                <br><br>After Hour Line Inc  has the right but not the obligation to monitor and edit all Content provided by users.
                <br><br>In addition, Content found on or through this Service are the property of After Hour Line Inc  or used with permission. You may not distribute, modify, transmit, reuse, download, repost, copy, or use said Content, whether in whole or in part, for commercial purposes or for personal gain, without express advance written permission from us.
            </p>
            <p>&nbsp;</p>

            <h1>Intellectual Property </h1>
            <p>
                The Service and its original content (excluding Content provided by users), features and functionality are and will remain the exclusive property of After Hour Line Inc  and its licensors. The Service is protected by copyright, trademark, and other laws of both the United States and foreign countries. Our trademarks and trade dress may not be used in connection with any product or service without the prior written consent of After Hour Line Inc .
            </p>
            <p>&nbsp;</p>

            <h1>Links To Other Web Sites</h1>
            <p>
                Our Service may contain links to third party web sites or services that are not owned or controlled by After Hour Line Inc .
                <br><br>After Hour Line Inc  has no control over, and assumes no responsibility for the content, privacy policies, or practices of any third party web sites or services. We do not warrant the offerings of any of these entities/individuals or their websites.
                <br><br>You acknowledge and agree that After Hour Line Inc  shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such third party web sites or services.
                <br><br>We strongly advise you to read the terms and conditions and privacy policies of any third party web sites or services that you visit.
            </p>
            <p>&nbsp;</p>

            <h1>Termination</h1>
            <p>
                We may terminate or suspend your account and bar access to the Service immediately, without prior notice or liability, under our sole discretion, for any reason whatsoever and without limitation, including but not limited to 
                <br><br>Voice usage that grossly exceeds the average typical consumer usage.
                <br><br>Voice calls that are deemed to have abnormal durations. For example: many short calls to different phone numbers or few calls of long duration.
                <br><br>Voice services that are used for data transmissions, transmission of broadcasts, monitoring services, transmission of recorded material, or other connections which don’t consist of uninterrupted live dialog between two individuals.
                <br><br>Where reasonable, After Hour Line Inc will provide you with notice of improper usage before suspension or termination of your service and, if appropriate, After Hour Line Inc may offer you an alternative plan. All calls will be disconnected  and require a re-dial after a 2 hour duration.
                <br><br>In addition, Customer agrees not to (i) modify, prepare derivative works of, or reverse engineer, our Services; (ii) knowingly or negligently use our Services in a way that abuses or disrupts our networks, user accounts, or the Services; (iii) transmit through the Services any harassing, fraudulent or unlawful material; (iv) market, or resell the Services to any third party; (v) use the Services in violation of our policies, applicable laws, or regulations; (vi) use the Services to send unauthorized advertising, or spam; (vii) harvest, collect, or gather user data without their consent; or (viii) transmit through the Services any material that may infringe the intellectual property or other rights of third parties.
                <br><br>Changes to Services; Additional Services. We reserve the right to enhance or modify features of our Services but will not materially reduce the core functionality or discontinue any Services unless we provide you with prior written notice. We may offer additional functionality to our standard Services or premium feature improvements for an additional cost. Any additional Services you Order will be subject to these Terms.
                <br><br>You acknowledge that we or our licensors retain all proprietary right, title and interest in the Services, our name, logo or other marks and any related intellectual property rights, including, without limitation, all modifications, enhancements, derivative works, and upgrades thereto.
                <br><br>If you wish to terminate your account, you may simply discontinue using the Service.
                <br><br>All provisions of the Terms which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability.
            </p>
            <p>&nbsp;</p>

            <h1>Indemnification</h1>
            <p>
                You agree to defend, indemnify and hold harmless After Hour Line Inc and its licensee and licensors, and their employees, contractors, agents, officers and directors, from and against any and all claims, damages, obligations, losses, liabilities, costs or debt, and expenses (including but not limited to attorney’s fees), resulting from or arising out of a) your use and access of the Service, by you or any person using your account and password; b) a breach of these Terms, or c) Content posted on the Service.
            </p>
            <p>&nbsp;</p>

            <h1>Limitation Of Liability</h1>
            <p>
                In no event shall After Hour Line Inc , nor its directors, employees, partners, agents, suppliers, or affiliates, be liable for any indirect, incidental, special, consequential or punitive damages, including without limitation, loss of profits, data, use, goodwill, or other intangible losses, resulting from (i) your access to or use of or inability to access or use the Service; (ii) any conduct or content of any third party on the Service; (iii) any content obtained from the Service; and (iv) unauthorized access, use or alteration of your transmissions or content, whether based on warranty, contract, tort (including negligence) or any other legal theory, whether or not we have been informed of the possibility of such damage, and even if a remedy set forth herein is found to have failed of its essential purpose.
            </p>
            <p>&nbsp;</p>

            <h1>Disclaimer</h1>
            <p>
                Your use of the Service is at your sole risk. The Service is provided on an “AS IS” and “AS AVAILABLE” basis. The Service is provided without warranties of any kind, whether express or implied, including, but not limited to, implied warranties of merchantability, fitness for a particular purpose, non-infringement or course of performance.
                <br><br>After Hour Line Inc  its subsidiaries, affiliates, and its licensors do not warrant that a) the Service will function uninterrupted, secure or available at any particular time or location; b) any errors or defects will be corrected; c) the Service is free of viruses or other harmful components; or d) the results of using the Service will meet your requirements.
            </p>
            <p>&nbsp;</p>

            <h1>Exclusions</h1>
            <p>
                Some jurisdictions do not allow the exclusion of certain warranties or the exclusion or limitation of liability for consequential or incidental damages, so the limitations above may not apply to you.
            </p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>

            <h1>ADDITIONAL TERMS</h1>
            <p>&nbsp;</p>

            <h1>Security Emergencies</h1>
            <p>
                If we reasonably determine that the security of our Services or infrastructure may be compromised due to hacking attempts, denial of service attacks, or other malicious activities, we may temporarily suspend the Services and we will take action to promptly resolve any security issues. We will notify you of any suspension or other action taken for security reasons by publishing on our website and/or emailing you.
            </p>
            <p>&nbsp;</p>

            <h1>High-Risk Use</h1>
            <p>
                You understand that the Services (i) are not designed or intended for use during high-risk activities, and (ii) do not allow and should not be used for calls to Call centre services numbers
                <br><br><b>Recording.</b> Certain Services provide functionality that allows you to record audio and data shared during sessions. You are solely responsible for complying with all applicable laws in the relevant jurisdictions while using recording functionality. We disclaim all liability for your recording of audio or shared data, and you agree to hold us harmless from damages or liabilities related to the recording of any audio or data.
                <br><br><b>General Terms.</b>  If any term of this Agreement is not enforceable, this will not affect any other terms. Both parties are independent contractors and nothing in this Agreement creates a partnership, agency, fiduciary or employment relationship between the parties. No person or entity not a party to the Agreement will be a third party beneficiary. Our authorized distributors do not have the right to modify the Agreement or to make commitments binding on us. Failure to enforce any right under the Agreement will not waive that right. Unless otherwise specified, remedies are cumulative. The Agreement may be agreed to online, or executed by electronic signature and in one or more counterparts. No party will be responsible for any delay or failure to perform under the Agreement due to force major events (e.g. natural disasters; terrorist activities, activities of third party service providers, labor disputes; and acts of government) and acts beyond a party’s reasonable control, but only for so long as those conditions persist.
            </p>
            <p>&nbsp;</p>

            <h1>Changes</h1>
            <p>
                We reserve the right, at our sole discretion, to modify or replace these Terms at any time. If a revision is material we will provide at least 30 days notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion.
                <br><br>By continuing to access or use our Service after any revisions become effective, you agree to be bound by the revised terms. If you do not agree to the new terms, you are no longer authorized to use the Service.
            </p>
            <p>&nbsp;</p>

            <h1>Governing Law</h1>
            <p>
                If you have any questions about these Terms, please contact us.These Terms shall be governed and construed in accordance with the laws of Texas, United States, without regard to its conflict of law provisions.
                <br><br>Our failure to enforce any right or provision of these Terms will not be considered a waiver of those rights. If any provision of these Terms is held to be invalid or unenforceable by a court, the remaining provisions of these Terms will remain in effect. These Terms constitute the entire agreement between us regarding our Service, and supersede and replace any prior agreements we might have had between us regarding the Service.
            </p>
            <p>&nbsp;</p>

            <h1>Contact Us</h1>
            <p>If you have any questions about these Terms, please contact us.</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
        </div>
    </section>
    
    <!-- <comontime-footer></comontime-footer> -->
    @include('includes.footer')
</div>
@endsection