@extends('layouts.app')
@section('content')
<div>
    <comontime-subheader></comontime-subheader>
    <comontime-header></comontime-header>
    <main role="main">
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron mb-0 homepage-banner">
        <div class="container">
        <div class="mb-2 mt-5 captions">
            <h3>Schedule</h3>
            <p>Your Appointment Like a Genius</p>
            <p><a class="btn btn-primary btn-lg" href="#" role="button">Start your Free Trial »</a></p>

            <div class="link-text pt-5">
                <p><i class="fa fa-question-circle"></i> <a href="/faqpage">Frequently Asked Questions »</a></p>
                <div class="row">
                    <div class="col-sm-6">
                        <span class="mr-3"><i class="fa fa-envelope"></i> <a href="#">Message Us</a></span>
                        <span><i class="fa fa-phone"></i> <a href="#">Call Sales 1-800-31</a></span>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    <section>
        <div class="hero-bar pt-3 pb-3">
            <p class="text-center">Watch your schedule fill up FAST with appointments, while you focus on providing great service</p>
        </div>
    </section>
    <section class="pt-5 pb-5 about-section">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <h5 class="text-center">THE FUTURE OF APPOINTMENT SCHEDULING IS HERE!</h5>
                    <p>Run your business like a Genius by using a combination of text appointment reminders and email reminders.</p>
                    <h5>Comontime-Guarantee</h5>
                    <p>No customer like to wait. Imagine your waiting room without customer registration, watch as they self book, while you focus on providing great service.</p>
                    
                    <h5>Wow Your Customers</h5>
                    <p>Sometimes we forget things. Imagine your customer getting an automated appointment reminder for them to confirm, or cancel their appointment with just a Yes or No reply.</p>

                    <h5>Appointment Reallocate slots:</h5>
                    <p>If an appointment was cancelled prior to the time - imagine a system that automatically reallocate slot to the next person on your waiting list.</p>
                    </div>
                    <div class="col-md-6 col-lg-6">
                        <img src="./img/comontime2.jpg" alt="" class="img-fluid">
                    </div>
            </div>
        </div>
    </section>
    <section class="offer pt-4 pb-3">
    <div class="container">
        <h1 class="lt_title_big text-center">What We <span>Offer</span></h1>
        <div class="cl_title_line text-center"><div></div></div>
        <p class="lt_title_bottomtext text-center">Our company is Comontime and our job is simply to make your business run on time.</p>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-lg-4 col-md-12 col-sm-12 last">
                <div class="ontimeo-box">
                    <div class="ontimeo-content" style="height: 189px;">
                        <div class="text_holder">
                            <div class="icon"><span class="fa fa-2x fa-briefcase" aria-hidden="true"></span>
                                <div class="arrow"></div>
                            </div>
                            <div class="text">
                                <h4 class="lessmar">Work with existing platform</h4>
                                <p>
                                    Comontime works with your existing software. It can Integrate with Google Calendar, Outlook Calendar, iCal + More
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end item-->
            <div class="col-lg-4 col-md-12 col-sm-12 last">
                <div class="ontimeo-box">
                    <div class="ontimeo-content active" style="height: 189px;">
                        <div class="text_holder ">
                            <div class="icon"><span class="fa fa-2x fa-calendar" aria-hidden="true"></span>
                                <div class="arrow"></div>
                            </div>
                            <div class="text">
                                <h4 class="lessmar">Scheduling Like A Genius</h4>
                                <p>
                                    Even when you’re out-of-office or can't answer the phone -  Let your customer view your real-time availability and schedule their own appointment like a Genius.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end item-->
            <div class="col-lg-4 col-md-12 col-sm-12 last">
                <div class="ontimeo-box">
                    <div class="ontimeo-content" style="height: 189px;">
                        <div class="text_holder">
                            <div class="icon"><span class="fa fa-2x fa-ban" aria-hidden="true"></span>
                                <div class="arrow"></div>
                            </div>
                            <div class="text">
                                <h4 class="lessmar">No-shows Prevention</h4>
                                <p>
                                    Run your business with a piece of mind by minimize back-and-forth communication using our automated confirmations and reminders system to make sure clients show up.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end item-->
        </div>
        {{--  <div class="margin_top1"></div>  --}}
        
        <div class="row">
            <div class="col-lg-4 col-md-12 col-sm-12 last">
                <div class="ontimeo-box">
                    <div class="ontimeo-content" style="height: 189px;">
                        <div class="text_holder">
                            <div class="icon"><span class="fa fa-2x fa-users" aria-hidden="true"></span>
                                <div class="arrow"></div>
                            </div>
                            <div class="text">
                                <h4 class="lessmar">Customer Appreciation</h4>
                                <p>
                                    Treat each customer like a Rock star with thank you notes and birthday wishes. Easily notify them of last-minute cancellations proving that their time matters.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end item-->
            <div class="col-lg-4 col-md-12 col-sm-12 last">
                <div class="ontimeo-box">
                    <div class="ontimeo-content" style="height: 189px;">
                        <div class="text_holder">
                            <div class="icon"><span class="fa fa-2x fa-credit-card" aria-hidden="true"></span>
                                <div class="arrow"></div>
                            </div>
                            <div class="text">
                                <h4 class="lessmar">Simplify payment collection</h4>
                                <p>
                                Enhance your practise by accepting payments for your appointments and services online. Track your income with a centralized dashboard
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end item-->
            <div class="col-lg-4 col-md-12 col-sm-12 last">
                <div class="ontimeo-box">
                    <div class="ontimeo-content" style="height: 189px;">
                        <div class="text_holder">
                            <div class="icon">
                                <img height="30" src="/img/logo-hipaa.png">
                                <!-- <span class="fa fa-2x fa-check" aria-hidden="true" ></span> -->
                                <!-- <div class="arrow"></div> -->
                            </div>
                            <div class="text">
                                <h4 class="lessmar">HIPAA Compliant / Encrypted</h4>
                                <p>
                                    Approved by HIPAA and Trusted by thousands of small businesses.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end item--> 
        </div>
    </div>
    </section>
    <div class="container">
    <section id="tabs-section">
       <div class="tabs-section">
        <h2 class="text-center mt-5 mb-5">Be easy to do business with.</h2>
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="first-tab" data-toggle="tab" href="#first-panel" role="tab" aria-controls="first-panel" aria-selected="true">
                  <table class="tab-table">
                      <tr>
                          <td class="baseline tab-icon">
                              <i class="fa fa-calendar"></i>
                          </td>
                          <td>
                              <span>
                                  Be Bookable <br>
                                  <span class="tab-subtitle">From Anywhere</span>
                              </span>
                          </td>
                      </tr>
                  </table>
              </a>
            </li>
            <li class="nav-item">
              <a 
              style="
              border-left: none;
              border-right: none;
              "
              class="nav-link" id="second-tab" data-toggle="tab" href="#second-panel" role="tab" aria-controls="second-panel" aria-selected="false">
                <table class="tab-table">
                    <tr>
                        <td class="baseline tab-icon">
                            <i class="fa fa-comments"></i>
                        </td>
                        <td>
                            <span>
                                Be Top of Mind <br>
                                <span class="tab-subtitle">With the text reminders</span>
                            </span>
                        </td>
                    </tr>
                </table>
            </a>
            </li>
            <li class="nav-item">
              <a style="border-right: none;" class="nav-link" id="third-tab" data-toggle="tab" href="#third-panel" role="tab" aria-controls="third-panel" aria-selected="false">
              <table class="tab-table">
                    <tr>
                        <td class="baseline tab-icon">
                            <i class="fa fa-credit-card"></i>
                        </td>
                        <td>
                            <span>
                                Be easy to Pay <br>
                                <span class="tab-subtitle">On the Go, 24/7</span>
                            </span>
                        </td>
                    </tr>
                </table>
            </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="fourth-tab" data-toggle="tab" href="#fourth-panel" role="tab" aria-controls="fourth-panel" aria-selected="false">
                <table class="tab-table">
                    <tr>
                        <td class="baseline tab-icon">
                            <i class="fa fa-share-alt"></i>
                        </td>
                        <td>
                            <span>
                                Be found <br>
                                <span class="tab-subtitle">on 70+ Sites</span>
                            </span>
                        </td>
                    </tr>
                </table>
              </a>
            </li>
          </ul>
          <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="first-panel" role="tabpanel" aria-labelledby="first-tab">
                <div class="container-fluid mt-5">
                    <img style="width: 100%;" src="/img/home-page-tab/first-tab.gif" alt="Image">
                </div>
            </div>
            <div class="tab-pane fade" id="second-panel" role="tabpanel" aria-labelledby="second-tab">
                <div class="container-fluid mt-5">
                    <img style="width: 100%;" src="/img/home-page-tab/SMS_Reminder.gif" alt="Image">
                </div>
            </div>
            <div class="tab-pane fade" id="third-panel" role="tabpanel" aria-labelledby="second-tab">
                <div class="container-fluid mt-5">
                    <img style="width: 100%;" src="/img/home-page-tab/SMS_booking.gif" alt="Image">
                </div>
            </div>
            <div class="tab-pane fade" id="fourth-panel" role="tabpanel" aria-labelledby="fourth-tab">
                <div class="container-fluid mt-5">
                    <img style="width: 100%;" src="/img/home-page-tab/Social_Media.gif" alt="Image">
                </div>
            </div>
          </div>
       </div>
    </section>
    </div>
    
    <home-page-slider></home-page-slider>

    <home-counter></home-counter>

    <section id="home-page-calculate">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1>
                        Calculate the return on investment
                    </h1>
                    <p>See how much we can save you with our ROI Calculator</p>
                    <button id="button-roi-showpopup" class="btn btn-lg">Calculate your Savings</button>
                </div>
                <div class="col-md-6">
                    <div>
                        <img style="width:100%;" src="/img/img-calculate.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    </main>
    
    <!-- <comontime-footer></comontime-footer> -->
    @include('includes.footer')

    <!-- START MODAL ROI -->
    <div id="modal-roi" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
           <div class="modal-content">
              <div class="modal-header">
                 <h5>ROI Calculator</h5>
                 <button id="button-roi-closepopup" type="button" class="close"><span aria-hidden="true">×</span></button>
              </div>
              <div class="modal-body">
                 <form id="form_roi" >
                    <div class="vue-form-wizard md">
                       <div class="wizard-navigation">
                          <div class="wizard-progress-with-circle">
                             <div class="wizard-progress-bar" style="width: 12.5%;"></div>
                          </div>
                          <ul role="tablist" class="wizard-nav wizard-nav-pills">
                             <li step-size="md" class="tab-step-1 active">
                                <a href="javascript:void(0)">
                                   <div role="tab" tabindex="0" class="wizard-icon-circle md">
                                      <div class="wizard-icon-container"><i class="wizard-icon">1</i></div>
                                   </div> 
                                   <span class="stepTitle">Personal details</span>
                                </a>
                             </li>
                             <li step-size="md" class="tab-step-2">
                                <a href="javascript:void(0)">
                                   <div role="tab" tabindex="1" class="wizard-icon-circle md">
                                       <div class="wizard-icon-container"><i class="wizard-icon">2</i></div>
                                   </div>
                                   <span class="stepTitle">Select A Product</span>
                                </a>
                             </li>
                             <li step-size="md" class="tab-step-3">
                                <a href="javascript:void(0)">
                                   <div role="tab" tabindex="2" class="wizard-icon-circle md">
                                        <div class="wizard-icon-container"><i class="wizard-icon">3</i></div>
                                   </div> 
                                   <span class="stepTitle">Company Details</span>
                                </a>
                             </li>
                             <li step-size="md" class="tab-step-4">
                                <a href="javascript:void(0)">
                                   <div role="tab" tabindex="3" class="wizard-icon-circle md">
                                        <div class="wizard-icon-container"><i class="wizard-icon">4</i></div>
                                   </div>
                                   <span class="stepTitle">Heres what you save</span>
                                </a>
                             </li>
                          </ul>

                          <div class="wizard-tab-content">
                             <div role="tabpanel" id="Personaldetails0" class="wizard-tab-container">
                               <div class="form-group">
                                  <label for="email">Email</label>
                                  <input id="form_email" type="email" name="form_email" class="form-control">
                               </div>
                               <div class="form-group">
                                  <label for="name">Company</label>
                                  <input id="form_company" type="text" name="form_company" class="form-control">
                               </div>
                               <div class="form-group">
                                  <label for="contact">Telephone</label>
                                  <input id="form_contact" type="text" name="form_contact" class="form-control">
                               </div>
                               <div class="form-group">
                                    <div class="clearfix">&nbsp;</div>
                                    <p class="pull-left">Need any help? Please contact support <a href="http://test.ontimeo.com/contact-us">here</a>.</p>
                                    <div class="pull-right">
                                        <button id="button-roi-next-personal" type="button" class="btn btn-danger">Next</button>
                                    </div>
                               </div>
                             </div>

                             <div role="tabpanel" id="SelectAProduct2" class="wizard-tab-container" style="display: none;">
                                <input id="package_selected" name="form_package" type="hidden" value="0" data-package_id="0">
                                <div class="row package-list" style="padding-top: 20px; margin-bottom: 15px;">-- PACKAGE LIST HERE --</div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p class="pull-left">Need any help? Please contact support <a href="http://test.ontimeo.com/contact-us">here</a>.</p>
                                        <div class="pull-right">
                                            <button id="button-roi-prev-product" type="button" class="btn btn-danger">Prev</button>
                                            <button id="button-roi-next-product" type="button" class="btn btn-danger">Next</button>
                                        </div>
                                    </div>
                                </div>
                             </div>

                             <div role="tabpanel" id="CompanyDetails4" class="wizard-tab-container" style="display: none;">
                               <div class="form-group">
                                  <label for="email">Expected Employee</label> 
                                  <input id="form_expected" type="email" name="form_expected" class="form-control">
                               </div>
                               <div class="form-group">
                                  <label for="name">Annual Salary</label>
                                  <input id="form_salary" type="text" name="form_salary" class="form-control">
                               </div>
                               <div class="form-group">
                                    <div class="clearfix">&nbsp;</div>
                                    <p class="pull-left">Need any help? Please contact support <a href="http://test.ontimeo.com/contact-us">here</a>.</p>
                                    <div class="pull-right">
                                        <button id="button-roi-prev-company" type="button" class="btn btn-danger">Prev</button>
                                        <button id="button-roi-next-company" type="button" class="btn btn-danger">Next</button>
                                    </div>
                               </div>
                             </div>

                             <div role="tabpanel" id="Hereswhatyousave6" class="wizard-tab-container" style="display: none;">
                               <div class="row" style="text-align: center;"><h5 style="margin: 0 auto;">We can save you</h5></div>
                               <div class="row" style="margin: 20px 0 15px 0;">
                                    <div class="col-lg-6 col-md-6 col-sm-6" style="max-width: 49%; margin-right: 5px; border-radius: 10px; border: 1px solid #D0D0D0; text-align: center; padding-top: 20px; padding-bottom: 20px;">
                                        <h4>7h</h4>
                                        per month
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6" style="max-width: 49%; margin-left: 5px; border-radius: 10px; border: 1px solid #D0D0D0; text-align: center; padding-top: 20px; padding-bottom: 20px;">
                                        <h4>- $500</h4>
                                        per month
                                    </div>
                                </div>
                                <div class="row" style="text-align: center;"><h5 style="margin: 0 auto; width: 90%; background-color: #e9503b; border-radius: 10px; padding: 10px; color: #FFFFFF;">That's amazing send me the results!</h5></div>
                               <div class="form-group">
                                    <div class="clearfix">&nbsp;</div>
                                    <p class="pull-left">Need any help? Please contact support <a href="http://test.ontimeo.com/contact-us">here</a>.</p>
                                    <div class="pull-right">
                                        <button id="button-roi-prev-yousave" type="button" class="btn btn-danger">Prev</button>
                                        <button id="button-roi-next-yousave" type="button" class="btn btn-danger">Close</button>
                                        <button id="button-roi-sendemail-yousave" type="button" class="btn btn-success">Send to E-mail</button>
                                    </div>
                               </div>
                             </div>
                          </div>
                       </div>
                    </div>
                 </form>
              </div>
           </div>
        </div>
    </div>
    <!-- END MODAL ROI -->
</div>
@endsection
