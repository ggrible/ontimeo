module.exports = {
    methods: {
      moneyFormat (value) {
        if (value) {
          var parts = value.toFixed(2).toString().split('.')
          return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',') + (parts[1] ? '.' + parts[1] : '')
        }
      },
      toWeekDay (day) {
        const weekDays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
        return weekDays[day]
      },
      toQueryFilter (array) {
        const filters = array.filter(value => value).join('&')
        return filters ? '?' + filters : ''
      }
    }
  }
  