
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import 'sweetalert2/dist/sweetalert2.all.min.js'
import mixin from './mixins/helpers.js';
import VeeValidate from 'vee-validate';
window.Vue = require('vue');
import VueRouter from 'vue-router'
import VueCookie from 'vue-cookie'
import VueAnimateNumber from 'vue-animate-number'
import Vue2Filters from 'vue2-filters'
import Paginate from 'vuejs-paginate'
Vue.component('paginate', Paginate)
Vue.use(Vue2Filters)
Vue.use(VueCookie)
Vue.use(VeeValidate)
Vue.use(VueAnimateNumber)
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.mixin(mixin);
Vue.component('comontime-subheader', require('./components/SubHeader.vue'));
Vue.component('comontime-header', require('./components/Header.vue'));
Vue.component('comontime-footer', require('./components/Footer.vue'));
Vue.component('comontime-faq', require('./components/Faq.vue'));
Vue.component('comontime-product-tour', require('./components/ProductTour.vue'));
Vue.component('comontime-success', require('./components/Success.vue'));
Vue.component('package', require('./components/packages/Package.vue'));
Vue.component('package-term', require('./components/packages/Term.vue'));
Vue.component('checkout', require('./components/packages/Checkout.vue'));
Vue.component('login-form', require('./components/LoginModal.vue'));
Vue.component('custom-number', require('./components/CustomNumber.vue'));
Vue.component('home-page-slider', require('./components/HomePageSlider.vue'));
Vue.component('home-counter', require('./components/HomePageCounter.vue'));

// client
Vue.component('quick-install-index', require('./components/client/quick-install/Index.vue'));
Vue.component('schedule-index', require('./components/client/schedules/Index.vue'));
Vue.component('profile-index', require('./components/client/profile/Index.vue'));
Vue.component('setup-password', require('./components/SetupPassword.vue'));
Vue.component('email-sync-index', require('./components/client/email-sync/Index.vue'));
Vue.component('account-settings-index', require('./components/client/account-settings/Index.vue'));
Vue.component('select-appoinment-modal', require('./components/SelectAppointmentModal.vue'));
Vue.component('appointments-index', require('./components/client/profile/Appointments.vue'));
Vue.component('employees-index', require('./components/client/employees/Index.vue'));
Vue.component('client-transactions-index', require('./components/client/transactions/Index.vue'));
Vue.component('leads-index', require('./components/leads/Index.vue'));

const app = new Vue({
	el: '#app',
	data() {
		return {
			showModal: false,
			uploadingProfilePhoto: false,
			uploadingCoverPhoto: false,
			user: '',
			showModalSelectAppointment: false,
			selectedAppointment: '',
			currentUser: '',
			baseUrl: document.head.querySelector('meta[name="base-url"]').content,
			showPublicEmployees: false
		}
	},
  computed : {
		route_location () {
			let route = window.location.href.split('/')
			return route[3].split('-').join(' ')
		},
		userAddOns () {
			return this.currentUser && this.currentUser.subscription ? 
			this.currentUser.subscription.add_ons.data.map(value => {
				return value.name
			})
			 : []
		}
	},
	created () {
		// dont call api when user data already loaded
		this.currentUser = JSON.parse(this.$cookie.get('current_user'))
	},
	methods: {
		chooseProfilePhoto () {
		  $('#upload-profile-photo').click()
		},
		async onChangeProfilePhoto (e) {
		  const photo = e.target.files[0]
		  try {
			  this.uploadingProfilePhoto = true
			  const formData = new FormData
			  formData.set('photo', photo)
			  const response = await axios.post('/web-api/account/photo', formData)
			  this.uploadingProfilePhoto = false
			  window.location.reload()
		  } catch (error) {
			  // fails
		      this.uploadingProfilePhoto = false
		  }
		},
		async onChangeCoverPhoto (e) {
		  const photo = e.target.files[0]
		  try {
			  this.uploadingCoverPhoto = true
			  const formData = new FormData
			  formData.set('photo', photo)
			  const response = await axios.post('/web-api/account/cover-photo', formData)
			  this.uploadingCoverPhoto = false
			  window.location.reload()
		  } catch (error) {
			  // fails
		      this.uploadingProfilePhoto = false
		  }
		}
	}
});
