import VueRouter from 'vue-router';

const routes = [
    {
        path: '/admin/packages',
        component: require('./components/Packages'),
        name: 'packages',
        meta: {
            root: 'packages'
        }
    },
    {
        path: '/admin/terms',
        component: require('./components/Terms'),
        name: 'terms',
        meta: {
            root: 'packages'
        }
    },
    {
        path: '/admin/add-ons',
        component: require('./components/AddOns'),
        name: 'addons',
        meta: {
            root: 'packages'
        }
    },
    {
        path: '/admin/case-study',
        component: require('./components/CaseStudy'),
        name: 'case-study',
        meta: {
            root: 'case-study'
        }
    },
    {
        path: '/admin/case-study/categories',
        component: require('./components/CaseStudyCategory'),
        name: 'case-study-category',
        meta: {
            root: 'case-study'
        }
    },
    {
        path: '/admin/transactions',
        component: require('./components/Transactions'),
        name: 'admin-transactions',
        meta: {
            root: 'admin-transactions'
        }
    },
    {
        path: '/admin/settings',
        component: require('./components/Settings'),
        name: 'admin-settings',
        meta: {
            root: 'admin-settings'
        }
    },
    {
        path: '/admin/stripesettings',
        component: require('./components/StripeSettings'),
        name: 'stripe-config',
        meta: {
            root: 'stripe-config'
        }
    },
    {
        path: '/admin/coupons',
        component: require('./components/Vouchers'),
        name: 'admin-coupons',
        meta: {
            root: 'admin-coupons'
        }
    },
    {
        path: '/admin/faqs',
        component: require('./components/Faq'),
        name: 'admin-faqs',
        meta: {
            root: 'admin-faqs'
        }
    },
    {
        path: '/admin/leads',
        component: require('./components/Leads'),
        name: 'admin-leads',
        meta: {
            root: 'admin-leads'
        }
    },
    {
        path: '/admin/withdrawal',
        component: require('./components/Withdrawal'),
        name: 'admin-withdrawal',
        meta: {
            root: 'admin-withdrawal'
        }
    },
];

export default new VueRouter({
    mode: 'history',
    routes,
    scrollBehavior(to, from, savedPosition) {
        return {x: 0, y: 0};
    }
});
