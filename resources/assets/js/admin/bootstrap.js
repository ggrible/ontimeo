
window._ = require('lodash');
window.Popper = require('popper.js').default;

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.$ = window.jQuery = require('jquery');

    // require('./../../../../node_modules/adminbsb-materialdesign/plugins/bootstrap/js/bootstrap');
    require('bootstrap3');
} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');
require('promise.prototype.finally').shim();

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

window.Vue = require('vue');

import VueRouter from 'vue-router';
window.VueRouter = VueRouter;
Vue.use(VueRouter);

require('./../../../../node_modules/adminbsb-materialdesign/plugins/bootstrap-select/js/bootstrap-select');
require('./../../../../node_modules/adminbsb-materialdesign/plugins/jquery-slimscroll/jquery.slimscroll');
window.Waves = require('./../../../../node_modules/adminbsb-materialdesign/plugins/node-waves/waves');
require('./../../../../node_modules/adminbsb-materialdesign/plugins/jquery-countto/jquery.countTo');
require('./../../../../node_modules/adminbsb-materialdesign/plugins/raphael/raphael');
require('./../../../../node_modules/adminbsb-materialdesign/plugins/morrisjs/morris');
require('./../../../../node_modules/adminbsb-materialdesign/plugins/jquery-sparkline/jquery.sparkline');
require('adminbsb-materialdesign/js/helpers');
require('adminbsb-materialdesign/js/admin');
require('adminbsb-materialdesign/js/demo');

import wysiwyg from "vue-wysiwyg";
Vue.use(wysiwyg, {}); // config is optional. more below

import Vue2Filters from 'vue2-filters';
Vue.use(Vue2Filters);

require('./forms/bootstrap');
require('./utils');

window.toastr = require('toastr');
window.bootbox = require('bootbox');
