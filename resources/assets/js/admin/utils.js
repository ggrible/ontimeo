window.Utils = {
    arrayDelete(arr, object, replace = null){
        if (replace){
            arr.splice(arr.indexOf(object), 1, replace);
        }else{
            arr.splice(arr.indexOf(object), 1);
        }
    },

    arrayHas(arr, object){
        return arr.indexOf(object) > -1;
    },

    arrayHasAny(arr, objects){
        for (var i = 0; i < objects.length; i++){
            if (this.arrayHas(arr, objects[i])){
                return true;
            }
        }
        return false;
    },
    confirm(settings = {}){
        let options = {
            size: null,
            title : 'Confirmation',
            cancel : {
                label : 'Cancel',
                className: 'btn-default waves-effect'
            },
            success : {
                label : 'OK',
                className : 'btn-primary waves-effect'
            },
            callback : function () {

            }
        };
        $.extend(true, options, settings);
        bootbox.dialog({
            size: options.size,
            message: options.message,
            title: options.title,
            buttons: {
                cancel: {
                    label: options.cancel.label,
                    className: options.cancel.className,
                    callback : function () {
                        options.callback(false);
                    }
                },
                success: {
                    label: options.success.label,
                    className: options.success.className,
                    callback: function() {
                        options.callback(true);
                    }
                }
            }
        });
    },
};