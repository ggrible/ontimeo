module.exports = {
    post: function (uri, form, beforeSend = null) {
        return Comontime.sendForm('post', uri, form, beforeSend);
    },
    postData: function (uri, form, beforeSend = null) {
        return Comontime.sendFormData('post', uri, form, beforeSend);
    },

    get(uri, form){
        return Comontime.sendForm('get', uri, form);
    },


    put: function (uri, form, beforeSend = null) {
        return Comontime.sendForm('put', uri, form, beforeSend);
    },
    putData: function (uri, form) {
        return Comontime.sendFormData('put', uri, form);
    },


    delete: function (uri, form) {
        return Comontime.sendForm('delete', uri, form);
    },


    sendFormData: function (method, uri, form, beforeSend = null) {
        return new Promise(function (resolve, reject) {
            form.startProcessing();

            var form_data = form.getFormData();
            if (beforeSend != null){
                beforeSend(form_data);
            }
            axios[method](uri, form_data)
                .then(function (response) {
                    form.finishProcessing();
                    resolve(response);
                })
                .catch(function (errors) {
                    form.busy = false;
                    if (errors.response) {
                        form.errors.set(errors.response.data);
                        reject(errors.response.data);
                    } else {
                        form.errors.set(errors.message);
                        reject(errors.message);
                    }
                })
            ;
        })
            ;
    },

    sendForm: function (method, uri, form, beforeSend = null) {
        return new Promise(function (resolve, reject) {
            form.startProcessing();
            let data = JSON.parse(JSON.stringify(form));
            if (beforeSend) {
                beforeSend(data);
            }
            axios[method](uri, data)
                .then(function (response) {
                    form.finishProcessing();
                    resolve(response);
                })
                .catch(function (errors) {
                    form.busy = false;
                    console.log(errors.response);
                    if (errors.response) {
                        form.errors.set(errors.response.data);
                        reject(errors.response);
                    } else {
                        form.errors.set(errors.message);
                        reject(errors.message);
                    }
                })
            ;
        })
            ;
    }
};
