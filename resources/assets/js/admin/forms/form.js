window.Form = function (data) {
    var form = this;

    $.extend(this, data);

    this.errors = new FormErrors();

    this.busy = false;
    this.successful = false;

    this.getFormData = function () {
        var form_data = new FormData();
        for ( var key in this ) {
            if (this.hasOwnProperty(key) && typeof this[key] != 'function') {
                var data = this[key];
                if (data == null){
                    continue;
                }
                if (data instanceof Array){
                    var arr = data;
                    for (var i = 0; i < arr.length; i++) {
                        form_data.append(key + '[]', arr[i]);
                    }
                }else{
                    form_data.append(key, this[key]);
                }
            }
        }
        return form_data;
    };

    this.feedback = function (field, input) {
        /*if(input&&!this.errors.has(field)){
            return 'has-success has-feedback'
        }else if(!this.errors.has(field) && (!this.hasOwnProperty(field) || typeof this[field] != 'string' || this[field].trim() == '')){
            return 'has-empty'
        }else if(this.errors.has(field)){
            return 'has-error';
        }*/
        return {
            'has-success has-feedback' : input,
            'has-error' : this.errors.has(field),
            'is-empty' : !this.errors.has(field) && (!this.hasOwnProperty(field) || typeof this[field] !== 'string' || this[field].trim() === '')
        };
    };

    function htmlEncode(value){
        return $('<div/>').text(value).html();
    }

    this.helpBlock = function (field, input) {
        /*if (!this.errors.has(field)) return;
        if(input&&!this.errors.has(field)){
            $(`.${field}`).remove();
        }else{
            return `<span class="help-block ${field}">${this.errors.get(field)}</span>`;
        }*/
        if (!this.errors.has(field)) return;
        return `<span class="help-block">${this.errors.get(field)}</span>`;
    };

    this.listHelpBlock = function (field) {
        if (!this.errors.has(field)) return;
        var err = [];
        this.errors.allErrorsField(field).forEach(function(error){
            err.push(`<li class="list-group-item">${error}</li>`);
        });
        return err.join(" ");
    };

    this.startProcessing = function () {
        form.errors.forget();
        form.busy = true;
        form.successful = false;
    };

    this.finishProcessing = function (success = true) {
        form.busy = false;
        form.successful = success;
    };

    this.resetStatus = function () {
        form.errors.forget();
        form.busy = false;
        form.successful = false;
    };

    this.setErrors = function (errors) {
        form.busy = false;
        form.errors.set(errors);
    }

    this.clearFields = function () {
        $.extend(this, data);
    }
};
