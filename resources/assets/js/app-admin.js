
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./admin/bootstrap');
import mixin from './mixins/helpers.js';
import VeeValidate from 'vee-validate';
window.Vue = require('vue');
import VueRouter from 'vue-router';
import Multiselect from 'vue-multiselect'
import 'vue-multiselect/dist/vue-multiselect.min.css'
import Paginate from 'vuejs-paginate'
Vue.component('multiselect', Multiselect)
Vue.use(VeeValidate)
Vue.component('paginate', Paginate)

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.mixin(mixin);

window.Store = new Vue({
    data: {
        user: window.Comontime.state.user
    },
});

// client

import router from './admin/router';

const app = new Vue({
    router,
    el: '#app',
    data() {
        return {

        }
    },
    computed : {
        user() {
            return window.Store.user;
        }
    }
});
