<?php

use App\Http\Middleware\HaventDoneQuickInstallMiddleware;
use App\Http\Middleware\QuickInstallMiddleware;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Mail\ROICalculator;
use App\Mail\ContactUsRequestDemo;
use App\Mail\ContactUsCallBack;
use Illuminate\Support\Facades\Mail;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::any('/admin{all}', function () {
    return view('layouts.admin');
})
    ->where(['all' => '[\/\w\.-]*'])
    ->middleware(['auth', 'admin']);

Route::get('/meet-the-team', function () {
    return view('meet-the-team');
});

Route::get('/success', function () {
    return view('success');
});

Route::get('/faqpage', function () {
    return view('faq');
});

Route::get('/price-plans', function () {
    return view('price-plans');
});

Route::get('/privacy-policy', function () { return view('privacy-policy'); });
Route::get('/product-tour', function () { return view('product-tour'); });
Route::get('/custom-number', function () { return view('custom-number'); });
Route::get('/dot-env', function () { return view('vendor.dotenv-editor.overview'); });
Route::get('/success', function () { return view('success'); });
Route::get('/checkout', function () { return view('checkout'); });

Route::get('/package/{slug}', function (Request $request)
{
    $data['id'] = $request->slug;
    return view('packages/package-details', $data);
});

Route::get('/case-study/{category}', function ($category)
{
    $category_data  = App\CaseStudyCategory::where('name', '=', $category)->first();
    $case_studies   = null;

    if ($category_data)
    {
        $case_studies = App\CaseStudy::where('category_id', $category_data->id)->get();
    }

    return view('case-study-category', ['category'=>$category, 'case_studies'=>$case_studies]);
});

Route::get('/v/{number}', function ($number) {
    $user = \App\User::where('custom_number', $number)->first();
    if ($user) {
        return view('client-public-view', ['user' => $user]);
    } else {
        abort(404);
    }
})->where('number', '[0-9]+');

Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['middleware' => 'auth'], function () {
    Route::group(['prefix' => 'auth'], function () {
        Route::get('user', 'AuthController@user');
        Route::post('logged-in-first-time', 'AuthController@loggedInFirstTime');
    });
    Route::group(['prefix' => 'web-api'], function () {
        Route::apiResource('term', 'TermController', [
            'except' => ['index', 'show']
        ]);
        Route::apiResource('add-on', 'AddOnController', [
            'except' => ['index', 'show']
        ]);
        Route::get('package/count-summary', 'PackageController@getCountSummary')->middleware('admin');
        Route::apiResource('package', 'PackageController', [
            'except' => ['index', 'show']
        ]);
        Route::apiResource('case-study-category', 'CaseStudyCategoryController', [
            'except' => ['index', 'show']
        ]);
        Route::apiResource('case-study', 'CaseStudyController', [
            'except' => ['index', 'show']
        ]);
        Route::apiResource('user', 'UserController', [
            'except' => ['index', 'show']
        ]);
        Route::apiResource('voucher', 'VoucherController', [
            'except' => ['index', 'show']
        ]);
        Route::apiResource('lead', 'LeadController', [
            'except' => ['index', 'show']
        ]);
        Route::apiResource('faq', 'FaqController', [
            'except' => ['index', 'show']
        ]);
        Route::apiResource('employee', 'EmployeeController', [
            'except' => ['index', 'show']
        ]);
        Route::group(['middleware' => 'admin'], function () {
            Route::post('user/{user}/refund-subscription', 'UserController@refundSubscription');
        });
        Route::group(['middleware' => 'doctor'], function () {
            Route::resource('schedule', 'ScheduleController');
            Route::post('schedule/{schedule}/status', 'ScheduleController@setStatus');
            Route::get('schedule/download/icalendar', 'ScheduleController@downloadICalendar');
            Route::get('schedule/download/igoogle', 'ScheduleController@saveEventToGoogleCalendar');
        });
        Route::group(['prefix' => 'account'],  function () {
            Route::post('photo', 'AccountController@updatePhoto');
            Route::post('cover-photo', 'AccountController@updateCoverPhoto');
            Route::post('details', 'AccountController@updateDetails');
            Route::group(['middleware' => 'doctor'], function () {
                Route::post('subscription/cancel', 'AccountController@cancelSubscription');
            });
        });
        Route::group(['prefix' => 'admin'], function () {
            Route::group(['middleware' => 'admin', 'prefix' => 'setting'], function () {
                Route::get('/', 'SettingController@index');
                Route::get('/stripe-settings', 'SettingController@stripe');
                Route::post('/update-stripe', 'SettingController@updatestripe');
                Route::post('update', 'SettingController@update');
            });
        });
        Route::get('transaction', 'TransactionController@index');
        Route::get('transaction/summary', 'TransactionController@getSummary');
        Route::apiResource('withdrawal', 'WithdrawalController');
        Route::post('withdrawal/{withdrawal}/approve', 'WithdrawalController@approve')->middleware('admin');
    });
    Route::group(['middleware' => 'doctor'], function () {
        Route::get('google', 'GoogleController@handle');
        Route::get('google/callback', 'GoogleController@handleCallback');
        Route::get('outlook', 'OutlookController@handle');
        Route::get('outlook/callback', 'OutlookController@handleCallback');
        Route::get('/quick-install', function () {
            return view('client/quick-install');
        })->middleware(HaventDoneQuickInstallMiddleware::class);
        Route::group(['middleware' => QuickInstallMiddleware::class], function () {
            Route::get('/profile', function () {
                return view('client/profile');
            });
            Route::get('/schedules', function () {
                return view('client/schedules');
            });
            Route::get('/appointments', function () {
                return view('client/appointments');
            });
            Route::get('/email-sync', function () {
                return view('client/email-sync');
            });
            Route::get('/account-settings', function () {
                return view('client/account-settings');
            });
            Route::get('/employees', function () {
                return view('client/employee');
            });
            Route::get('/transactions', function () {
                return view('client/transactions');
            });
        });
    });
});

Route::group(['prefix' => 'web-api'], function () {
    Route::apiResource('term', 'TermController', [
        'only' => ['index', 'show']
    ]);
    Route::apiResource('add-on', 'AddOnController', [
        'only' => ['index', 'show']
    ]);
    Route::apiResource('package', 'PackageController', [
        'only' => ['index', 'show']
    ]);
    Route::apiResource('voucher', 'VoucherController', [
        'only' => ['index', 'show']
    ]);
    Route::post('voucher/check', 'VoucherController@check');
    Route::apiResource('lead', 'LeadController', [
        'only' => ['index', 'show']
    ]);
    Route::apiResource('faq', 'FaqController', [
        'only' => ['index', 'show']
    ]);
    Route::group(['middleware' => 'guest'], function () {
        Route::post('package/{package}/subscribe', 'PackageController@subscribe');
    });
    Route::post('telnyx/search', 'TelnyxController@search');
    Route::get('user/find-by-custom-number', 'Usercontroller@findByCustomNumber');
    Route::resource('appointment', 'AppointmentController');
    Route::get('schedule/{schedule}/public', 'ScheduleController@show');
    Route::get('schedule-date', 'ScheduleController@getScheduleByDate');
    Route::get('user-employees', 'UserController@userEmployees');
    Route::get('home/summary', 'HomeController@getSummary');
});

Route::group(['middleware' => 'guest'], function () {
    Route::get('setup-password/{email}/{code}', 'SetupPasswordController@showForm');
    Route::post('setup-password/submit', 'SetupPasswordController@submit');
});

Route::get('profile/{company}', 'LeadController@profile');
Route::get('case-study-category', 'CaseStudyCategoryController@index');
Route::stripeWebhooks('stripe/webhook');

Route::get('/checkout', function () {
    return view('checkout');
});

Route::get('/contact-us', function () { return view('contact-us'); });
Route::get('/clear-cache', function()
{
    Artisan::call('cache:clear');
    Artisan::call('view:clear');
    return '<span>View cache cleared</span> - <a href="http://test.ontimeo.com">Go to home</a>';
});

Route::get('/packages', function()
{
    $packages = App\Package::all();
    return response()->json($packages);
});

Route::post('/roi_sendmail', function(Request $request)
{
    $user = new \stdClass();
    $user->company_name = $request->input('form_company');

    Mail::to($request->input('form_email'))->send(new ROICalculator($user));

    $lead               = new App\Lead();
    $lead->company      = $request->input('form_company');
    $lead->url          = '';
    $lead->ceo_company  = '';
    $lead->telephone    = $request->input('form_contact');
    $lead->email        = $request->input('form_email');
    $lead->company_type = 'Business';
    $lead->save();

    return response()->json(array('status'=>'OK', 'message'=>'E-mail successfully sent.'));
});

Route::post('/contact_requestdemo_sendmail', function(Request $request)
{
    $user               = new \stdClass();
    $user->first_name   = $request->input('first_name');
    $user->last_name    = $request->input('last_name');
    $user->email        = $request->input('form_email');
    $user->contact      = $request->input('form_contact');
    $user->website      = $request->input('form_website');
    $user->comments     = $request->input('form_comment');
    $user->company      = $request->input('form_company');

    Mail::to('gdanilonava@gmail.com')->send(new ContactUsRequestDemo($user));

    $lead               = new App\Lead();
    $lead->company      = $request->input('form_company');
    $lead->url          = $request->input('form_website');
    $lead->ceo_company  = $request->input('form_company');
    $lead->telephone    = $request->input('form_contact');
    $lead->email        = $request->input('form_email');
    $lead->company_type = 'Business';
    $lead->save();

    return response()->json(array('status'=>'OK', 'message'=>'E-mail successfully sent.'));
});

Route::post('/contact_callback_sendmail', function(Request $request)
{
    $user                   = new \stdClass();
    $user->support_day      = $request->input('form_support_day');
    $user->support_time     = $request->input('form_support_time');
    $user->support_contact  = $request->input('form_support_contact');
    $user->support_comment  = $request->input('form_support_comment');
    $user->support_datesent = @date("l F d, Y - h:i A");

    Mail::to('gdanilonava@gmail.com')->send(new ContactUsCallBack($user));

    /*$lead               = new App\Lead();
    $lead->company      = $request->input('form_company');
    $lead->url          = $request->input('form_website');
    $lead->ceo_company  = $request->input('form_company');
    $lead->telephone    = $request->input('form_contact');
    $lead->email        = $request->input('form_email');
    $lead->company_type = 'Business';
    $lead->save();*/

    return response()->json(array('status'=>'OK', 'message'=>'E-mail successfully sent.'));
});