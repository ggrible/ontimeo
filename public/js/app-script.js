/*
 * Author: Danilo M. Nava Jr.
 * Date: 15 Jan 2015
 * Description: This file should be included in all pages
 **/


function isEmail (email)
{
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

jQuery(function()
{
	console.log('this is >> app-script.js');

	/**
	 * MODALS..
	**/
	//jQuery('#modal-roi').modal();

	/**
	 * Homepage ROI button..
	**/
	jQuery("#button-roi-showpopup").click(function(e)
	{
		e.preventDefault();
		console.log('ROI button here..');

		// reset to tab 1..
		jQuery('div.wizard-tab-container').css('display','none');
		jQuery('#Personaldetails0').css('display','block');
		jQuery('.wizard-progress-bar').css('width', '12.5%');
		jQuery('li.tab-step-2').removeClass('active');
		jQuery('li.tab-step-3').removeClass('active');
		jQuery('li.tab-step-4').removeClass('active');

		// reset selected package id..
		jQuery('#package_selected').val(0);

		jQuery('#form_email').val('');
		jQuery('#form_company').val('');
		jQuery('#form_contact').val('');
		jQuery('#form_expected').val('');
		jQuery('#form_salary').val('');

		jQuery.ajax({
			url: "/packages",
			type: "GET",
			async: true,
			dataType: 'json',
			data: { },
			success: function(data)
			{
				console.log(data);
				var mark_up = '<div class="col-lg-12 col-md-12 col-sm-12" style="text-align: center; padding: 30px 0;"><b style="color: red;">-- No package found --</b></div>';

				if (data.length > 0)
				{
					mark_up = '';

					jQuery.each(data, function(index, package)
					{
						mark_up += '<div class="col-lg-4 col-md-12 col-sm-12">'+
                '<div class="ontimeo-box">'+
                   '<div class="ontimeo-content" style="height: 150px;" data-package_id="'+ package.id +'">'+
                      '<div class="text_holder"><div class="text"><h4 class="lessmar">'+ package.name +'</h4></div></div>'+
                   '</div>'+
                '</div>'+
             '</div>';
					});
				}

				jQuery('#SelectAProduct2 .package-list').empty().append(mark_up);
				jQuery('#modal-roi').modal('show');

				jQuery(".ontimeo-content").click(function(e)
				{
					e.preventDefault();
					jQuery(".ontimeo-content").removeClass('active');
					jQuery(this).addClass('active');

					var package_id = jQuery(this).data('package_id');
					console.log(package_id);
					jQuery('#package_selected').val(package_id);
				});
			},
			error: function(req, status, err)
			{
				console.log(req, status, err);
			}
		});
	});

	// bind for modal close..
	jQuery("#button-roi-closepopup").click(function(e)
	{
		e.preventDefault();
		jQuery('#modal-roi').modal('hide');
	});

	// Tab PERSONAL button..
	jQuery("#button-roi-next-personal").click(function(e)
	{
		e.preventDefault();

		var form_email 		= jQuery.trim(jQuery('#form_email').val());
		var form_company 	= jQuery.trim(jQuery('#form_company').val());
		var form_contact 	= jQuery.trim(jQuery('#form_contact').val());
		var error_message = [];

		console.log('Email >> '+ form_email, (form_email == ''))

		jQuery('#form_email').removeClass('red-border');
		jQuery('#form_company').removeClass('red-border');
		jQuery('#form_contact').removeClass('red-border');

		if (form_email == '')
		{
			error_message.push('* Email is required.');
			jQuery('#form_email').addClass('red-border');
		}
		else if ( ! isEmail(form_email))
		{
			error_message.push('* Email invalid format.');
			jQuery('#form_email').addClass('red-border');
		}
		else { }

		if (form_company == '')
		{
			error_message.push('* Company is required.');
			jQuery('#form_company').addClass('red-border');
		}

		if (form_contact == '')
		{
			error_message.push('* Contact number is required.');
			jQuery('#form_contact').addClass('red-border');
		}

		if (error_message.length > 0)
		{
			console.log(error_message);
		}
		else
		{
			jQuery('div.wizard-tab-container').css('display','none');
			jQuery('#SelectAProduct2').css('display','block');

			jQuery('li.tab-step-2').addClass('active');
			jQuery('.wizard-progress-bar').css('width', '35.5%');
		}
	});

	// Tab PRODUCT buttons..
	jQuery("#button-roi-next-product").click(function(e)
	{
		e.preventDefault();

		var package_id 		= jQuery('#package_selected').val();
		var error_message = [];

		jQuery('.package-list').removeClass('red-border');

		if (package_id == 0)
		{
			error_message.push('* Select one package.');
			jQuery('.package-list').addClass('red-border');
		}

		if (error_message.length > 0)
		{
			console.log(error_message);
		}
		else
		{
			jQuery('div.wizard-tab-container').css('display','none');
			jQuery('#CompanyDetails4').css('display','block');

			jQuery('li.tab-step-3').addClass('active');
			jQuery('.wizard-progress-bar').css('width', '60.5%'); // 85.5
		}
	});

	jQuery("#button-roi-prev-product").click(function(e)
	{
		e.preventDefault();
		jQuery('div.wizard-tab-container').css('display','none');
		jQuery('#Personaldetails0').css('display','block');

		jQuery('li.tab-step-2').removeClass('active');
		jQuery('.wizard-progress-bar').css('width', '12.5%');
	});

	// Tab COMPANY buttons..
	jQuery("#button-roi-next-company").click(function(e)
	{
		e.preventDefault();

		var form_expected = jQuery.trim(jQuery('#form_expected').val());
		var form_salary 	= jQuery.trim(jQuery('#form_salary').val());
		var error_message = [];

		jQuery('#form_expected').removeClass('red-border');
		jQuery('#form_salary').removeClass('red-border');

		if (form_expected == '')
		{
			error_message.push('* Expected employee is required.');
			jQuery('#form_expected').addClass('red-border');
		}

		if (form_salary == '')
		{
			error_message.push('* Annual salary number is required.');
			jQuery('#form_salary').addClass('red-border');
		}

		if (error_message.length > 0)
		{
			console.log(error_message);
		}
		else
		{
			jQuery('div.wizard-tab-container').css('display','none');
			jQuery('#Hereswhatyousave6').css('display','block');

			jQuery('li.tab-step-4').addClass('active');
			jQuery('.wizard-progress-bar').css('width', '85.5%'); // 85.5
		}
	});

	jQuery("#button-roi-prev-company").click(function(e)
	{
		e.preventDefault();
		jQuery('div.wizard-tab-container').css('display','none');
		jQuery('#SelectAProduct2').css('display','block');

		jQuery('li.tab-step-3').removeClass('active');
		jQuery('.wizard-progress-bar').css('width', '35.5%');
	});

	// Tab YOUSAVE buttons..
	jQuery("#button-roi-next-yousave").click(function(e)
	{
		e.preventDefault();
		// SUBMIT FORM..
		jQuery('#modal-roi').modal('hide');
	});

	jQuery("#button-roi-prev-yousave").click(function(e)
	{
		e.preventDefault();
		jQuery('div.wizard-tab-container').css('display','none');
		jQuery('#CompanyDetails4').css('display','block');

		jQuery('li.tab-step-4').removeClass('active');
		jQuery('.wizard-progress-bar').css('width', '60.5%');
	});

	jQuery("#button-roi-sendemail-yousave").click(function(e)
	{
		e.preventDefault();

		jQuery.ajax({
			url: "/roi_sendmail",
			type: "POST",
			async: true,
			dataType: 'json',
			data: jQuery('#form_roi').serialize(),
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
			success: function(data)
			{
				console.log(data);

				if (data.status == 'OK')
				{
					jQuery('#modal-roi').modal('hide');
				}
			},
			error: function(req, status, err)
			{
				console.log(req, status, err);
			}
		});
	});

	/**
	 * CaseStudy..
	**/
	jQuery("#user_casestudy a.user_image").click(function(e)
	{
		e.preventDefault();
		var user_name 			= jQuery(this).data('user_name');
		var company_name 		= jQuery(this).data('company_name');
		var user_case_study = jQuery(this).data('user_case_study');

		jQuery('#user_casestudy .user_case_study').text(user_case_study);
		jQuery('#user_casestudy .user_name').html(user_name +'  in <cite class="company_name">'+ company_name +'</cite>');
		//jQuery('#user_casestudy .company_name').text(company_name);
	});

	/**
	 * CONTACT US
	 **/
	jQuery("#button-contact-demo-closepopup").click(function(e) { e.preventDefault(); jQuery('#modal-contact-demo').modal('hide'); });
	jQuery("#button-contact-demo-showpopup").click(function(e)
	{
		e.preventDefault();

		jQuery('#modal-contact-demo .form-control').removeClass('red-border');

		jQuery('#first_name').val('');
		jQuery('#last_name').val('');
		jQuery('#form_email').val('');
		jQuery('#form_contact').val('');
		jQuery('#form_company').val('');
		jQuery('#form_website').val('');
		jQuery('#form_comment').val('');

		jQuery('#modal-contact-demo').modal('show');
	});

	// send demo form..
	jQuery("#button-contact-demo-sendform").click(function(e)
	{
		e.preventDefault();
		
		var first_name 		= jQuery.trim(jQuery('#first_name').val());
		var last_name 		= jQuery.trim(jQuery('#last_name').val());
		var form_email 		= jQuery.trim(jQuery('#form_email').val());
		var form_contact 	= jQuery.trim(jQuery('#form_contact').val());
		var form_company 	= jQuery.trim(jQuery('#form_company').val());
		var form_website 	= jQuery.trim(jQuery('#form_website').val());
		var form_comment 	= jQuery.trim(jQuery('#form_comment').val());
		var error_message = [];

		jQuery('#modal-contact-demo .form-control').removeClass('red-border');

		if (form_email == '')
		{
			error_message.push('* Email is required.');
			jQuery('#form_email').addClass('red-border');
		}
		else if ( ! isEmail(form_email))
		{
			error_message.push('* Email invalid format.');
			jQuery('#form_email').addClass('red-border');
		}
		else { }

		if (form_company == '')
		{
			error_message.push('* Company is required.');
			jQuery('#form_company').addClass('red-border');
		}

		if (first_name == '')
		{
			error_message.push('* First name is required.');
			jQuery('#first_name').addClass('red-border');
		}

		if (last_name == '')
		{
			error_message.push('* Last name is required.');
			jQuery('#last_name').addClass('red-border');
		}

		if (form_contact == '')
		{
			error_message.push('* Contact number is required.');
			jQuery('#form_contact').addClass('red-border');
		}

		if (error_message.length > 0)
		{
			console.log(error_message);
		}
		else
		{
			jQuery.ajax({
				url: "/contact_requestdemo_sendmail",
				type: "POST",
				async: true,
				dataType: 'json',
				data: jQuery('#form_contact_requestdemo').serialize(),
				headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
				success: function(data)
				{
					console.log(data);

					if (data.status == 'OK')
					{
						jQuery('#modal-contact-demo').modal('hide');
					}
				},
				error: function(req, status, err)
				{
					console.log(req, status, err);
				}
			});
		}
	});

	jQuery("#button-contact-sales-closepopup").click(function(e) { e.preventDefault(); jQuery('#modal-contact-sales').modal('hide'); });
	jQuery("#button-contact-sales-showpopup").click(function(e)
	{
		e.preventDefault();
		
		jQuery('#form_support_day').val('TODAY');
		jQuery('#form_support_time').val('07AM');
		jQuery('#form_support_contact').val('');
		jQuery('#form_support_comment').val('');

		// just use the support popup..
		//jQuery('#modal-contact-sales').modal('show');
		jQuery('#modal-contact-support').modal('show');
	});

	jQuery("#button-contact-support-closepopup").click(function(e) { e.preventDefault(); jQuery('#modal-contact-support').modal('hide'); });
	jQuery("#button-contact-support-showpopup").click(function(e)
	{
		e.preventDefault();
		
		jQuery('#form_support_day').val('TODAY');
		jQuery('#form_support_time').val('07AM');
		jQuery('#form_support_contact').val('');
		jQuery('#form_support_comment').val('');

		jQuery('#modal-contact-support').modal('show');
	});

	// send callback form..
	jQuery("#button-contact-callback-sendform").click(function(e)
	{
		e.preventDefault();
		
		var form_contact 	= jQuery.trim(jQuery('#form_support_contact').val());
		var error_message = [];

		jQuery('#modal-contact-support .form-control').removeClass('red-border');

		if (form_contact == '')
		{
			error_message.push('* Contact number is required.');
			jQuery('#form_support_contact').addClass('red-border');
		}

		if (error_message.length > 0)
		{
			console.log(error_message);
		}
		else
		{
			jQuery.ajax({
				url: "/contact_callback_sendmail",
				type: "POST",
				async: true,
				dataType: 'json',
				data: jQuery('#form_contact_callback').serialize(),
				headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
				success: function(data)
				{
					console.log(data);

					if (data.status == 'OK')
					{
						jQuery('#modal-contact-support').modal('hide');
					}
				},
				error: function(req, status, err)
				{
					console.log(req, status, err);
				}
			});
		}
	});
});